<?php

/*
*
*	Template Name: Archive [ Press ]
*	Filename: archive-press.php
*
*/

get_header();

// ---------------------------------------- Polite Department
$VP = new PDTheme();
$post_id = get_the_ID();

// ---------------------------------------- Template Data
$block_name = 'press';
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$featured = get_field( 'featured', 'options' ) ? get_field( 'featured', 'options' ) : [];
$navigation = get_field( 'navigation', 'options' ) ? get_field( 'navigation', 'options' ) : [];
$post_type = get_post_type();

// ---------------------------------------- Template
echo '<div class="' . $block_name . '">';

  /////////////////////////////////////////////////////////
  ////  Navigation
  //////////////////////////////////////////////////////////

  if ( $navigation ) {
    echo '<nav class="' . $block_name . '__navigation navigation">';
      echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
        echo '<div class="navigation__main">';

          echo '<div class="navigation__label">Sort by:</div>';
          echo '<div class="navigation__list">';

            foreach( $navigation as $i => $item ) {

              $current_cat = ( isset($_GET['category_name']) && !empty($_GET['category_name']) ) ? $_GET['category_name'] : false;
              $cat = ( isset($item['category']) && !empty($item['category']) ) ? $item['category'] : false;
              $cat_name = $cat->name;
              $cat_slug = $cat->slug;
              $base_url = get_post_type_archive_link( 'press' );
              $cat_url = $base_url . '?category_name=' . $cat_slug;
              $is_active = ( $current_cat === $cat_slug ) ? true : false;

              echo '<div class="navigation__item">';
                echo $VP->render_link([
                  'active' => $is_active,
                  'classes' => 'navigation__link',
                  'title' => $cat_name,
                  'url' => $cat_url,
                ]);
              echo '</div>';

            }

            $is_active = ( $_GET ) ? false : true;

            echo '<div class="navigation__item">';
              echo $VP->render_link([
                'active' => $is_active,
                'classes' => 'navigation__link',
                'title' => 'All',
                'url' => get_post_type_archive_link( 'press' ),
              ]);
            echo '</div>';

          echo '</div>';

        echo '</div>';
      echo $VP->render_container( 'closed' );
    echo '</nav>';
  }

  if ( $_GET ) {

    //////////////////////////////////////////////////////////
    ////  Default WP Query with URL Params
    //////////////////////////////////////////////////////////

    if ( have_posts() ) {

      echo '<section class="' . $block_name . '__grid all">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
          echo '<div class="row row--inner">';

            while ( have_posts() ) {
              the_post();
              echo '<div class="col-12 col-sm-6 col-md-4 col-lg-3">';
                echo $VP->render_press_preview( [ 'post_id' =>  get_the_ID() ] );
              echo '</div>';
            }

          echo '</div>';
        echo $VP->render_container( 'closed' );
      echo '</section>';

      if ( $wp_query->max_num_pages > 1 ) {

        $button_prev = get_previous_posts_link( 'Prev' );
        $button_next = get_next_posts_link( 'Next',  $wp_query->max_num_pages );
        $big = 999999999;
        $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
        $pagination_args = [
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => $pagination_format,
          'total' => $wp_query->max_num_pages,
          'current' => max( 1, get_query_var('paged') ),
          'aria_current' => false,
          'show_all' => true,
          'end_size' => 1,
          'mid_size' =>2,
          'prev_next' => false,
          'prev_text' => '',
          'next_text' => '',
          'type' => 'array',
          'add_args' => false,
          'add_fragment' => '',
          'before_page_number' => '',
          'after_page_number' => ''
        ];
        $pagination_links = paginate_links( $pagination_args ) ? paginate_links( $pagination_args ) : [];

        echo $VP->render_pagination([ 'next' => $button_next, 'pages' => $pagination_links, 'prev' => $button_prev, ]);
      }

    } else {
      echo '<section class="error">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
          echo '<h2 class="press__title title title--no-press">Nothing here.</h2>';
        echo $VP->render_container( 'closed' );
      echo '</section>';
    }

  } else {

    //////////////////////////////////////////////////////////
    ////  Featured Press
    //////////////////////////////////////////////////////////

    if ( $featured && $paged < 2 ) {
      echo '<section class="' . $block_name . '__grid featured">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
          echo '<div class="row row--inner">';

            foreach( $featured as $i => $item ) {
              $press_id = ( isset($item['press']) && !empty($item['press']) ) ? $item['press'] : false;
              $press_wide = ( 0 === $i ) ? true : false;
              $column_classes = ( 0 === $i ) ? 'col-12 col-lg-6' : 'col-12 col-sm-6 col-lg-3';
              if ( $press_id ) {
                echo '<div class="' . $column_classes . '">';
                  echo $VP->render_press_preview( [ 'post_id' => $press_id, 'wide' => $press_wide ] );
                echo '</div>';
              }
            }

          echo '</div>';
        echo $VP->render_container( 'closed' );
      echo '</section>';
    }

    //////////////////////////////////////////////////////////
    ////  Custom WP Query
    //////////////////////////////////////////////////////////

    $query_args = [
      'post_type'             => [ 'press' ],
      'post_status'           => [ 'publish' ],
      'order'                 => 'DESC',
      'orderby'               => 'date',
      'nopaging'              => false,
      'paged'                 => $paged,
      'post__not_in'          => [],
    ];

    foreach ( $featured as $i => $item ) {
      $press_id = ( isset($item['press']) && !empty($item['press']) ) ? $item['press'] : false;
      if ( $press_id ) {
        array_push( $query_args['post__not_in'], $press_id );
      }
    }

    $press_query = new WP_Query( $query_args );

    if ( $press_query->have_posts() ) {

      echo '<section class="' . $block_name . '__grid all">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
          echo '<div class="row row--inner">';

            while ( $press_query->have_posts() ) {

              // init post data
              $press_query->the_post();

              echo '<div class="col-12 col-sm-6 col-md-4 col-lg-3">';
                echo $VP->render_press_preview( [ 'post_id' => get_the_ID() ] );
              echo '</div>';

            }

          echo '</div>';
        echo $VP->render_container( 'closed' );
      echo '</section>';

      if ( $press_query->max_num_pages > 1 ) {
        $button_prev = get_previous_posts_link( 'Prev' );
        $button_next = get_next_posts_link( 'Next', $press_query->max_num_pages );
        $big = 999999999;
        $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
        $pagination_args = [
          'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
          'format' => $pagination_format,
          'total' => $wp_query->max_num_pages,
          'current' => max( 1, get_query_var('paged') ),
          'aria_current' => false,
          'show_all' => true,
          'end_size' => 1,
          'mid_size' =>2,
          'prev_next' => false,
          'prev_text' => '',
          'next_text' => '',
          'type' => 'array',
          'add_args' => false,
          'add_fragment' => '',
          'before_page_number' => '',
          'after_page_number' => ''
        ];
        $pagination_links = paginate_links( $pagination_args ) ? paginate_links( $pagination_args ) : [];
        echo $VP->render_pagination([ 'next' => $button_next, 'pages' => $pagination_links, 'prev' => $button_prev, ]);
      }

      wp_reset_postdata();

    } else {
      echo '<section class="error">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
          echo '<h2>Nothing here</h2>';
        echo $VP->render_container( 'closed' );
      echo '</section>';
    }

  }

echo '</div>';

get_footer();

?>
