<?php
  
  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////
  
  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();
  
  //////////////////////////////////////////////////////////
  ////  Template Vars
  //////////////////////////////////////////////////////////
  
  echo '<h1>Featured</h1>';
  
  if ( is_category() ) {
    $cat = get_queried_object();
    $cat_term = get_term( $cat->term_id );
  }

  if ( have_rows( 'articles' ) ) {
    while ( have_rows( 'articles' ) ) {
      the_row();
      if ( get_sub_field( 'featured' ) ){
        $featured_article_id = get_sub_field( 'featured' );
        debug_this( $featured_article_id );
      }
    }
  }
    
?>
