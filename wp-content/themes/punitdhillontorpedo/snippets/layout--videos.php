<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Section
  //////////////////////////////////////////////////////////

  echo '<section class="section section--videos">';

    if ( have_rows( 'video_repeater' ) ) {

      echo '<ul class="section__video-list">';

        while ( have_rows( 'video_repeater' ) ) {

          // init data
          the_row();

          // default data
          $aspect_ratio = $id = $source = false;

          // get data
          $aspect_ratio = get_sub_field( 'aspect_ratio' ) ? get_sub_field( 'aspect_ratio' ) : false;
          $id = get_sub_field( 'id' ) ? get_sub_field( 'id' ) : false;
          $source = get_sub_field( 'source' ) ? get_sub_field( 'source' ) : false;

          // print data
          echo ( $id && $source ) ? '<li class="section__video" data-aspect-ratio="' . $aspect_ratio . '">' . $VP->render_lazyload_iframe( $id, $source ) . '</li>' : '';

        }

      echo '</ul>';

    }

  echo '</section>';

?>
