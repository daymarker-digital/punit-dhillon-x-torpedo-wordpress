<?php
  
  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////
  
  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();
  
  //////////////////////////////////////////////////////////
  ////  Section
  //////////////////////////////////////////////////////////
      
  echo '<section class="section section--blockquote blockquote">';
    echo '<div class="container-fluid">';
      echo '<div class="row">';
        echo ' <div class="col-12 col-lg-10 offset-lg-1">';
  
          if ( have_rows( 'blockquote' ) ) {
            while ( have_rows( 'blockquote' ) ) {
              
              // init data
              the_row();
              
              // default data
              $icon = $quote = $note = false;
              
              // get data
              if ( get_sub_field('icon') ) {
                $icon = get_sub_field('icon');
              }
              if ( get_sub_field('quote') ) {
                $quote = get_sub_field('quote');
              }
              if ( get_sub_field('note') ) {
                $note = get_sub_field('note');
              }
              
              // print data
              if ( $icon ) {
                echo '<div class="blockquote__icon">';
                  echo '<img src="' . $icon['url'] . '" alt="Decorative Icon" />';
                echo '</div>';
              }
              
              if ( $quote ) { 
                echo '<div class="blockquote__quote">';
                  echo '<h2 class="blockquote__heading heading">' . $quote . '</h2>';
                echo '</div>';
              }
              
              if ( $note ) { 
                echo '<p class="blockquote__note">';
                  echo $note;
                echo '</p>';
              }
              
            }  
          }
        
        echo '</div>';
      echo '</div>';
    echo '</div>';
  echo '</section>';
    
?>
