<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Template Vars
  //////////////////////////////////////////////////////////

  if ( have_rows( 'about' ) ) {
    while ( have_rows( 'about' ) ) {

      // init data
      the_row();

      // default data
      $heading = $post_excerpt = $post_id = $post_image = $post_title = $post_permalink = false;

      // get data
      if ( get_sub_field( 'post_id' ) ) {
        $post_id = get_sub_field( 'post_id' );
        $post_title = get_the_title( $post_id );
        $post_permalink = get_permalink( $post_id );
        if ( get_the_excerpt( $post_id ) ) {
          $post_excerpt = get_the_excerpt( $post_id );
        }
      }
      if ( get_sub_field( 'heading' ) ) {
        $heading = get_sub_field( 'heading');
      } else {
        $heading = $post_title;
      }
      if ( $VP->get_featured_image_by_post_id( $post_id ) ) {
        $post_image = $VP->get_featured_image_by_post_id( $post_id );
      }

      // print data
      echo '<div class="articles-about">';

        if ( $heading ) {
          echo '<h2 class="articles-about__heading heading heading--secondary">' . $heading . '</h2>';
        }
        if ( $post_image ) {
          echo '<div class="articles-about__image">';
            echo $VP->render_lazyload_image( $post_image );
          echo '</div>';
        }
        if ( $post_excerpt ) {
          echo '<div class="articles-about__excerpt"><p>' . trim_string( $post_excerpt, 90 ) . '</p></div>';
        }
        if ( $post_permalink ) {
          echo '<div class="articles-about__cta">';
            echo '<a href="' . $post_permalink . '">More</a>';
          echo '</div>';
        }

      echo '</div>';

    }
  }

?>
