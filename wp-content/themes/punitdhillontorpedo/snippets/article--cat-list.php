<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////
  ////  Snippet
  //////////////////////////////////////////////////////////

  if ( isset( $post ) ) {
    $cats = get_the_category();
    if ( $cats ) {
      echo '<ul class="article__cat-list article__cat-list--icon">';
      foreach ( $cats as $i => $cat ) {

        // default data
        $cat_icon = $cat_id = $cat_link = $cat_name = $cat_term = false;
        $cat_icon_field = 'icon';

        // get data
        $cat_id = $cat->term_id;
        $cat_name = $cat->name;
        $cat_term = get_term( $cat_id );
        $cat_link = get_category_link( $cat_id );

        if ( get_field( $cat_icon_field, $cat_term ) ) {
          $cat_icon = get_field( $cat_icon_field, $cat_term );
        }

        // print data
        if ( $cat_icon && $cat_link ) {
          echo '<li class="article__cat article__cat--icon">';
            echo '<span class="article__cat-icon">';
              echo '<img src="' . $cat_icon['url'] . '" alt="' . $cat_name . ' Icon" />';
            echo '</span>';
            echo '<span class="article__cat-name">';
              echo $cat_name;
            echo '</span>';
          echo '</li>';
        }
      }
      echo '</ul>';
    }
  }

?>
