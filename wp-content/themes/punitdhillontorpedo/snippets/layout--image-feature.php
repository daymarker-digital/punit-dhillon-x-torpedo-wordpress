<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Section | Image Feature
  //////////////////////////////////////////////////////////

  if ( have_rows( 'image_feature' ) ) {
    while ( have_rows( 'image_feature' ) ) {

      // init data
      the_row();

      // default data
      $image_count = 0;

      // get data
      if ( get_sub_field('images') ) {
        $image_count = count( get_sub_field('images') );
      }

      // print data
      if ( have_rows( 'images' ) ) {

          echo '<section class="section section--image-feature image-feature" data-image-count="' . $image_count . '">';

          while ( have_rows( 'images' ) ) {

            // init data
            the_row();

            // default data
            $image = false;

            // get data
            if ( get_sub_field('image') ) {
              $image = get_sub_field('image');
            }

            if ( $image ) {
              echo '<div class="image-feature__image image">';
                echo $VP->render_lazyload_image( $image );
              echo '</div>';
            }

          }

          echo '</section>';

        }

    }
  }

?>
