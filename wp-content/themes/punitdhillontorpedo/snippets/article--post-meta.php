<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////
  ////  Snippet
  //////////////////////////////////////////////////////////

  if ( isset( $post ) ) {

    $author = get_the_author();
    $post_date = get_the_date("m.d.y");

    echo '<div class="article__post-info">';

      if ( $author ) {
        echo '<span>By ' . $author . '</span><span class="spacer">|</span><span>' . $post_date . '</span>';
      } else {
        echo '<span>Posted ' . $post_date . '</span>';
      }

    echo '</div>';

  }

?>
