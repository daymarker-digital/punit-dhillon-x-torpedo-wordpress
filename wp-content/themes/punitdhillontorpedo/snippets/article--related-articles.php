<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////
  ////  Snippet Vars
  //////////////////////////////////////////////////////////

  if ( isset( $post ) ) {

    $related = false;
    $post_cats = wp_get_post_categories( $post->ID );
    $query_args = array(
      'category__in' => $post_cats,
      'posts_per_page' => 2,
      'post__not_in' => array( $post->ID ),
      'order' => 'DESC',
      'orderby' => 'date',
    );

    if ( $post_cats ) {

      $related = new WP_Query( $query_args );

      if ( $related->have_posts() ) {

        echo '<div class="article__related-articles">';

          echo '<h3 class="article__heading article__heading--related">Related Articles</h3>';

          while ( $related->have_posts() ) {

            // init post data
            $related->the_post();

            // post vars
            $related_post_ID = get_the_ID();
            $related_post_title = get_the_title();
            $related_post_permalink = get_the_permalink();
            $related_post_date = get_the_date("m.d.y");
            $related_post_cats = wp_get_post_categories( $related_post_ID );
            $related_post_cats_html = '';

            foreach ( $related_post_cats as $index => $cat_id ) {

              $this_cat = get_category( $cat_id );
              $link = get_category_link( $cat_id );
              $title = $this_cat->name;

              $related_post_cats_html .= '<li class="article__related-cats-item">';
                $related_post_cats_html .=  $title;
              $related_post_cats_html .= '</li>';

            }

            // print data
            echo '<article class="article article--related">';

              echo '<div class="article__info">';
                echo '<h4 class="article__title article__title--related">' . $related_post_title . '</h4>';
                echo '<div class="article__post-info article__post-info--related">';
                  echo '<span>' . $related_post_date . '</span><span class="spacer">|</span>' . ( $related_post_cats_html ? '<ul class="article__related-cats-list">' . $related_post_cats_html . '</ul>' : '' );
                echo '</div>';
              echo '</div>';

              echo '<div class="article__cta">';
                echo '<a class="cta button button--outline button--rounded" href="' . $related_post_permalink . '">Read</a>';
              echo '</div>';

            echo '</article>';

          }

        echo '</div>';

      }

      wp_reset_postdata();

    }

  }

?>
