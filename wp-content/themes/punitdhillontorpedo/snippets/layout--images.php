<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Section
  //////////////////////////////////////////////////////////

  echo '<section class="section section--images">';

    if ( have_rows( 'image_repeater' ) ) {

      echo '<ul class="section__image-list">';

        while ( have_rows( 'image_repeater' ) ) {

          // init data
          the_row();

          // get data
          $alt_text = get_sub_field( 'alt_text' ) ? get_sub_field( 'alt_text' ) : false;
          $image = get_sub_field( 'image' ) ? get_sub_field( 'image' ) : false;
          $image_params = ( $alt_text ) ? [ 'alt_text' => $alt_text ] : [];

          // print data
          echo ( $image ) ? '<li class="section__image">' . $VP->render_lazyload_image( $image, $image_params ) . '</li>' : '';

        }

      echo '</ul>';

    }

  echo '</section>';

?>
