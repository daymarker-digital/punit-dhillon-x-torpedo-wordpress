<div class="placeholder" style="padding: 62px 0;">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          
          <div class="grid">

            <h2>Fluid Columns</h2>
            
            <a href="https://punit-dhillon.myshopify.com/cart/36332343558306:1?checkout" target="_blank" rel="noopener noreferrer">Checkout</a>
            
            <?php
              if ( get_field( 'post_content' ) ) {
                $post_id = get_field( 'post_content' );
                debug_this( $post_id );
                
                if ( $post_id ) {
                  $the_post = get_post( $post_id );
                  debug_this( $the_post );
                }
                
              }
            ?>
                        
            <p>Sed ac rutrum turpis, id posuere justo. Nullam finibus egestas neque eget luctus. Etiam vel dapibus erat. Sed hendrerit dui sed eros ultrices gravida a a turpis. Phasellus lectus nisl, molestie sit amet ullamcorper at, faucibus et mauris. Pellentesque consequat tempor turpis eu posuere. Nam faucibus est vitae odio blandit, sed maximus sem suscipit. Suspendisse pharetra maximus eros, at semper ante blandit eu. Nam aliquam sollicitudin purus, sed imperdiet diam. Sed lacinia tristique eros a malesuada. Nullam convallis eu risus blandit tristique. Nam eget odio convallis erat imperdiet bibendum non nec massa. Pellentesque ut accumsan leo, a euismod tellus.</p>
            
            <p>Pellentesque condimentum ultrices est eget hendrerit. Integer molestie magna massa, sit amet dapibus erat condimentum non. Cras egestas odio ac diam laoreet condimentum. Donec consectetur interdum enim at convallis. Praesent ornare volutpat dapibus. Nunc ornare, nibh at viverra malesuada, mi erat scelerisque dui, ut posuere elit purus id enim. Aliquam mollis libero at arcu mattis finibus. Praesent viverra sollicitudin nunc quis feugiat. Sed a justo pulvinar, sollicitudin mauris in, lobortis nibh. Praesent porttitor at ligula vitae laoreet. Nunc placerat lectus a aliquet ultrices. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas malesuada lectus ut magna varius, vitae venenatis nisi imperdiet. Nam ut egestas mi, ut pretium arcu.</p>
                    
            <div class="row">
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
              <div class="col-1">
                <span>1</span>
              </div>
            </div>
            
            <div class="row">
              <div class="col-2">
                <span>2</span>
              </div>
              <div class="col-2">
                <span>2</span>
              </div>
              <div class="col-2">
                <span>2</span>
              </div>
              <div class="col-2">
                <span>2</span>
              </div>
              <div class="col-2">
                <span>2</span>
              </div>
              <div class="col-2">
                <span>2</span>
              </div>
            </div>
            
            <div class="row">
              <div class="col-3">
                <span>3</span>
              </div>
              <div class="col-3">
                <span>3</span>
              </div>
              <div class="col-3">
                <span>3</span>
              </div>
              <div class="col-3">
                <span>3</span>
              </div>
            </div>
            
            <div class="row">
              <div class="col-4">
                <span>4</span>
              </div>
              <div class="col-4">
                <span>4</span>
              </div>
              <div class="col-4">
                <span>4</span>
              </div>
            </div>
            
            <div class="row">
              <div class="col-6">
                <span>6</span>
              </div>
              <div class="col-6">
                <span>6</span>
              </div>
            </div>
            
            <p>hasellus nulla nulla, fringilla rhoncus imperdiet ac, luctus id diam. Donec vel sapien feugiat, placerat tellus vel, pulvinar mi. Cras sed pulvinar velit. Vivamus eget finibus erat. Nulla eu consequat enim. Proin eget erat aliquet, pretium magna ac, scelerisque arcu. Integer ornare lorem ligula, sollicitudin dignissim ipsum iaculis vel. Sed facilisis nulla leo. Duis efficitur egestas eros in vehicula.</p>
            
            <p>Nulla facilisi. Quisque cursus venenatis dolor, id porta justo cursus et. Donec urna felis, congue vel bibendum non, ultricies ut massa. Suspendisse semper augue velit, nec blandit enim iaculis a. Nulla vel egestas ligula. Morbi mi neque, luctus et nisi accumsan, tempus scelerisque libero. Nunc quis suscipit eros. In velit leo, tempus nec neque id, tempor tempor sapien. Pellentesque pharetra magna in facilisis imperdiet. Proin scelerisque rutrum neque eu dapibus.</p>
            
            <p>Vivamus consectetur velit vestibulum, dictum quam et, convallis enim. Vestibulum molestie arcu dolor, eu rutrum dui laoreet sit amet. Nullam luctus auctor augue, eu tempus ex efficitur vel. Phasellus sit amet mattis tortor. Aenean augue tortor, porttitor sed maximus vel, maximus id diam. Aliquam pharetra dolor risus, non tincidunt tortor dignissim at. Proin mollis volutpat metus, ac fringilla elit faucibus quis. Maecenas hendrerit aliquam ipsum, quis vehicula quam rutrum nec.</p>
            
            <p>Maecenas aliquet ipsum a porttitor tincidunt. Proin sagittis lectus gravida diam hendrerit accumsan. Quisque pharetra dapibus nulla, a tempor felis malesuada non. Morbi dapibus nibh a leo consequat, vitae vulputate lorem gravida. Morbi aliquam nunc quis leo varius dictum. Donec feugiat fermentum lorem, non vehicula massa fermentum vel. Vestibulum a mauris erat. Morbi pretium lobortis elementum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            
            <p>Pellentesque interdum, dui eu facilisis sollicitudin, diam ipsum lobortis augue, malesuada fermentum nunc tellus eu nibh. Aliquam erat volutpat. In ullamcorper nisl id commodo laoreet. Suspendisse a nisl vitae enim aliquet dapibus. Donec mollis, arcu in semper pellentesque, metus mauris mattis lorem, vel commodo odio est a diam. Quisque volutpat ligula non felis elementum, eget suscipit dolor pretium. Phasellus aliquet ac nisl a pretium. Vivamus et aliquam velit, non malesuada quam. Vivamus congue molestie nisl eu vestibulum. Maecenas ut sodales ligula.</p>
          
        </div>
        <!-- /.grid -->
        
      </div>
    </div>
  </div>
</div>
