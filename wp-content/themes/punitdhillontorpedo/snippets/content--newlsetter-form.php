
  
<div class="form__main">
  <div class="form__field form__field--input form__field--text">
    <input type="text" name="name" placeholder="Name" value="" />
  </div>
  <div class="form__field form__field--input form__field--email">
    <input type="email" name="_replyto" placeholder="Email" value="" />
  </div>
</div>

<div class="form__action">
  <button class="form__button cta button button--outline button--rounded" type="submit">Sign Up</button>
</div>
