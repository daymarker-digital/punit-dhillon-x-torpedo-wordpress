<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////
  ////  Snippet
  //////////////////////////////////////////////////////////

  $excerpt = false;

  if ( isset( $post ) ) {
    if ( get_the_excerpt() ) {

      $excerpt = get_the_excerpt();

      if ( isset($trim_length) && $trim_length ) {
        $excerpt = trim_string( $excerpt, $trim_length );
      }

      echo '<div class="article__excerpt rte">';
        echo '<p>' . $excerpt . '</p>';
      echo '</div>';
    }
  }

  // reset var
  $trim_length = false;

?>
