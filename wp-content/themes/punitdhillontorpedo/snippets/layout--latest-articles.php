<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $assets_dir = $VP->get_theme_directory('assets');

  //////////////////////////////////////////////////////////
  ////  Template Vars
  //////////////////////////////////////////////////////////

  $slider_button_next_icon = $assets_dir . '/img/icon/PUNIT--web-assets--icon--next--';
  $cat = $cat_term = $featured_article_id = false;

  if ( is_category() ) {
    $cat = get_queried_object();
    $cat_term = get_term( $cat->term_id );
  }

  $args = array(
    'post_status' => array( 'publish' ),
    'posts_per_page' => '20',
    'order' => 'DESC',
    'orderby' => 'date',
  );

  if ( $cat ) {
    $args['category_name'] = $cat->slug;
  }

  if ( isset( $curated_article_ids ) && !empty( $curated_article_ids ) ) {
    $args['post__not_in'] = $curated_article_ids;
  }

  $query = new WP_Query( $args );

  //////////////////////////////////////////////////////////
  ////  Template Layout
  //////////////////////////////////////////////////////////

  if ( $query->have_posts() ) {

    $category_count = 0;

    echo '<div class="latest-articles">';

      echo '<div class="glide js--slider" id="latest-articles--slider">';

        echo '<div class="glide__track" data-glide-el="track">';
          echo '<ul class="glide__slides">';
            while ( $query->have_posts() ) {

              // init data
              $query->the_post();

              // default data
              $featured_image = $permalink = $title = false;
              $cats = get_the_category();

              // get data
              if ( get_the_title() ) {
                $title = get_the_title();
              }
              if ( get_the_permalink() ) {
                $permalink = get_the_permalink();
              }
              if ( $VP->get_featured_image_by_post_id() ) {
                $featured_image = $VP->get_featured_image_by_post_id();
              }

              // print data
              echo '<li class="glide__slide" data-index="' . $category_count . '">';

                echo '<article class="latest-articles__article article">';
                  echo '<div class="container-fluid">';
                    echo '<div class="row">';

                      echo '<div class="col-12 col-lg-6">';
                        if ( $featured_image ) {
                          echo '<div class="article__image">';
                            echo $VP->render_lazyload_image( $featured_image, [ 'background' => true ] );
                          echo '</div>';
                        }
                      echo '</div>';

                      echo '<div class="col-12 col-lg-6">';
                        echo '<div class="article__main">';

                          if ( $cats ) {
                            echo '<ul class="article__cat-list article__cat-list--icon">';
                              $cat = $cats[0];
                              $cat_name = $cat->name;
                              $cat_id = $cat->term_id;
                              $cat_term = get_term( $cat_id );
                              $cat_icon_field = 'icon';
                              if ( ! $is_cat ) {
                                $cat_icon_field = 'icon_dark';
                              }
                              if ( get_field( $cat_icon_field, $cat_term ) ) {
                                $icon = get_field( $cat_icon_field, $cat_term );
                                $icon = $icon['url'];
                              }
                              if ( $icon && $cat_name ) {
                                echo '<li class="article__cat article__cat--icon">';
                                  echo '<span class="article__cat-icon">';
                                    echo '<img src="' . $icon . '" alt="' . $cat_name . ' Icon" />';
                                  echo '</span>';
                                  echo '<span class="article__cat-name">';
                                    echo $cat_name;
                                  echo '</span>';
                                echo '</li>';
                              }
                            echo '</ul>';
                          }

                          echo '<h2 class="latest-articles__heading heading heading--title article__title"><a href="' . $permalink . '">' . $title . '</a></h2>';

                          $trim_length = 300;
                          include( locate_template( './snippets/article--excerpt.php' ) );

                          echo '<div class="article__meta">';
                            include( locate_template( './snippets/article--post-meta.php' ) );
                            echo '<a href="' . $permalink . '">More</a>';
                          echo '</div>';

                        echo '</div>';
                      echo '</div>';

                    echo '</div>';
                  echo '</div>';
                echo '</article>';

              echo '</li>';

              // increment counter
              $category_count++;

            }
          echo '</ul>';
        echo '</div>';

        echo '<button class="latest-articles__button glide__button glide__button--next" type="button" data-action=">">';
          echo '<img src="' . $slider_button_next_icon . '" alt="Next" />';
        echo '</button>';

      echo '</div>';

    echo '</div>';

  }

  // Restore original Post Data
  wp_reset_postdata();

?>
