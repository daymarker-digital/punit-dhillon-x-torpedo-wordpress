<?php
  // check for success action
  if ( !isset($success_action) && empty($success_action) ) {
    $success_action = "none";
  }
?>

<form class="form js--validate-me" action="https://formspree.io/f/moqpjnbn" method="POST" data-success-action="<?php echo $success_action; ?>">
  
  <div class="form__main">
    <div class="form__row">
      <div class="form__field form__field--input form__field--text">
        <input class="required" type="text" name="name" placeholder="Name" value="" />
        <span class="form__error">Invalid Field(s)</span>
      </div>
    </div>
    <div class="form__row">
      <div class="form__field form__field--input form__field--email">
        <input class="required" type="email" name="_replyto" placeholder="Email" value="" />
        <span class="form__error">Invalid Field(s)</span>
      </div>
    </div>
    <div class="form__row form__row--rude">
      <div class="form__field form__field--input form__field--rude">
        <label>Rude</label>
        <input class="rude" type="text" name="rude">
      </div>
    </div>
  </div>
  
  <div class="form__action">
    <button class="form__button cta button button--outline button--rounded" type="submit">Sign Up</button>
  </div>
  
</form>

<?php
  // reset action
  $success_action = false; 
?>
