<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Template Vars
  //////////////////////////////////////////////////////////

  $page_id = false;
  $cat = $cat_term = $is_cat = false;

  $curated = [];
  $curated_article_ids = [];
  $curated_article_headings = [];
  $class_modifiers = '';
  $icon_file_suffix = "black.svg";

  if ( is_category() ) {
    $is_cat = true;
    $cat = get_queried_object();
    $cat_term = get_term( $cat->term_id );
    $page_id = $cat_term;
    $class_modifiers = ' articles--category articles--' . $cat->slug;
    $icon_file_suffix = "white.svg";
  } else {
    $page_id = $post->ID;
  }

  if ( have_rows( 'curated', $page_id ) ) {
    while ( have_rows( 'curated', $page_id ) ) {

      // init curated data
      the_row();

      if ( have_rows( 'article' ) ) {
        while ( have_rows( 'article' ) ) {

          // init article data
          the_row();

          // default article data
          $heading = $post_id = '';

          if ( get_sub_field( 'heading' ) ){
            $heading = get_sub_field( 'heading' );
          }
          if ( get_sub_field( 'post_id' ) ){
            $post_id = get_sub_field( 'post_id' );
            $current_post = get_post( $post_id );
          }

          array_push( $curated, array(
            'type' => $current_post->post_type,
            'heading' => $heading,
            'id' => $post_id
          ) );
          array_push( $curated_article_headings, $heading );
          array_push( $curated_article_ids, $post_id );

        }
      }

    }
  }

  echo '<div class="articles' . ( $class_modifiers ? $class_modifiers : '' ) . '">';

    // * IMPORTANT *
    // This snippet takes $curated_article_ids[] and omits posts based on array of post IDs
    include( locate_template( 'snippets/layout--latest-articles.php' ) );

    if ( !empty( $curated ) ) {

      $article_count = count( $curated );

      echo '<div class="curated-articles">';
        echo '<div class="container-fluid">';
          echo '<div class="row">';

            foreach ( $curated as $index => $item ) {

              // default data
              $columns = 'col-12 col-md-6 col-lg-3';
              $trim_length = 100;
              $excerpt = $featured_image = $heading = $permalink = false;
              $icon = $assets_dir . '/img/icon/';
              $a_post = get_post( $item['id'] );

              // layout data
              if ( $index == ( $article_count - 1 ) ) {
                $columns = 'col-12 col-lg-6';
                $trim_length = 200;
              }

              // post data
              if ( 'wps_products' == $item['type'] ) {
                if ( $a_post->post_content ) {
                  $excerpt = apply_filters( 'the_content', $a_post->post_content );
                }
              } else {
                if ( get_the_excerpt( $item['id'] ) ) {
                  $excerpt = get_the_excerpt( $item['id'] );
                }
              }

              if ( 'wps_products' == $item['type'] ) {

                if ( have_rows( 'featured_image', $item['id'] ) ) {
                  while ( have_rows( 'featured_image', $item['id'] ) ) {

                    // init data
                    the_row();

                    // get data
                    if ( get_sub_field( 'mobile' ) ) {
                      $featured_image = get_sub_field( 'mobile' );
                    }

                  }
                }

              } else {
                if ( $VP->get_featured_image_by_post_id( $item['id'] ) ) {
                  $featured_image = $VP->get_featured_image_by_post_id( $item['id'] );
                }
              }

              if ( isset( $item['heading'] ) && !empty( $item['heading'] ) ) {
                $heading = $item['heading'];
              } else {
                $heading = get_the_title( $item['id'] );
              }
              if ( get_permalink( $item['id'] ) ) {
                $permalink = get_permalink( $item['id'] );
              }

              switch ( $index ) {
                case 0:
                  // product
                  $icon .= 'PUNIT--web-assets--icon--shop--' . $icon_file_suffix;
                  break;
                case 1:
                  // about
                  $icon .= 'PUNIT--web-assets--icon--punit--' . $icon_file_suffix;
                  break;
                case 2:
                  // article category
                  $cats = get_the_category( $item['id'] );
                  if ( $cats ) {
                    $cat = $cats[0];
                    $cat_id = $cat->term_id;;
                    $cat_term = get_term( $cat_id );
                    $cat_icon_field = 'icon';
                    if ( ! $is_cat ) {
                      $cat_icon_field = 'icon_dark';
                    }
                    if ( get_field( $cat_icon_field, $cat_term ) ) {
                      $icon = get_field( $cat_icon_field, $cat_term );
                      $icon = $icon['url'];
                    }
                  }
                  break;
              }

              // print data
              echo '<div class="' . $columns . '">';
                echo '<div class="curated-articles__article article">';

                  if ( $heading ) {
                    echo '<h2 class="curated-articles__heading heading heading--secondary">';
                      echo '<span class="curated-articles__heading-copy"><a href="' . $permalink . '">' . $heading . '</a></span>';
                      echo '<span class="curated-articles__heading-icon"><img src="' . $icon . '" alt="Icon" /></span>';
                    echo '</h2>';
                  }

                  if ( $featured_image ) {
                    echo '<div class="curated-articles__image">';
                      echo $VP->render_lazyload_image( $featured_image, [ 'background' => true ] );
                    echo '</div>';
                  }

                  if ( $excerpt ) {
                    echo '<div class="curated-articles__excerpt"><p>' . trim_string( $excerpt, $trim_length ) . '</p></div>';
                  }

                  if ( $permalink ) {
                    echo '<div class="curated-articles__cta article__cta">';
                      echo '<a href="' . $permalink . '">More</a>';
                    echo '</div>';
                  }

                echo '</div>';
              echo '</div>';

            } // endforeach

          echo '<div>';
        echo '</div>';
      echo '</div>';
    }

  echo '</div>';

?>
