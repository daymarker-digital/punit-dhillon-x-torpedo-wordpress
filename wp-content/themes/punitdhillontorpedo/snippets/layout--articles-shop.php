<?php
  
  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////
  
  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();
  
  //////////////////////////////////////////////////////////
  ////  Template Vars
  //////////////////////////////////////////////////////////

  if ( have_rows( 'shop' ) ) {
    while ( have_rows( 'shop' ) ) {
      
      // init data
      the_row();
      
      // default data
      $excerpt = $heading = $post_id = $product_id = $product_image = $product_permalink = $product_post = $product_title = false;
  
      // get data
      if ( get_sub_field( 'product' ) ){
        $post_id = get_sub_field( 'product' );
        $product_post = get_post( $post_id );
      }

      if ( get_sub_field( 'heading' ) ){
        $heading = get_sub_field( 'heading' );
      } else {
        $heading = $product_title;
      }

      $WP_Shopify_Product = WP_Shopify\Factories\DB\Products_Factory::build();
      $product_id = $WP_Shopify_Product->get_product_ids_from_titles([$product_title]);
      $product = $WP_Shopify_Product->get_product_from_product_id($product_id);

      if ( isset( $product[0]->image ) && !empty( $product[0]->image ) ) {
        $product_image = $product[0]->image;
      }

      // print data
      echo '<div class="articles-shop">';  
        if ( $heading ) {
          echo '<h2 class="articles-shop__heading heading heading--secondary">' . $heading . '</h2>';
        }
        if ( $product_image ) {
          echo '<div class="articles-shop__image">';
            echo '<img 
            class="lazyload-item lazyload-item--image lazyload-item--inline lazyload" 
            data-src="' . $product_image . '"
            src=""
            alt="' . $product_title . '" />';
          echo '</div>';
        }
        if ( $excerpt ) {
          echo '<div class="articles-shop__excerpt"><p>' . trim_string($excerpt, 90) . '</p></div>';
        }
        if ( $product_permalink ) {
          echo '<div class="articles-shop__cta">';
            echo '<a href="' . $product_permalink . '">More</a>';
          echo '</div>';
        }
      echo '</div>';  
      
    }
  }
  
?>
