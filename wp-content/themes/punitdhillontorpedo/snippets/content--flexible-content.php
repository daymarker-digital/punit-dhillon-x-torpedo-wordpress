<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Content
  //////////////////////////////////////////////////////////

  if ( have_rows('content') ) {
    while ( have_rows('content') ) {

      // init data
      the_row();

      // get row layout
      $row_layout = get_row_layout();

      // conditionally load layout
      switch ($row_layout) {
        case "blockquote":
          include( locate_template( './snippets/layout--blockquote.php' ) );
          break;
        case "image-feature":
          include( locate_template( './snippets/layout--image-feature.php' ) );
          break;
        case "image-text":
          include( locate_template( './snippets/layout--image-and-text.php' ) );
          break;
        case "text-links":
          include( locate_template( './snippets/layout--text-and-links.php' ) );
          break;
      }

    }
  }

?>
