<?php
  
  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////
  
  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();
  
  //////////////////////////////////////////////////////////
  ////  Section | Text & Links
  //////////////////////////////////////////////////////////
      
  if ( have_rows( 'text_links' ) ) {
    while ( have_rows( 'text_links' ) ) {

      // init data
      the_row();

      // default data
      $heading = $message = $theme = false;

      // get data
      if ( get_sub_field('heading') ) {
        $heading = get_sub_field('heading');
      }
      if ( get_sub_field('message') ) {
        $message = get_sub_field('message');
      }
      if ( get_sub_field('theme') ) {
        $theme = get_sub_field('theme');
      }

      echo '<section class="section section--' . $theme . ' section--image-text text-links">';
        echo '<div class="container-fluid">';
          echo '<div class="row">';
            echo '<div class="col-12 col-lg-10 offset-lg-1">';

              if ( $heading || $message ) { 
                echo '<div class="text-links__content">';
                  if ( $heading ) { 
                    echo '<h2 class="text-links__heading heading heading--title">';
                      echo $heading;
                    echo '</h2>';
                  }
                  if ( $message ) {
                    echo '<div class="text-links__message message rte">';
                      echo $message;
                    echo '</div>';
                  }
                echo '</div>';
              }

              if ( have_rows('links') ) {
                echo '<ul class="text-links__cta-list">';
                while( have_rows('links') ) {
                  // init data
                  the_row();
                  
                  echo '<li class="text-links__cta-item">';
                    $cta_classes = "button button--rounded button--outline";
                    include( locate_template( './snippets/layout--cta.php' ) );
                  echo '</li>';
                  
                }
                echo '</ul>';
              }

            echo '</div>';
          echo '</div>';
        echo '</div>';
      echo '</section>';
      
    }  
  }
      
?>
