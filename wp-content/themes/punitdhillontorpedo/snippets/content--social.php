<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $assets_dir = $VP->get_theme_directory('assets');

  //////////////////////////////////////////////////////////
  ////  Snippet Vars
  //////////////////////////////////////////////////////////

  $likes = $post_id = 0;

  if ( get_field("likes") ) {
    $likes = get_field("likes");
  }
  if ( get_the_ID() ) {
    $post_id = get_the_ID();
  }

  $img_src = $assets_dir . '/img/icon/PUNIT--web-assets--icon--likes--black.svg';


?>

<div class="social">

  <button class="social__share cta button button--outline button--rounded js--share" type="button">Share</button>

  <form class="social__likes js-trigger--likes" action="<?php echo $home; ?>/wp-admin/admin-ajax.php" data-form-id="form-id--<?php echo $post_id; ?>">

    <div class="social__likes-icon">
      <img src="<?php echo $img_src; ?>" alt="Likes" />
    </div>

    <div class="social__likes-counter">
      <?php if ( $likes ) : echo $likes; endif; ?>
    </div>

    <div class="social__likes-inputs">
      <input type="text" name="rude" value="" >
      <input type="text" name="likes" value="<?php echo $likes; ?>" disabled>
      <input type="text" name="post-id" value="<?php echo $post_id; ?>" disabled>
    </div>

  </form>

</div>
<!-- /.social -->
