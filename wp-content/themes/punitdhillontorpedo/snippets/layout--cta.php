<?php
  
  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////
  
  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();
  
  //////////////////////////////////////////////////////////
  ////  Snippet Vars
  //////////////////////////////////////////////////////////
  
  if ( !isset( $cta_classes ) && empty( $cta_classes ) ) {
    $cta_classes = "cta";
  } else {
    $cta_classes = "cta " . $cta_classes;
  }
  
  if ( !isset( $cta_email_subject ) && empty( $cta_email_subject ) ) {
    $cta_email_subject = false;
  }
  
  //////////////////////////////////////////////////////////
  ////  Section
  //////////////////////////////////////////////////////////
      
  if ( have_rows( 'cta' ) ) {
    while ( have_rows( 'cta' ) ) {
        
      // init data
      the_row();
      
      // default data
      $link = $title = $type = false;
      $link_prefix = $link_target = $link_subject = '';
      
      // get data
      if ( get_sub_field( 'title') ) {
        $title = get_sub_field( 'title');
      }
      if ( get_sub_field( 'type') ) {
        $type = get_sub_field( 'type');
      }
           
      switch ( $type ) {
        case 'internal':
          $field = 'link_internal';
          break;
        case "email":
          $field = 'link_email';
          $link_prefix = 'mailto:';
          if ( $cta_email_subject ) {
            $link_subject = '?subject=' . urlencode( $cta_email_subject ); 
          }
          break;
        case "external":
          $field = 'link_external';
          $link_target = 'target="_blank" rel="noreferrer noopener"';
          break;
      }
      
      if ( get_sub_field( $field ) ) {
        $link = get_sub_field( $field );
        if ( $link_prefix ) {
          $link = $link_prefix . $link;
          if ( $link_subject ) {
            $link .= $link_subject;
          }
        }
        
      }
      
      if ( $link && $title ) {
        echo '<a class="' . $cta_classes . '" href="' . $link . '" ' . ( $link_target ? $link_target : '' ) . '>' . $title . '</a>';
      }

    }
  }
    
?>
