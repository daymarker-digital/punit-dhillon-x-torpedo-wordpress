<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Template Vars
  //////////////////////////////////////////////////////////

  $curated_posts = [];
  $posts_to_exclude = [];
  $cats_to_exclude = [];
  $cats_to_include = [];
  $tax_query = [];
  $query_args = [
    'post_status' => [ 'publish' ],
    'posts_per_page' => '20',
    'order' => 'DESC',
    'orderby' => 'date',
  ];

  // ---------------------------------------- Build Query Params to Exclude by Post ID

  if ( have_rows( 'curated' ) ) {
    while ( have_rows( 'curated' ) ) {

      // init curated data
      the_row();

      // default data
      $curated_post_heading = $curated_post_icon  = $curated_post_id = $curated_post_permalink = $curated_post_object = $curated_post_type = false;

      // get data
      if ( get_sub_field( 'icon' ) ) {
        $curated_post_icon = get_sub_field( 'icon' );
        $curated_post_icon = $curated_post_icon['url'];
      }
      if ( have_rows( 'article' ) ) {
        while ( have_rows( 'article' ) ) {

          // init data
          the_row();

          // get data
          if ( get_sub_field( 'post_id' ) ) {
            $curated_post_id = get_sub_field( 'post_id' );
            $curated_post_object = get_post( $curated_post_id );
            $curated_post_type = $curated_post_object->post_type;
            $curated_post_permalink = $curated_post_object->guid;
          }
          if ( get_sub_field( 'heading' ) ) {
            $curated_post_heading = get_sub_field( 'heading' );
          } else {
            if ( get_the_title( $curated_post_id ) ) {
              $curated_post_heading = get_the_title( $curated_post_id );
            }
          }

        }
      }

      // collect data
      array_push( $posts_to_exclude, $curated_post_id );
      array_push( $curated_posts, [
        'heading' => $curated_post_heading,
        'icon' => $curated_post_icon,
        'id' => $curated_post_id,
        'type' => $curated_post_type,
        'permalink' => $curated_post_permalink,
        'object' => $curated_post_object
      ] );

    }
  }

  if ( !empty( $posts_to_exclude ) ) {
    $query_args['post__not_in'] = $posts_to_exclude;
  }

  // ---------------------------------------- Build Query Params based on Categories

  if ( have_rows( 'categories' ) ) {
    while ( have_rows( 'categories' ) ) {

      // init curated data
      the_row();

      if ( get_sub_field( 'include' ) ) {
        $cats_to_include = get_sub_field( 'include' );
        array_push($tax_query, array(
          'taxonomy' => 'category',
          'field'    => 'term_id',
          'terms'    => $cats_to_include,
          'operator' => 'IN',
        ));
      }

      if ( get_sub_field( 'exclude' ) ) {
        $cats_to_exclude = get_sub_field( 'exclude' );
        array_push($tax_query, array(
          'taxonomy' => 'category',
          'field'    => 'term_id',
          'terms'    => $cats_to_exclude,
          'operator' => 'NOT IN',
        ));
      }

      if ( count($tax_query) > 1 ) {
        $tax_query['relation'] = 'AND';
      }

    }
  }

  if ( !empty( $tax_query ) ) {
    $query_args['tax_query'] = $tax_query;
  }

  // ---------------------------------------- Build WP Query

  $query = new WP_Query( $query_args );

  //////////////////////////////////////////////////////////
  ////  Latest Articles
  //////////////////////////////////////////////////////////

  if ( $query->have_posts() ) {

    $article_count = 0;

    echo '<div class="latest-articles" data-colour-theme="' . $page_colour_theme . '">';

      echo '<div class="glide js--slider" id="latest-articles--slider">';

        echo '<div class="glide__track" data-glide-el="track">';
          echo '<ul class="glide__slides">';

            //////////////////////////////////////////////////////////
            ////  Masthead
            //////////////////////////////////////////////////////////

            if ( have_rows( 'masthead' ) ) {
              while ( have_rows( 'masthead' ) ) {

                // init data
                the_row();

                // default data
                $enable = false;
                $masthead_graphic = $masthead_message = false;

                // get data
                if ( get_sub_field( 'enable' ) ) {
                  $enable = get_sub_field( 'enable' );
                }
                if ( get_sub_field( 'graphic' ) ) {
                  $masthead_graphic = get_sub_field( 'graphic' );
                }
                if ( get_sub_field( 'message' ) ) {
                  $masthead_message = get_sub_field( 'message' );
                }

                if ( $enable ) {
                  echo '<li class="glide__slide">';
                    echo '<article class="masthead">';
                      echo '<div class="container-fluid">';
                        echo '<div class="row">';
                          echo '<div class="col-12">';

                            echo '<div class="masthead__main">';

                              if ( $masthead_graphic ) {
                                echo '<div class="masthead__image">';
                                  echo $VP->render_lazyload_image( $masthead_graphic );
                                echo '</div>';
                              }

                              if ( $masthead_message ) {
                                echo '<div class="article__message masthead__message rte">';
                                  echo $masthead_message;
                                echo '</div>';
                              }

                            echo '</div>';

                          echo '</div>';
                        echo '</div>';
                      echo '</div>';
                    echo '</article>';
                  echo '</li>';
                }

              }
            }

            //////////////////////////////////////////////////////////
            ////  Posts
            //////////////////////////////////////////////////////////

            while ( $query->have_posts() ) {

              // init data
              $query->the_post();

              // default data
              $post_issue = 0;
              $featured_image = $permalink = $title = false;
              $post_id = get_the_ID();

              // get data
              if ( get_the_title() ) {
                $title = get_the_title();
              }
              if ( get_the_permalink() ) {
                $permalink = get_the_permalink();
              }
              if ( $VP->get_featured_image_by_post_id() ) {
                $featured_image = $VP->get_featured_image_by_post_id();
              }
              if ( get_field( 'issue' ) ) {
                $post_issue = get_field( 'issue' );
              }

              // print data
              echo '<li class="glide__slide" data-index="' . $article_count . '">';

                echo '<article class="latest-articles__article article">';
                  echo '<div class="container-fluid">';
                    echo '<div class="row">';

                      echo '<div class="col-12 col-lg-6">';
                        if ( $featured_image ) {
                          echo '<div class="article__image">';
                            echo $VP->render_lazyload_image( $featured_image, [ 'background' => true ] );
                          echo '</div>';
                        }
                      echo '</div>';

                      echo '<div class="col-12 col-lg-6">';
                        echo '<div class="article__main">';

                          $args = [
                            'post_id' => $post_id,
                            'colour_theme' => $page_colour_theme,
                            'issue' => $post_issue
                          ];
                          echo $VP->render_article_category( $args );

                          echo '<h2 class="latest-articles__heading heading heading--title article__title">';
                            echo '<a href="' . $permalink . '">' . $title . '</a>';
                          echo '</h2>';

                          $args = [
                            'post_id' => $post_id,
                            'trim_length' => 300
                          ];
                          echo $VP->render_article_excerpt( $args );

                          echo '<div class="article__meta">';
                            include( locate_template( './snippets/article--post-meta.php' ) );
                            echo '<a href="' . $permalink . '">More</a>';
                          echo '</div>';

                        echo '</div>';
                      echo '</div>';

                    echo '</div>';
                  echo '</div>';
                echo '</article>';

              echo '</li>';

              $article_count++;

            }
          echo '</ul>';
        echo '</div>';

        $args = [
          'colour_theme' => $page_colour_theme,
        ];
        echo $VP->render_latest_articles_button_next( $args );

      echo '</div>';

    echo '</div>';

  }

  // Restore original Post Data
  wp_reset_postdata();

  //////////////////////////////////////////////////////////
  ////  Curated Articles
  //////////////////////////////////////////////////////////

  if ( !empty( $curated_posts ) ) {

    echo '<div class="curated-articles" data-colour-theme="' . $page_colour_theme . '">';
      echo '<div class="container-fluid">';
        echo '<div class="row">';

          foreach( $curated_posts as $index => $post ) {

            // default data
            $columns = 'col-12 col-md-6 col-lg-3';
            $trim_length = 100;
            $curated_excerpt = $curated_featured_image = $curated_heading = $curated_permalink = false;
            $curated_post_type = $curated_post_object = $curated_post_id = false;
            $curated_post_icon = false;

            // conditional default data
            if ( 2 == $index ) {
              $columns = 'col-12 col-lg-6';
              $trim_length = 200;
            }

            // get data
            if ( isset( $post['icon'] ) && !empty( $post['icon'] ) ) {
              $curated_post_icon = $post['icon'];
            }
            if ( isset( $post['id'] ) && !empty( $post['id'] ) ) {
              $curated_post_id = $post['id'];
            }
            if ( isset( $post['heading'] ) && !empty( $post['heading'] ) ) {
              $curated_heading = $post['heading'];
            }
            if ( isset( $post['permalink'] ) && !empty( $post['permalink'] ) ) {
              $curated_permalink = $post['permalink'];
            }
            if ( isset( $post['type'] ) && !empty( $post['type'] ) ) {
              $curated_post_type = $post['type'];
            }
            if ( isset( $post['object'] ) && !empty( $post['object'] ) ) {
              $curated_post_object = $post['object'];
            }

            if ( 'wps_products' == $curated_post_type ) {
              if ( $curated_post_object->post_content ) {
                $curated_excerpt = apply_filters( 'the_content', $curated_post_object->post_content );
              }
              if ( have_rows( 'featured_image', $curated_post_id ) ) {
                while ( have_rows( 'featured_image', $curated_post_id ) ) {

                  // init data
                  the_row();

                  // get data
                  if ( get_sub_field( 'mobile' ) ) {
                    $curated_featured_image = get_sub_field( 'mobile' );
                  }

                }
              }
            } else {
              if ( $VP->get_featured_image_by_post_id( $curated_post_id ) ) {
                $curated_featured_image = $VP->get_featured_image_by_post_id( $curated_post_id );
              }
              if ( get_the_excerpt( $curated_post_id ) ) {
                $curated_excerpt = get_the_excerpt( $curated_post_id );
              }
            }

            // print data
            echo '<div class="' . $columns . '">';
            echo '<div class="curated-articles__article article">';

              if ( $curated_heading ) {
                echo '<h2 class="curated-articles__heading heading heading--secondary">';
                  echo '<span class="curated-articles__heading-copy"><a href="' . $curated_permalink . '">' . $curated_heading . '</a></span>';
                  if ( $curated_post_icon ) {
                    echo '<span class="curated-articles__heading-icon"><img src="' . $curated_post_icon . '" alt="Icon" /></span>';
                  }
                echo '</h2>';
              }

              if ( $curated_featured_image ) {
                echo '<div class="curated-articles__image">';
                  echo $VP->render_lazyload_image( $curated_featured_image, [ 'background' => true ] );
                echo '</div>';
              }

              if ( $curated_excerpt ) {
                echo '<div class="curated-articles__excerpt"><p>' . trim_string( $curated_excerpt, $trim_length ) . '</p></div>';
              }

              if ( $curated_permalink ) {
                echo '<div class="curated-articles__cta article__cta">';
                  echo '<a href="' . $curated_permalink . '">More</a>';
                echo '</div>';
              }

            echo '</div>';
            echo '</div>';
          }

        echo '</div>';
      echo '</div>';
    echo '</div>';

  } // if 'curated'

?>
