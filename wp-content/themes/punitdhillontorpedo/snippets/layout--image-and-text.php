<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $VP = new PDTheme();
  $home = $VP->get_theme_directory('home');
  $assets_dir = $VP->get_theme_directory('assets');
  $theme_dir = $VP->get_theme_directory();

  //////////////////////////////////////////////////////////
  ////  Section | Image & Text
  //////////////////////////////////////////////////////////

  if ( have_rows( 'image_text' ) ) {
    while ( have_rows( 'image_text' ) ) {

      // init data
      the_row();

      // default data
      $heading = $image = $layout = $message = $theme = false;

      // get data
      if ( get_sub_field('heading') ) {
        $heading = get_sub_field('heading');
      }
      if ( get_sub_field('image') ) {
        $image = get_sub_field('image');
      }
      if ( get_sub_field('layout') ) {
        $layout = get_sub_field('layout');
      }
      if ( get_sub_field('message') ) {
        $message = get_sub_field('message');
      }
      if ( get_sub_field('theme') ) {
        $theme = get_sub_field('theme');
      }

      echo '<section class="section section--' . $theme . ' section--image-text image-text image-text--' . $layout . '">';

        echo '<div class="container-fluid">';
          echo '<div class="row">';

            echo '<div class="vr d-none d-lg-block"></div>';

            echo '<div class="col-12 col-lg-6 ' . ( $layout == 'image-right' ? 'order-lg-2' : '' ) . '">';
              if ( $image ) {
                echo '<div class="image-text__image image">';
                  echo $VP->render_lazyload_image( $image );
                echo '</div>';
              }
            echo '</div>';

            echo '<div class="col-12 col-lg-6 ' . ( $layout == 'image-right' ? 'order-lg-1' : '' ) . '">';
              if ( $heading || $message ) {

                echo '<div class="image-text__main">';

                  if ( $heading ) {
                    echo '<h2 class="image-text__heading heading heading--title">';
                      echo $heading;
                    echo '</h2>';
                  }

                  if ( $message ) {
                    echo '<div class="image-text__message message rte">';
                      echo $message;
                    echo '</div>';
                  }

                  $cta_classes = "button button--rounded button--outline";
                  include( locate_template( './snippets/layout--cta.php' ) );

                echo '</div>';

              }
            echo '</div>';

          echo '</div>';
        echo '</div>';

      echo '</section>';

    }
  }

?>
