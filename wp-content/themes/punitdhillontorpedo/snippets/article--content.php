<?php

  //////////////////////////////////////////////////////////
  ////  Polite Department Vars
  //////////////////////////////////////////////////////////

  $post_type = get_post_type();

  //////////////////////////////////////////////////////////
  ////  Snippet
  //////////////////////////////////////////////////////////

  if ( isset( $post ) ) {
    if ( have_rows( 'content' ) ) {

      echo '<div class="article__content article__content--' . $post_type . '">';

        // Loop through rows.
        while ( have_rows( 'content' ) ) {

          // init data
          the_row();

          // get row layout
          $row = get_row_layout();

          // conditionally show layout
          switch ( $row ) {
            case "images":
              include( locate_template( './snippets/layout--images.php' ) );
              break;
            case "videos":
              include( locate_template( './snippets/layout--videos.php' ) );
              break;
            case "wysiwyg":
              include( locate_template( './snippets/layout--wysiwyg.php' ) );
              break;
          }

        }

      echo '</div>';

    }
  }

?>
