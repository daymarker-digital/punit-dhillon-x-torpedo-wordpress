<?php

class PoliteDepartment {

  /*
  //////////////////////////////////////////////////////////
  ////  Vars
  //////////////////////////////////////////////////////////
  */

  // VC OTHER THEMES
  // RESTYLE CART STUFF

  private $version = '2.0';

  public $custom_image_title = 'custom-image-size';
  public $custom_image_sizes = [ 1, 10, 180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048 ];
  public $is_torpedo = true;

  public $punit = [
    'newsletter_form_url' => 'https://formspree.io/f/xknkepvn',
  ];
  public $torpedo = [
    'newsletter_form_url' => 'https://formspree.io/f/moqpjnbn',
  ];

  /*
  //////////////////////////////////////////////////////////
  ////  Constructor
  //////////////////////////////////////////////////////////
  */

  public function __construct() {}

  /*
  //////////////////////////////////////////////////////////
  ////  Getters
  //////////////////////////////////////////////////////////
  */

  // --------------------------- Featured Image
  public function get_featured_image_by_post_id( $post_id = false ) {

    $image = $image_alt = $image_attributes = $post_thumbnail_id = $sizes = false;

    if ( $post_id ) {

      // get image sizes
      if ( get_intermediate_image_sizes() ) {
        $sizes = get_intermediate_image_sizes();
      }

      // get post thumbnail id
      if ( get_post_thumbnail_id( $post_id ) ) {
        $post_thumbnail_id = get_post_thumbnail_id( $post_id );
      }

      // if image sizes and image id
      if ( $sizes && $post_thumbnail_id ) {

        $image_alt = get_post_meta( $post_thumbnail_id , '_wp_attachment_image_alt', true );
        $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, "full" );

        $image = [
          "url" => get_the_post_thumbnail_url( $post_id ),
          "sizes" => [],
          "alt" => $image_alt,
          "width" => $image_attributes[1],
          "height" => $image_attributes[2],
        ];

        foreach ( $sizes as $index => $size ) {
          $image["sizes"][$size] = wp_get_attachment_image_src( $post_thumbnail_id, $size )[0];
        }

      }

    }

    return $image;

  }

  // --------------------------- Google Maps Directions Link
  public function get_google_maps_directions_link( $params = [] ) {

    $html = '';
    $base = 'https://www.google.com/maps/search/?api=1&query=';

    if ( $params ) {

      // default data
      $name = $city = $country = $region = $postal = $address = $address_2 = false;

      // extract $params
      extract( $params );

      $html .= ( $address ) ? $address : '';
      $html .= ( $city ) ? ' ' . $city : '';
      $html .= ( $region ) ? ' ' . $region : '';
      $html .= ( $postal ) ? ' ' . $postal : '';
      $html .= ( $country ) ? ' ' . $country : '';
      $html .= ( $name ) ? ' ' . $name : '';

      if ( $address_2 ) {
        $html = $address_2 . '–' . $html;
      }

      if ( $html ) {
        $html = $base . trim($html);
      }

    }

    return $html;

  }

  // --------------------------- Theme Classes
  public function get_theme_classes() {

    global $post;
    global $template;

    $classes = '';

  	if ( isset( $post ) ) {

  		$post_ID = $post->ID;
  		$post_type = $post->post_type;
  		$post_slug = $post->post_name;
  		$template = basename( $template, '.php' );

  		$classes .= 'post-type--' . $post_type;
  		$classes .= ' ' . $post_type . '--' . $post_slug;
  		$classes .= ' page-id--' . $post_ID;
  		$classes .= ' template--' . $template;
  		$classes .= ' ' . $template;

      if ( is_front_page() ) {
        $classes .= ' is-front-page';
      }

      if ( is_page() ) {
        $classes .= ' is-page';
      }

      if ( is_page_template( 'page-templates/page--about-us.php') ) {
        $classes .= ' is-about-us-page';
      }

      if ( is_single() ) {
        $classes .= ' is-single';
      }

      if ( is_archive() ) {
        $classes .= ' is-archive';
      }

      if ( is_category() ) {
        $classes .= ' is-category';
      }

  	}

    if ( is_404() ) {
      $classes .= ' is-404';
    }

    if ( $this->is_torpedo ) {
      $classes .= ' is-torpedo';
    }

    if ( is_plugin_active( 'wp-shopify-pro/wp-shopify.php' ) ) {
      $classes .= ' wp-shopify';
    }

  	return $classes;

  }

  // --------------------------- Theme Directory
  public function get_theme_directory( $level = 'base' ) {

    switch ( $level ) {
      case 'assets':
        return get_template_directory_uri() . '/assets';
        break;
      case 'base':
        return get_template_directory_uri();
        break;
      case 'home':
        return get_home_url();
        break;
    }

  }

  // --------------------------- Theme Info
  public function get_theme_info( $param = 'version' ) {

    global $post;
    $html = '';

    switch ( $param ) {
      case 'object_ID':
				$html = get_queried_object_id();
				break;
      case 'php_version':
				$html = phpversion();
				break;
      case 'post_ID':
				// $html = $post->ID;
				break;
			case 'post_type':
				$html = get_post_type( $post->ID );
				break;
      case 'template':
				$html = basename( get_page_template(), ".php" );
				break;
      case 'version':
				$html = $this->version;
				break;
      case 'wp_version':
				$html = get_bloginfo( "version" );
				break;
    }

    return $html;

  }

}

?>
