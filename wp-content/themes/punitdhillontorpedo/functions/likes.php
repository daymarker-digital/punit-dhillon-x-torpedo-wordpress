<?php

//////////////////////////////////////////////////////////
////  Update Likes
//////////////////////////////////////////////////////////

function VP__likes() {

  // default data
  $likes = $post_id = false;

  // conditionally get data
  if ( (isset($_POST['likes'])) && ("" !== $_POST['likes']) ) {
    $likes = $_POST['likes'];
  }

  if ( (isset($_POST['postID'])) && ("" !== $_POST['postID']) ) {
    $post_id = $_POST['postID'];
  }

  // conditionally update ACF
  if ( $likes && $post_id ) {
    update_field( 'likes', $likes, $post_id );
  }

}

add_action( 'wp_ajax_nopriv_VP__likes', 'VP__likes' );
add_action( 'wp_ajax_VP__likes','VP__likes' );

?>
