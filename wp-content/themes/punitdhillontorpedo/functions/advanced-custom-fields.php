<?php

/*
//////////////////////////////////////////////////////////
////  ACF | Options
//////////////////////////////////////////////////////////
*/

function VP__acf_options() {

  $VP = new PDTheme();
  $main_slug = 'theme-settings';

  function VP__custom_options( $title = '', $slug = '' ) {
    if ( $title && $slug ) {
      return [ 'title' => $title, 'slug' => $slug ];
    }
  }

  $custom_options = [
    VP__custom_options( '404', '404' ),
    VP__custom_options( 'Newsletter', 'newsletter' ),
  ];

  if ( $VP->is_torpedo ) {
    array_push( $custom_options, VP__custom_options( 'Contributions', 'contributions' ) );
    array_push( $custom_options, VP__custom_options( 'Issues', 'issues' ) );
  } else {
    array_push( $custom_options,  VP__custom_options( 'Press', 'press' ) );
  }

  if ( function_exists('acf_add_options_page') ) {

  	acf_add_options_page(
  		array(
  			'page_title' => 'Theme Settings',
  			'menu_title' => 'Theme Settings',
  			'menu_slug' => $main_slug,
  			'capability' => 'edit_posts',
  			'parent_slug' => '',
  			'position' => '2.1',
  			'icon_url' => false
  		)
  	);

    foreach( $custom_options as $option ) {

      $slug = $title = '';
      extract( $option );

      if ( $title && $slug ) {

        $args = [
          'page_title' => $title,
    			'menu_title' => $title,
    			'menu_slug' => $main_slug . '-' . $slug,
    			'capability' => 'edit_posts',
    			'parent_slug' => $main_slug,
        ];

        acf_add_options_sub_page( $args );

      }

    }

  }

} // VP__acf_options()

add_action( 'acf/init', 'VP__acf_options' );

/*
//////////////////////////////////////////////////////////
////  ACF | REST
//////////////////////////////////////////////////////////
*/

function create_ACF_meta_in_REST() {

  $postypes_to_exclude = ['acf-field-group','acf-field'];
  $extra_postypes_to_include = ["page"];
  $post_types = array_diff(get_post_types(["_builtin" => false], 'names'),$postypes_to_exclude);
  array_push($post_types, $extra_postypes_to_include);
  foreach ($post_types as $post_type) {
    register_rest_field( $post_type, 'ACF', [
      'get_callback'    => 'expose_ACF_fields',
      'schema'          => null,
    ]);
  }

}

function expose_ACF_fields( $object ) {
  $ID = $object['id'];
  return get_fields($ID);
}

add_action( 'rest_api_init', 'create_ACF_meta_in_REST' );

?>
