<?php

class PDTheme extends PoliteDepartment {

  private $version = '2.0';

  /*
  //////////////////////////////////////////////////////////
  ////  Render
  //////////////////////////////////////////////////////////
  */

  // --------------------------- Articles
  public function render_articles( $params = [] ) {

    $block_name = 'articles';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $html .= '<div class="' . $block_name . '">';

        //////////////////////////////////////////////////////////
        ////  Featured Articles by Custom WP Query
        //////////////////////////////////////////////////////////

        $html .= $this->render_articles_featured([ 'post_id' => $post_id ]);

        //////////////////////////////////////////////////////////
        ////  Curated Content by ACF
        //////////////////////////////////////////////////////////

        $html .= $this->render_articles_curated([ 'post_id' => $post_id ]);

      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Articles | Curated
  public function render_articles_curated( $params = [] ) {

    // ## TODO include WP Post

    $block_name = 'articles';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $curated_articles = get_field( 'curated', $post_id ) ? get_field( 'curated', $post_id ) : [];

      // ---------------------------------------- Template
      $html .= '<div class="'. $block_name . '__curated">';
        $html .= '<div class="container-fluid"><div class="row">';

          foreach ( $curated_articles as $i => $item ) {

            // ---------------------------------------- ACF Data
            $id = ( isset($item['post_id']) && !empty($item['post_id']) ) ? $item['post_id'] : false;
            $icon = ( isset($item['icon']) && !empty($item['icon']) ) ? $item['icon'] : false;
            $heading = ( isset($item['heading']) && !empty($item['heading']) ) ? $item['heading'] : get_the_title( $id );

            // ---------------------------------------- WP Data
            $feature_image = $this->get_featured_image_by_post_id( $id );
            $post_type = get_post_type( $id ) ? get_post_type( $id ) : false;
            $url = get_permalink( $id ) ? get_permalink( $id ) : false;

            // ---------------------------------------- If WP Shopify Product
            if ( 'wps_products' === $post_type ) {
              $feature_image_groupd = get_field( 'featured_image', $id ) ? get_field( 'featured_image', $id ) : [];
              $feature_image = ( isset($feature_image_groupd['mobile']) && !empty($feature_image_groupd['mobile']) ) ? $feature_image_groupd['mobile'] : false;
              // if ( $curated_post_object->post_content ) {
              //   $curated_excerpt = apply_filters( 'the_content', $curated_post_object->post_content );
              // }
            }

            // ---------------------------------------- Layout
            $columns = ( $i > 1 ) ? 'col-12 col-lg-6' : 'col-12 col-md-6 col-lg-3';
            $trim_length = ( $i > 1 ) ? 200 : 100;

            if ( $heading && $url ) {
              $html .= '<div class="' . $columns . '">';
                $html .= '<article class="article" data-style="curated" data-post-id="' . $id . '" data-post-type="' . $post_type . '">';

                  $html .= '<h2 class="article__title title title--article-curated">';
                    $html .= '<a href="' . $url . '" target="_self">' . $heading . '</a>';
                    $html .= $icon ? '<div class="article__icon">' . $this->render_lazyload_image( $icon ) . '</div>' : '';
                  $html .= '</h2>';

                  if ( $feature_image ) {
                    $html .= '<div class="article__feature-image">' . $this->render_lazyload_image( $feature_image, [ 'background' => true ] ) . '</div>';
                  }

                  $html .= $this->render_post_excerpt([ 'post_id' => $id, 'trim_length' => $trim_length ]);

                  $html .= '<div class="article__meta meta">';
                    $html .= '<div class="article__cta cta">' . $this->render_link([ 'title' => 'More', 'url' => $url ]) . '</div>';
                  $html .= '</div>';

                $html .= '</article">';
              $html .= '</div>';
            }

          }

        $html .= '</div></div>';
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Articles | Featured
  public function render_articles_featured( $params = [] ) {

    $block_name = 'articles';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $query_args = [
        'post_status'           => [ 'publish' ],
        'posts_per_page'        => '20',
        'order'                 => 'DESC',
        'orderby'                => 'date',
        'post__not_in'          => [],
      ];
      $tax_query = [];
      $curated_articles = get_field( 'curated', $post_id ) ? get_field( 'curated', $post_id ) : [];
      $categories = get_field( 'categories', $post_id ) ? get_field( 'categories', $post_id ) : [];
      $categories_excluded = ( isset($categories['exclude']) && !empty($categories['exclude']) ) ? $categories['exclude'] : false;
      $categories_included = ( isset($categories['include']) && !empty($categories['include']) ) ? $categories['include'] : false;

      if ( $categories_included ) {
        array_push( $tax_query, [
          'taxonomy'      => 'category',
          'field'         => 'term_id',
          'terms'         => $categories_included,
          'operator'      => 'IN',
        ]);
      }

      if ( $categories_excluded ) {
         array_push( $tax_query, [
          'taxonomy' => 'category',
          'field'    => 'term_id',
          'terms'    => $categories_excluded,
          'operator' => 'NOT IN',
        ]);
      }

      if ( $tax_query ) {
        if ( count( $tax_query ) > 1 ) {
          $tax_query['relation'] = 'AND';
        }
        $query_args['tax_query'] = $tax_query;
      }

      foreach( $curated_articles as $i => $item ) {
        $id = ( isset($item['post_id']) && !empty($item['post_id']) ) ? $item['post_id'] : false;
        if ( $id ) {
          $query_args['post__not_in'][] = $id;
        }
      }

      $query = new WP_Query( $query_args );

      //////////////////////////////////////////////////////////
      ////  The Loop
      //////////////////////////////////////////////////////////

      $html .= '<div class="'. $block_name . '__featured">';

        if ( $query->have_posts() ) {

          $html .= '<div class="glide js--glide" id="glide--featured-articles">';
            $html .= '<div class="glide__track" data-glide-el="track">';
              $html .= '<ul class="glide__slides">';

                $masthead = get_field( 'masthead', $post_id );
                $masthead_enable = ( isset($masthead['enable']) && $masthead['enable'] ) ? true : false;

                if ( $masthead_enable ) {
                  $html .= '<li class="glide__slide">';
                    $html .= $this->render_articles_masthead([ 'post_id' => $post_id ]);
                  $html .= '</li>';
                }

                while ( $query->have_posts() ) {

                  $query->the_post();

                  // ---------------------------------------- WP Data
                  $id = get_the_ID();
                  $feature_image = $this->get_featured_image_by_post_id( $id );
                  $post_type = get_post_type( $id );
                  $title = get_the_title( $id );
                  $url = get_permalink( $id );

                  if ( $title && $url ) {
                    $html .= '<li class="glide__slide">';
                      $html .= '<article class="article" data-style="featured" data-post-id="' . $id . '" data-post-type="' . $post_type . '">';
                        $html .= '<div class="article__main">';

                          $html .= $feature_image ? '<div class="article__feature-image">' . $this->render_lazyload_image( $feature_image, [ 'background' => true ] ) . '</div>' : '';

                          $html .= '<div class="article__content">';
                            $html .= '<div class="article__meta meta meta--categories-issue">';
                              $html .= $this->render_post_meta([ 'show_cat_icon' => true, 'cat_limit' => 1, 'meta_types' => [ 'categories', 'issue' ], 'post_id' => $id ]);
                            $html .= '</div>';
                            $html .= '<h2 class="article__title title title--article-featured"><a href="' . $url . '" target="_self">' . $title . '</a></h2>';
                            $html .= $this->render_post_excerpt([ 'post_id' => $id, 'trim_length' => 300 ]);
                            $html .= '<div class="article__meta meta meta--date-cta">';
                              $html .= $this->render_post_meta([ 'date_format' => 'm.d.y', 'meta_types' => [ 'author', 'date' ], 'post_id' => $id ]);
                              $html .= '<div class="article__cta cta">' . $this->render_link([ 'title' => 'More', 'url' => $url  ]) . '</div>';
                            $html .= '</div>';
                          $html .= '</div>';

                        $html .= '</div>';
                      $html .= '</article>';
                    $html .= '</li>';
                  }

                }

              $html .= '</ul>';
            $html .= '</div>';

            $html .= '<div class="glide__controls prev" data-glide-el="controls">';
              $html .= '<button class="glide__button button prev" type="button" data-glide-dir="<">' . $this->render_svg_icon([ 'type' => 'glide.control.next' ]) . '</button>';
            $html .= '</div>';

            $html .= '<div class="glide__controls next" data-glide-el="controls">';
              $html .= '<button class="glide__button button next" type="button" data-glide-dir=">">' . $this->render_svg_icon([ 'type' => 'glide.control.next' ]) . '</button>';
            $html .= '</div>';

          $html .= '</div>';

          wp_reset_postdata();

        }

      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Articles | Masthead
  public function render_articles_masthead( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $masthead = get_field( 'masthead', $post_id );
      $enable = ( isset($masthead['enable']) && $masthead['enable'] ) ? true : false;
      $graphic = ( isset($masthead['graphic']) && !empty($masthead['graphic']) ) ? $masthead['graphic'] : false;
      $message = ( isset($masthead['message']) && !empty($masthead['message']) ) ? $masthead['message'] : false;

      if ( $enable ) {
        $html .= '<article class="article" data-style="masthead" data-post-id="' . $post_id . '">';
          $html .= '<div class="article__masthead">';
            $html .= $graphic ? '<div class="article__masthead-image">' . $this->render_lazyload_image( $graphic ) . '</div>' : '';
            $html .= $message ? '<div class="article__masthead-message rte">' . $message . '</div>' : '';
          $html .= '</div>';
        $html .= '</article>';
      }

    }

    return $html;

  }

  // --------------------------- Article Actions
  public function render_article_actions( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- ACF Var
      $likes = get_field( 'likes', $post_id ) ? get_field( 'likes', $post_id ) : false;
      $likes_obj = get_field_object( 'likes', $post_id );
      $likes_key = ( isset($likes_obj['key']) && !empty($likes_obj['key']) ) ? $likes_obj['key'] : false;

      // ---------------------------------------- Template
      $html .= '<div class="' . $block_name . '__actions">';

        $html .= '<button
            class="' . $block_name . '__share button button--pill button--secondary js--share"
            type="button"
          >Share</button>';

        $html .= '<button
            class="' . $block_name . '__likes button button--likes js--likes"
            data-form-action="' . $this->get_theme_directory( 'home' ) . '/wp-admin/admin-ajax.php"
            data-likes-count="' . $likes . '"
            data-post-id="' . $post_id . '"
            data-acf-field-key="' . $likes_key . '"
            data-form-id="' . $likes_key . '"
            type="button"
          >';
            $html .= '<span class="button__icon">' . $this->render_svg_icon([ 'type' => 'heart' ]) . '</span>';
            $html .= '<span class="button__count">' . $likes . '</span>';
        $html .= '</button>';

      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Article Categories
  public function render_article_categories( $params = [] ) {

    $block_name = 'categories';
    $defaults = [ 'show_cat_icon' => false, 'post_id' => false, 'limit' => 1 ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $categories = get_the_category( $post_id ) ? get_the_category( $post_id ) : [];

      foreach( $categories as $i => $cat ) {



        if ( $i < $limit ) {

          // ---------------------------------------- WP Data
          $cat_name = $cat->name;
          $cat_id = $cat->term_id;
          $cat_slug = $cat->slug;
          $cat_url = get_category_link( $cat_id );
          $cat_term = get_term( $cat_id ) ? get_term( $cat_id ) : false;

          // ---------------------------------------- ACF Data
          $cat_icon = get_field( 'icon_svg', $cat_term );

          if ( $i > 0 ) {
            $html .= '<div class="' . $block_name . '__item delimiter">|</div>';
          }

          $html .= '<div class="' . $block_name . '__item">';
            $html .= ( $show_cat_icon && $cat_icon ) ? '<div class="' . $block_name . '__icon">' . $cat_icon . '</div>' : '';
            $html .= '<div class="' . $block_name . '__name">' . $cat_name . '</div>';
          $html .= '</div>';

        }
      }

    }

    return $html;

  }

  // --------------------------- Post Excerpt
  public function render_post_excerpt( $params = [] ) {

    // default data
    $block_name = 'article';
    $defaults = [ 'post_id' => false, 'trim_length' => 0 ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      if ( 'post' !== get_post_type( $post_id ) ) {
        $block_name = get_post_type( $post_id );
      }

      $excerpt = get_the_excerpt( $post_id );

      $html .= $excerpt ? '<div class="' . $block_name . '__excerpt excerpt"><p>' . trim_string( $excerpt, $trim_length )  . '</p></div>' : '';

    }

    return $html;

  }

  // --------------------------- Article Hero
  public function render_article_hero( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'post_id' => false, 'mobile' => true ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- Post Vars
      $featured_image = $this->get_featured_image_by_post_id( $post_id );
      $title = get_the_title( $post_id );

      // ---------------------------------------- Template
      $html .= '<div class="article__hero">';

        if ( $mobile && $featured_image ) {
          $html .= '<div class="article__featured-image">';
            $html .= $this->render_lazyload_image( $featured_image, [ 'alt_text' => $title ] );
          $html .= '</div>';
        }

        $html .= '<div class="article__hero-content">';

          $html .= '<div class="article__meta meta meta--categories-issue">';
            $html .= $this->render_post_meta([ 'show_cat_icon' => true, 'cat_limit' => 1, 'meta_types' => [ 'categories', 'issue' ], 'post_id' => $post_id ]);
          $html .= '</div>';

          $html .= ( $title ) ? '<h1 class="article__title title title--article-title">' . $title . '</h1>' : '';
          $html .= $this->render_post_excerpt([ 'post_id' => $post_id, 'trim_length' => 999999 ]);

          $html .= '<div class="article__meta meta meta--author-date">';
            $html .= $this->render_post_meta([ 'date_format' => 'm.d.y', 'meta_types' => [ 'author', 'date' ], 'post_id' => $post_id ]);
          $html .= '</div>';

        $html .= '</div>';

      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Article | Related Articles
  public function render_article_preview( $params = [] ) {

    $block_name = 'article';
    $date_format = [
      'preview' => 'F j, Y',
      'related' => 'm.d.y',
    ];
    $defaults = [ 'post_id' => false, 'style' => 'preview' ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- Post Vars
      $featured_image = $this->get_featured_image_by_post_id( $post_id ) ? $this->get_featured_image_by_post_id( $post_id ) : false;
      $permalink = get_permalink( $post_id ) ? get_permalink( $post_id ) : false;
      $title = get_the_title( $post_id ) ? get_the_title( $post_id ) : false;
      $limit = ( 'related' === $style ) ? 3 : 1;

      // ---------------------------------------- Template
      $html .= '<article
        class="' . $block_name . '"
        data-post-id="' . $post_id . '"
        data-style="' . $style . '"
      >';

        if ( 'preview' === $style ) {
          $html .= ( $featured_image ) ? '<div class="' . $block_name . '__featured-image">' . $this->render_lazyload_image( $featured_image ) . '</div>' : '';
        }

        $html .= '<div class="' . $block_name . '__content">';
          $html .= $title ? '<h2 class="' . $block_name . '__title title title--article-' . $style . '">' . $title . '</h2>' : '';
          $html .= '<div class="article__meta meta meta--date-categories">';
            $html .= $this->render_post_meta([ 'cat_limit' => $limit, 'date_format' => 'm.d.y', 'meta_types' => [ 'date', 'categories' ], 'post_id' => $post_id ]);
          $html .= '</div>';
        $html .= '</div>';

        if ( 'related' === $style ) {
          $html .= '<div class="' . $block_name . '__cta">';
            $html .= $this->render_link([
              'classes' => 'button button--pill button--secondary',
              'title' => 'Read',
              'url' => $permalink
            ]);
          $html .= '</div>';
        }

      $html .= '</article>';

    }

    return $html;

  }

  // --------------------------- Article | Related Articles
  public function render_article_releated_articles( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id && wp_get_post_categories( $post_id ) ) {

      $query_args = [
        'posts_per_page'        => 2,
        'post__not_in'          => [ $post_id ],
        'order'                 => 'DESC',
        'orderby'               => 'date',
        'category__in'          => wp_get_post_categories( $post_id ),
      ];
      $releated_articles = new WP_Query( $query_args );

      if ( $releated_articles->have_posts() ) {
        $html .= '<div class="' . $block_name . '__related-articles">';
          $html .= '<h3 class="' . $block_name . '__heading meta meta--mono meta--related-articles">Related Articles</h3>';
          while ( $releated_articles->have_posts() ) {
            $releated_articles->the_post();
            $releated_article_id = get_the_ID();
            $html .= $this->render_article_preview( [ 'post_id' => $releated_article_id, 'style' => 'related' ]  );
          }
          wp_reset_postdata();
        $html .= '</div>';
      }

    }

    return $html;

  }

  // --------------------------- Button
  public function render_button( $button = [] ) {

    $html = '';
    $block_name = 'button';

    if ( $button ) {

      $link = $link_attachment = $link_category = $link_external = $link_page = $link_type = $title = false;
      $appearance = 'primary';
      $target = '_self';

      extract( $button );

      switch ( $link_type ) {
        case 'attachment':
          $link = ( isset($link_attachment['url']) ) ? $link_attachment['url'] : false;
          $target = '_blank';
          break;
        case 'category':
          $link = get_category_link( $link_category );
          break;
        case 'external':
          $link = $link_external;
          $target = '_blank';
          break;
        case 'page':
          $link = get_permalink( $link_page );
          break;
      }

      if ( $link && $title ) {
        $html .= '<a
          class="' . $block_name . '"
          href="' . $link . '"
          target="' . $target . '"
          data-appearance="' . $appearance . '"
        >' . $title . '</a>';
      }

    }

    return $html;

  }

  // --------------------------- Call to Action
  public function render_cta( $params = [] ) {

    // ---------------------------------------- Vars
    $html = '';
    $defaults = [
      'classes' => '',
      'email_subject' => '',
      'title' => false,
      'type' => '',
      'link_internal' => false,
      'link_email' => '',
      'link_external' => '',
      'new_tab' => false,
      'url' => false,
    ];

    extract( array_merge( $defaults, $params ) );

    switch( $type ) {
      case 'email':
        $url = ( $link_email ) ? 'mailto:' . $link_email : false;
        if ( $url && $email_subject ) {
          $url .= '?subject=' . $email_subject;
        }
        break;
      case 'external':
        $url = ( $link_external ) ? $link_external : false;
        $new_tab = true;
        break;
      case 'internal':
        $url = ( $link_internal ) ? $link_internal : false;
        break;
    }

    if ( $url && $title ) {
      $html .= $this->render_link( [ 'classes' => $classes, 'new_tab' => $new_tab, 'title' => $title, 'url' => $url ] );
    }

    return $html;

  }

  // --------------------------- Call to Action | LEGACY
  public function render_cta__LEGACY( $params = [] ) {

    $html = '';
    $block_name = 'cta';
    $justification = 'left';
    $target = '_self';
    $appearance = $link = $link_category = $link_external = $link_page_post = $rel = $title = $type = false;
    $link_file = [];

    extract( $params );

    switch( $type ) {
      case 'category':
        $link = get_category_link( $link_category );
        break;
      case 'external':
        $link = $link_external;
        $rel = 'noopener';
        $target = '_blank';
        break;
      case 'file':
        $link = ( isset($link_file['url']) && !empty($link_file['url']) ) ? $link_file['url'] : false ;
        $rel = 'noopener';
        $target = '_blank';
        break;
      case 'page-post':
        $link = get_permalink( $link_page_post );
    }

    if ( $link && $title ) {
      $html .= '<div class="' . $block_name . '" data-justify="' . $justification . '">';
        $html .= $this->render_link( $link, $title, $appearance, $target, $rel );
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Container
  public function render_container( $state = 'open', $col = 'col-12', $container = 'container' ) {

    $html = '<div class="' . $container . '"><div class="row"><div class="' . $col . '">';
    if ( 'open' !== $state ) {
      $html = '</div></div></div>';
    }
    return $html;

  }

  // --------------------------- Flexible Content
  public function render_flexible_content( $params = [] ) {

    $block_name = 'flexbile-content';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $content = get_field( 'content', $post_id ) ? get_field( 'content', $post_id ) : [];

      if ( $content ) {
        $html .= '<div class="' . $block_name . '">';
          foreach ( $content as $index => $block ) {

            $layout = ( isset($block['acf_fc_layout']) && !empty($block['acf_fc_layout']) ) ? $block['acf_fc_layout'] : 'no-layout';
            $layout_underscore = str_replace( '-', '_', $layout );
            $section = ( isset($block[$layout_underscore]) && !empty($block[$layout_underscore]) ) ? $block[$layout_underscore] : [];

            $html .= '<section class="section section--' . $layout . ' ' . $layout . '">';

              switch ( $layout ) {
                case "blockquote":
                  $section['theme'] = 'blue';
                  $html .= $this->render_section_blockquote( $section );
                  break;
                case "image-feature":
                  $html .= $this->render_section_image_feature( $section );
                  break;
                case "image-text":
                  $html .= $this->render_section_image_text( $section );
                  break;
                case "text-links":
                  $html .= $this->render_section_text_links( $section );
                  break;
              }

            $html .= '</section>';

          }
        $html .= '</div>';
      }

    }

    return $html;

  }

  // --------------------------- Footer
  public function render_footer( $params = [] ) {

    // ---------------------------------------- Vars
    $block_name = 'footer';
    $default_args = [ 'current_id' => false ];
    $logo_src = $this->get_theme_directory( 'assets' ) . '/img/TORPEDO--brand--logo.svg';
    $site_name = get_bloginfo('name');
    $html = '';

    extract( array_merge( $default_args, $params ) );

    // ---------------------------------------- Templates
    $html .= '<footer id="' . $block_name . '" class="' . $block_name . '">';
      $html .= $this->render_container( 'open', 'col-12', 'container-fluid' );
        $html .= '<div class="row row--inner">';

          // ---------------------------------------- Top
          $html .= '<div class="col-12 col-lg-6">';
            $html .= '<div class="' . $block_name . '__navigation-group">';

              $html .= '<nav class="' . $block_name . '__navigation navigation navigation--primary">';
                $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Footer Main' ] );
              $html .= '</nav>';

              if ( $this->is_torpedo ) {
                $html .= '<nav class="' . $block_name . '__navigation navigation navigation--secondary d-none d-lg-inline-flex">';
                  $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Extras' ] );
                $html .= '</nav>';
              }

              $html .= '<nav class="' . $block_name . '__navigation navigation navigation--social">';
                $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Social' ] );
              $html .= '</nav>';

            $html .= '</div>';
          $html .= '</div>';

          $html .= '<div class="col-12 col-lg-6">';
            $html .= '<div class="' . $block_name . '__newsletter">';
              $html .= '<h3 class="' . $block_name . '__newsletter-heading meta meta--mono meta--newsletter-heading">Subscribe to Newsletter</h3>';

              if ( $this->is_torpedo ) {
                $url = $this->torpedo['newsletter_form_url'];
              } else {
                $url = $this->punit['newsletter_form_url'];
              }

              $form_args = [
                'action' => $url,
                'button_title' => 'Sign Up',
                'id' => 'newsletter',
              ];

              $html .= $this->render_form( $form_args );
            $html .= '</div>';
          $html .= '</div>';

          // ---------------------------------------- Bottom
          $html .= '<div class="col-12">';
            $html .= '<div class="' . $block_name . '__legal">';
              $html .= '<div class="' . $block_name . '__brand">';
                if ( $this->is_torpedo ) {
                  $html .= $this->render_svg_icon([ 'type' => 'torpedo.chevrons' ]);
                } else {
                  $html .= $this->render_svg_icon([ 'type' => 'punit.small' ]);
                }
              $html .= '</div>';
              $html .= '<div class="' . $block_name . '__copyright meta">';
                $html .= '<p>Copyright &copy; ' . date('Y') . ' ' . $site_name . '<br>All Rights Reserved.</p>';
              $html .= '</div>';
            $html .= '</div>';
          $html .= '</div>';

          $html .= '<div class="col-12 d-lg-none">';
            $html .= '<nav class="' . $block_name . '__navigation navigation navigation--secondary">';
              $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Extras' ] );
            $html .= '</nav>';
          $html .= '</div>';

        $html .= '</div>';
      $html .= $this->render_container( 'closed' );
    $html .= '</footer>';

    return $html;

  }

  // --------------------------- Form Newsletter
  public function render_form( $params = [] ) {

    $block_name = 'form';
    $defaults = [
      'action' => 'https://formspree.io/f/moqpjnbn',
      'button_title' => 'Sign Up',
      'id' => 'newsletter',
      'post_id' => false,
      'success_action' => 'none',
      'validate' => true
    ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    $html .= '<form
      action="' . $action . '"
      class="' . $block_name . ' js--validate-me"
      data-style="' . $id . '"
      data-success-action="' . $success_action . '"
      method="POST"
    >';

      $html .= '<div class="' . $block_name . '__main">';

        $html .= '<div class="' . $block_name . '__row">';
          $html .= '<div class="' . $block_name . '__field name">';
            $html .= '<input class="required" type="text" name="name" placeholder="Name" value="" />';
            $html .= '<span class="' . $block_name . '__error">Invalid Field(s)</span>';
          $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="' . $block_name . '__row">';
          $html .= '<div class="' . $block_name . '__field email">';
            $html .= '<input class="required" type="email" name="_replyto" placeholder="Email" value="" />';
            $html .= ' <span class="' . $block_name . '__error">Invalid Field(s)</span>';
          $html .= '</div>';
        $html .= '</div>';

        if ( 'notify-me' === $id && $post_id ) {
          $html .= '<div class="' . $block_name . '__row interest">';
            $html .= '<div class="' . $block_name . '__field email">';
              $html .= '<input type="text" name="interest" value="<?php echo get_the_title(); ?>" readonly/>';
            $html .= '</div>';
          $html .= '</div>';
        }

        $html .= '<div class="' . $block_name . '__row rude">';
          $html .= '<div class="' . $block_name . '__field">';
            $html .= '<label>Rude</label>';
            $html .= '<input class="rude" type="text" name="rude">';
          $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="' . $block_name . '__row action">';
          $html .= '<button class="form__button button button--pill button--secondary" type="submit">' . $button_title . '</button>';
        $html .= '</div>';

      $html .= '</div>';

    $html .= '</form>';

    return $html;

  }

  // --------------------------- Google Analytics
  public function render_google_analytics( $google_analytics_measurement_id = false ) {

    //G-KSQNHT61VM

    $html = '<!-- Global site tag (gtag.js) - Google Analytics -->';
    if ( $google_analytics_measurement_id ) {
      $html .= '<script async src="https://www.googletagmanager.com/gtag/js?id=' . $google_analytics_measurement_id . '"></script>';
      $html .= '<script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag("js", new Date());

        gtag("config", "' . $google_analytics_measurement_id . '" );
      </script>';
    } else {
      $html = '<!-- No Google Analytics Measurement ID Provided -->';
    }

    return $html;

  }

  // --------------------------- Header
  public function render_header( $params = [] ) {

    // ---------------------------------------- Vars
    $block_name = 'header';
    $default_args = [ 'current_id' => false ];
    $site_name = get_bloginfo('name');
    $html = '';

    extract( array_merge( $default_args, $params ) );

    // ---------------------------------------- Templates
    $html .= '<header id="' . $block_name . '" class="' . $block_name . ' fixed">';
      $html .= $this->render_container( 'open', 'col-12', 'container-fluid' );
        $html .= '<div class="' . $block_name . '__main">';

          $html .= '<div class="' . $block_name . '__brand d-lg-none">';
            $html .= '<a href="' . $this->get_theme_directory( 'home' ) .'" target="_self">';
              $html .= $this->render_svg_icon([ 'type' => 'punit.small' ]);
            $html .= '</a>';
          $html .= '</div>';

          $html .= '<div class="' . $block_name . '__brand d-none d-lg-inline-flex">';
            $html .= '<a href="' . $this->get_theme_directory( 'home' ) .'" target="_self">';
              $html .= $this->render_svg_icon([ 'type' => 'punit' ]);
            $html .= '</a>';
          $html .= '</div>';

          $html .= '<nav class="' . $block_name . '__navigation navigation navigation--main d-none d-lg-inline-flex">';
            $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Main Menu' ] );
          $html .= '</nav>';

          $html .= '<button class="' . $block_name . '__burger button d-lg-none js--mobile-menu-trigger" type="button">';
            $html .= $this->render_svg_icon([ 'type' => 'burger' ]);
          $html .= '</button>';

        $html .= '</div>';
      $html .= $this->render_container( 'closed' );
    $html .= '</header>';

    // ---------------------------------------- Returned
    return $html;

  }

  // --------------------------- Header | Torpedo
  public function render_header_torpedo( $params = [] ) {

    // ---------------------------------------- Vars
    $block_name = 'header';
    $default_args = [ 'current_id' => false ];
    $site_name = get_bloginfo('name');
    $html = '';

    extract( array_merge( $default_args, $params ) );

    // ---------------------------------------- Templates
    $html .= '<header id="' . $block_name . '" class="' . $block_name . ' fixed">';
      $html .= $this->render_container( 'open', 'col-12', 'container-fluid' );
        $html .= '<div class="' . $block_name . '__main">';

          $html .= '<div class="' . $block_name . '__brand">';
            $html .= '<a href="' . $this->get_theme_directory( 'home' ) .'" target="_self">';
              $html .= $this->render_svg_icon([ 'type' => 'torpedo' ]);
            $html .= '</a>';
          $html .= '</div>';

          $html .= '<nav class="' . $block_name . '__navigation navigation navigation--main d-none d-lg-inline-flex">';
            $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Main Menu' ] );
            if ( is_plugin_active( 'wp-shopify-pro/wp-shopify.php' ) ) {
              $html .= '<div class="navigation__item">';
                $html .= '<button class="navigation__link button js--cart-summary" type="button">Cart<span class="js--cart-item-count"></span></button>';
              $html .= '</div>';
            }
          $html .= '</nav>';

          $html .= '<button class="' . $block_name . '__burger button d-lg-none js--mobile-menu-trigger" type="button">';
            $html .= $this->render_svg_icon([ 'type' => 'burger' ]);
          $html .= '</button>';

        $html .= '</div>';
      $html .= $this->render_container( 'closed' );
    $html .= '</header>';

    return $html;

  }

  // --------------------------- Issue | Article Preview
  public function render_issue_article_preview( $params = [] ) {

    $block_name = 'issue';
    $defaults = [
      'issue_cat_id' => false,
      'issue_cat_name' => false,
      'post_id' => false
    ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- WP Vars
    $title = $post_id ? get_the_title( $post_id ) : false;
    $url = $post_id ? get_the_permalink( $post_id ) : false;
    $feature_image = $this->get_featured_image_by_post_id( $post_id );
    $fallback_image = '<div
      class="lazyload-item lazyload-item--image lazyload-item--background lazypreload lazyload"
      data-transition-duration="450"
      data-transition-delay="0"
      data-bg="' . $this->get_theme_directory( 'assets' ) . '/img/TORPEDO--issue--cover-image-fallback.png">
      </div>';

    $html .= '<article class="article" data-style="preview" data-post-id="' . $post_id . '">';
      $html .= ( $issue_cat_name ) ? '<div class="article__category meta meta--mono meta--category">' . $issue_cat_name . '</div>' : '';
      if ( $feature_image ) {
        $html .= '<div class="article__feature-image"><a href="' . $url . '" target="_self">' . $this->render_lazyload_image( $feature_image, [ 'background' => true ] ) . '</a></div>';
      } else {
        $html .= '<div class="article__feature-image-fallback">' . $fallback_image . '</div>';
      }
      if ( $post_id ) {
        $html .= '<div class="article__info">';
          $html .= ( $title ) ? '<h3 class="article__title"><a href="' . $url . '" target="_self">' . $title . '</a></h3>' : '';
          $html .= ( $url ) ? '<div class="article__cta">' . $this->render_link( [ 'classes' => 'meta meta--mono meta--link', 'title' => 'More', 'url' => $url ] ) . '</div>' : '';
        $html .= '</div>';
      }
    $html .= '</article>';

    return $html;

  }

  // --------------------------- Issue | Donations
  public function render_issue_donations( $params = [] ) {

    $block_name = 'issue';
    $default_contributions = get_field( 'contributions', 'options' ) ? get_field( 'contributions', 'options' ) : [];
    $defaults = [ 'post_id' => false ];
    $html = '';

    // contribution_enable
    // contribution_message

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- ACF Vars
      $contribution_enable = get_field( 'contribution_enable', $post_id ) ? get_field( 'contribution_enable', $post_id ) : false;
      $contribution_message = get_field( 'contribution_message', $post_id ) ? get_field( 'contribution_message', $post_id ) : false;

      if ( $contribution_enable  ) {

        // ---------------------------------------- ACF Options Vars
        $donations_cta = [];
        $donations_message = false;

        extract( $default_contributions );

        if ( $donations_cta ) {
          $donations_cta['classes'] = 'button button--pill button--tertiary';
        }

        if ( $contribution_message ) {
          $donations_message = $contribution_message;
        }

        $html .= '<section class="' . $block_name . '__donations">';
          $html .= $this->render_container( 'open', 'col-12', 'container-fluid' );
            $html .= '<div class="' . $block_name . '__donations-main">';

              $html .= ( $donations_message ) ? '<div class="' . $block_name . '__donations-message">' . $donations_message . '</div>' : '';
              $html .= ( $donations_cta ) ? '<div class="' . $block_name . '__donations-cta">' . $this->render_cta( $donations_cta ) . '</div>' : '';

            $html .= '</div>';
          $html .= $this->render_container( 'closed' );
        $html .= '</section>';

      }

    }

    return $html;

  }

  // --------------------------- Issue | Header
  public function render_issue_hero( $params = [] ) {

    $block_name = 'issue';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- ACF Vars
      $issue = get_field( 'issue', $post_id ) ? get_field( 'issue', $post_id ) : false;
      $topic = get_field( 'topic', $post_id ) ? get_field( 'topic', $post_id ) : false;

      // ---------------------------------------- WP Vars
      $content = get_the_content( $post_id );
      $date = get_the_date( 'F Y', $post_id );
      $feature_image = $this->get_featured_image_by_post_id( $post_id );
      $season = $this->render_season( get_the_date( 'n', $post_id ) );

      // ---------------------------------------- Template
      $html .= '<section class="' . $block_name . '__hero">';
        $html .= '<div class="' . $block_name . '__hero-image">';
          $html .= ( $feature_image ) ? '<div class="' . $block_name . '__feature-image d-lg-none">' . $this->render_lazyload_image( $feature_image, [ 'alt_text' => $topic ] ) . '</div>' : '';
          $html .= ( $feature_image ) ? '<div class="' . $block_name . '__feature-image d-none d-lg-block">' . $this->render_lazyload_image( $feature_image, [ 'background' => true ] ) . '</div>' : '';
        $html .= '</div>';
        $html .= '<div class="' . $block_name . '__hero-content">';
          $html .= ( $issue ) ? '<h2 class="' . $block_name . '__number">Issue ' . ( $issue > 10 ? $issue : '0' . $issue ) . '</h2>' : '';
          $html .= ( $topic ) ? '<h1 class="' . $block_name . '__topic">' . $topic . '</h1>' : '';
          $html .= ( $date ) ? '<time class="' . $block_name . '__date meta meta--mono meta--date" datetime="' . $date . '">' . $season . ' | ' . $date . '</time>' : '';
          $html .= ( $content ) ? '<div class="' . $block_name . '__content">' . $content . '</div>' : '';
        $html .= '</div>';
      $html .= '</section>';

    }

    return $html;

  }

  // --------------------------- Issue | Preview
  public function render_issue_preview( $params = [] ) {

    // RENDER META

    $block_name = 'issue';
    $html = '';
    $defaults = [
      'post_id' => false,
    ];

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- ACF Vars
      $cover_image = get_field( 'cover_image', $post_id ) ? get_field( 'cover_image', $post_id ) : false;
      $topic = get_field( 'topic', $post_id ) ? get_field( 'topic', $post_id ) : false;
      $issue = get_field( 'issue', $post_id ) ? get_field( 'issue', $post_id ) : false;

      // ---------------------------------------- Default Vars
      $cover_image_fallback_scr = $this->get_theme_directory( 'assets' ) . '/img/TORPEDO--issue--cover-image-fallback.png';

      // ---------------------------------------- WP Vars
      $date = get_the_date( 'm.d.y', $post_id );
      $title = get_the_title( $post_id );
      $url = get_the_permalink( $post_id );
      $cta_link = [
        'classes' => 'button button--pill',
        'title' => 'Read',
        'url' => $url
      ];

      $html .= '<article class="' . $block_name . '" data-style="preview" data-post-id="' . $post_id . '">';
        $html .= '<div class="' . $block_name . '__hero">';
          $html .= ( $issue ) ? '<h3 class="' . $block_name . '__number">Issue ' . ( $issue > 10 ? $issue : '0' . $issue ) . '</h3>' : '';
          $html .= ( $topic ) ? '<h2 class="' . $block_name . '__topic">' . $topic . '</h2>' : '';
          $html .= ( $date ) ? '<time class="' . $block_name . '__date meta meta--mono meta--date" datetime="' . $date . '">' . $date . '</time>' : '';
        $html .= '</div>';
        $html .= '<div class="' . $block_name . '__main">';
          $html .= '<div class="' . $block_name . '__cover-image">';
            if ( $cover_image ) {
              $html .= '<a href="' . $url . '" target="_self">';
                $html .= $this->render_lazyload_image( $cover_image, [ 'alt_text' => $topic ] );
              $html .= '</a>';
            } else {
              $html .= '<img
                class="lazyload lazyload-item lazyload-item--image"
                data-src="' . $cover_image_fallback_scr . '"
                data-transition-duration="450"
                data-transition-delay="0"
                width="800"
                height="1168"
                alt="Fallback Cover Image"
               />';
            }
          $html .= '</div>';
          $html .= $this->render_post_excerpt([ 'post_id' => $post_id, 'trim_length' => 200 ]);
          $html .= '<div class="' . $block_name . '__cta">' . $this->render_link( $cta_link ) . '</div>';
        $html .= '</div>';
      $html .= '</article>';

    }

    return $html;

  }

  // --------------------------- Latest Articles Button Next
  public function render_latest_articles_button_next( $params = [] ) {

    // default data
    $html = '';

    // get data
    if ( isset( $params['colour_theme'] ) && !empty( $params['colour_theme'] ) ) {

      $colour_theme = $params['colour_theme'];
      $icon_src = $this->theme_directory('assets') . '/img/icon/';

      switch ( $colour_theme ) {
        case 'black':
          $icon_src .= 'PUNIT--web-assets--icon--next--white.svg';
          break;
        case 'white':
          $icon_src .= 'PUNIT--web-assets--icon--next--black.svg';
          break;
      }

      $html .= '<button class="latest-articles__button glide__button glide__button--next" type="button" data-action=">">';
        $html .= '<img src="' . $icon_src . '" alt="Next" />';
      $html .= '</button>';

    }

    return $html;

  }

  // --------------------------- Lazyload iFrame
  public function render_lazyload_iframe( $id = false, $source = 'vimeo' ) {

    $result = false;
    $url = false;
    $classes = 'lazyload-item lazyload-item--iframe lazyload';

    switch ( $source ) {
      case 'vimeo':
        $url = 'https://player.vimeo.com/video/';
        break;
      case 'youtube':
        $url = 'https://www.youtube.com/embed/';
        break;
    }

    $result = '<iframe
      class="' . $classes . '"
      frameborder="0"
      allow="autoplay; encrypted-media"
      data-src="' . $url . $id . '"
      webkitallowfullscreen mozallowfullscreen allowfullscreen
    ></iframe>';

    return $result;

  }

  // --------------------------- Lazyload Image
  public function render_lazyload_image( $media = false, $params = [] ) {

    // default data
    $html = '';
    $classes = 'lazyload lazyload-item lazyload-item--image';
    $is_gif = $is_svg = false;
    $sizes_title = $this->custom_image_title;
    $sizes = $this->custom_image_sizes;

    // default params
    $alt_text = $background = false;
    $preload = $responsive = true;
    $delay = 0;
    $duration = 450;

    // conditionally update media settings
    if ( $media ) {

      // deconstruct $params object
      extract( $params );

      // check media object to see if mime type is SVG
      if ( isset( $media['subtype'] ) ) {
        $is_svg = ( 'svg+xml' === $media['subtype'] ) ? true : false;
        $is_gif = ( 'gif' === $media['subtype'] ) ? true : false;
      }

      // conditionally check if alt text is not provided argument and is provided with media
      if ( !$alt_text && isset( $media['alt'] ) && !empty( $media['alt'] ) ) {
        $alt_text = $media['alt'];
      }

      // conditionally build classes
      $classes .= $background ? ' lazyload-item--background' : ' lazyload-item--inline';
      $classes .= $preload ? ' lazypreload' : '';

      // conditionally build html
      if ( $background ) {

        $html = '<div
          class="' . $classes . '"
          data-bg="' . $media['url'] . '"
          data-transition-duration="' . $duration . '"
          data-transition-delay="' . $delay . '"
          style="background-url(' . $media['sizes'][$sizes_title . '-1'] . ');"
          ></div>';

       } else {

        $html = '<img
          class="' . $classes . '"
          width="' . $media['width'] . '"
          height="' . $media['height'] . '"
          data-src="' . $media['url'] . '"
          data-transition-duration="' . $duration . '"
          data-transition-delay="' . $delay . '"';

        if ( !$is_svg ) {

          $html .= ' src="' . $media['sizes'][$sizes_title . '-10'] . '"';

          if ( $responsive && !$is_gif ) {

            $srcset = '';

            foreach ( $sizes as $index => $size ) {
              if ( 0 == $index ) {
                $srcset .= $media['sizes'][$sizes_title . '-' . $size] . ' ' . $size . 'w';
              } else {
                $srcset .= ', ' . $media['sizes'][$sizes_title . '-' . $size] . ' ' . $size . 'w';
              }
            }

            $html .= ' data-sizes="auto"';
            $html .= ' data-srcset="' . $srcset . '"';

          }

        }

        if ( $alt_text ) {
          $html .= ' alt="' . $alt_text . '"';
        }

        $html .= ' />';

      }

    }

    return $html;

  }

  // --------------------------- Lazyload Video
  public function render_lazyload_video( $media = false, $post = 'inline' ) {

    $result = false;

    $classes = 'lazyload-item lazyload-item--video';
    $classes .= ' lazyload-item--' . $pos;
    $classes .= ' lazyload';

    $result = '<video class="' . $classes . '" preload="none" muted="" data-autoplay="" data-poster="" src="' . $media . '" loop playsinline muted>';
      $result .= '<source src="' . $media . '" type="video/mp4">';
    $result .= '</video>';

    return $result;

  }

  // --------------------------- Link
  public function render_link( $params = [] ) {

    // ---------------------------------------- Vars
    $html = '';
    $defaults = [
      'active' => false,
      'classes' => '',
      'new_tab' => false,
      'title' => '',
      'url' => ''
    ];

    extract( array_merge( $defaults, $params ) );

    if ( $title && $url ) {

      $classes .= $active ? ' active' : '';

      $html .= '<a';
      $html .= $classes ? ' class="' . $classes . '"' : '';
      $html .= ' href="' . $url . '"';
      if ( $new_tab ) {
        $html .= ' target="_blank"';
        $html .= ' rel="noopener"';
      } else {
        $html .= ' target="_self"';
      }
      $html .= '>';
      $html .= $title;
      $html .= '</a>';

    }

    return $html;

  }

  // --------------------------- Mobile Menu
  public function render_mobile_menu( $params = [] ) {

    // ---------------------------------------- Vars
    $block_name = 'mobile-menu';
    $default_args = [ 'current_id' => false ];
    $site_name = get_bloginfo('name');
    $logo_src = $this->get_theme_directory( 'assets' ) . '/img/TORPEDO--brand--chevron--yellow.svg';
    $html = '';

    extract( array_merge( $default_args, $params ) );

    // ---------------------------------------- Templates
    $html .= '<div class="' . $block_name . '">';
      $html .= '<div class="' . $block_name . '__main">';

        $html .= '<button class="' . $block_name . '__button button js--mobile-menu-trigger" type="button">';
          $html .= $this->render_svg_icon_close();
        $html .= '</button>';

        $html .= '<nav class="' . $block_name . '__navigation navigation navigation--main">';
          $html .= $this->render_navigation_wp( [ 'current_id' => $current_id, 'menu_title' => 'Main Menu' ] );
          if ( is_plugin_active( 'wp-shopify-pro/wp-shopify.php' ) ) {
            $html .= '<div class="navigation__item">';
              $html .= '<button class="navigation__link button js--cart-summary" type="button">Cart<span class="js--cart-item-count"></span></button>';
            $html .= '</div>';
          }
        $html .= '</nav>';

        $html .= '<div class="' . $block_name . '__footer">';
          $html .= '<div class="' . $block_name . '__brand">';
            if ( $this->is_torpedo ) {
              $html .= $this->render_svg_icon([ 'type' => 'torpedo.chevrons' ]);
            } else {
              $html .= $this->render_svg_icon([ 'type' => 'punit' ]);
            }
          $html .= '</div>';
          $html .= '<div class="' . $block_name . '__legal meta meta--mono meta--copyright">';
            $html .= '<p>Copyright &copy; ' . date('Y') . ' ' . $site_name . '<br>All Rights Reserved.</p>';
          $html .= '</div>';
        $html .= '</div>';

      $html .= '</div>';
    $html .= '</div>';

    // ---------------------------------------- Returned
    return $html;

  }

  // --------------------------- Modal
  public function render_modal( $params = [] ) {

    $block_name = 'modal';
    $defaults = [
      'days_saved' => 365,
      'form' => false,
      'heading' => 'Subscribe to emails from Punit',
      'id' => 'default',
      'post_id' => false,
      'theme' => 'red',
      'timeout' => 1500,
    ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- Template
    $html .= '<div
      class="' . $block_name . ' micromodal-slide"
      data-background-colour="' . $theme . '"
      data-days-saved="' . $days_saved . '"
      data-timeout="' . $timeout . '"
      data-style="' . $id . '"
      id="modal--' . $id . '"
      aria-hidden="true"
    >';

      $html .= '<div class="' . $block_name . '__overlay" tabindex="-1">';
        $html .= '<div class="' . $block_name . '__container" id="modal-container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">';
          $html .= $this->render_container( 'open', 'col-12 col-lg-7 offset-lg-3', 'container-fluid' );

            $html .= '<button class="' . $block_name . '__button button d-lg-none" type="button" data-micromodal-close>';
              $html .= $this->render_svg_icon([ 'type' => 'close' ]);
            $html .= '</button>';

            $html .= '<div class="' . $block_name . '__main">';

              $html .= '<button class="' . $block_name . '__button button d-none d-lg-block" type="button" data-micromodal-close>';
                $html .= $this->render_svg_icon([ 'type' => 'close' ]);
              $html .= '</button>';

              $html .= '<div class="' . $block_name . '__content">';
                $html .= $heading ? '<h2 class="' . $block_name . '__heading heading heading--modal">' . $heading . '</h2>' : '';

                if ( 'share' === $id && $post_id ) {
                  $html .= '<div class="' . $block_name . '__share-links">';
                    $accounts = [ 'twitter', 'linkedin', 'facebook' ];
                    foreach( $accounts as $i => $account ) {
                      $url = $this->render_share_url([ 'type' => $account, 'post_id' => $post_id ]);
                      $icon = $this->render_svg_icon([ 'type' => $account ]);
                      if ( $url && $icon ) {
                        $html .= '<div class="' . $block_name . '__share-item ' . $account . '">';
                          $html .= '<a class="' . $block_name . '__share-link" href="' . $url . '" target="_blank" rel="noopener">';
                            $html .= '<div class="' . $block_name . '__share-icon">' . $icon . '</div>';
                          $html .= '</a>';
                        $html .= '</div>';
                      }
                    }
                  $html .= '</div>';
                }

                if ( 'newsletter' === $id || 'notify-me' === $id ) {
                  if ( 'newsletter' === $id ) {

                    if ( $this->is_torpedo ) {
                      $url = $this->torpedo['newsletter_form_url'];
                    } else {
                      $url = $this->punit['newsletter_form_url'];
                    }

                    $form_args = [
                      'action' => $url,
                      'success_action' => 'close-modal',
                      'button_title' => 'Sign Up',
                      'id' => $id,
                    ];
                    $html .= $form ? $this->render_form( $form_args ) : '';

                  }
                  if ( 'notify-me' === $id ) {
                    $form_args = [
                      'success_action' => 'close-modal',
                      'action' => 'https://formspree.io/f/mleopqnq',
                      'button_title' => 'Notify Me',
                      'id' => $id,
                    ];
                    $html .= $form ? $this->render_form( $form_args ) : '';
                  }
                  $html .= '<div class="' . $block_name . '__brand">';
                    if ( $this->is_torpedo ) {
                      $html .= $this->render_svg_icon([ 'type' => 'torpedo.chevrons' ]);
                    } else {
                      $html .= $this->render_svg_icon([ 'type' => 'punit.small.alt' ]);
                    }
                  $html .= '</div>';
                }

              $html .= '</div>';

            $html .= '</div>';

          $html .= $this->render_container( 'closed' );
        $html .= '</div>';
      $html .= '</div>';
    $html .= '</div>';

    return $html;

  }

  // --------------------------- Navigation for WP
  public function render_navigation_wp( $params = [] ) {

    // ---------------------------------------- Vars
    $block_name = 'navigation';
    $default_args = [ 'current_id' => false, 'menu_title' => '' ];
    $html = '';

    extract( array_merge( $default_args, $params ) );

    if ( wp_get_nav_menu_items( $menu_title ) ) {

      $menu_items = wp_get_nav_menu_items( $menu_title ) ? wp_get_nav_menu_items( $menu_title ) : [];

      foreach ( $menu_items as $item ) {

        $id = $item->object_id;
        $link = $item->url;
        $title = $item->title;
        $object_type = $item->object;
        $link_type = $item->type;
        $active = ( $id == $current_id ) ? true : false;
        $new_tab = ( 'custom' == $link_type ) ? true : false;
        $classes = $block_name . '__link';

        $link_args = [
          'active' => $active,
          'classes' => $classes,
          'new_tab' => $new_tab,
          'title' => $title,
          'url' => $link
        ];

        $html .= '<div class="' . $block_name . '__item">';
          $html .= ( $link && $title ) ? $this->render_link( $link_args ) : '';
        $html .= '</div>';

      }

    }

    return $html;

  }

  // --------------------------- Navigation
  public function render_navigation( $links = [], $current_id = false, $class_modifier = '' ) {

    $html = '';
    $block_name = 'navigation';
    $block_classes = ( $class_modifier ) ? $block_name . ' ' . $block_name . '--' . $class_modifier : $block_name;

    if ( $links ) {
      $html .= '<nav class="' . $block_classes . '">';
        foreach( $links as $i => $item ) {

          // set defaults
          $type = $title = $link = $link_anchor = $link_category = $link_external = $link_page = false;
          $is_internal = true;
          $is_active = false;

          // extract link data
          extract( $item );

          switch ( $type ) {
            case 'anchor':
              $link = ( $link_anchor ) ? '#' . $link_anchor : false;
              break;
            case 'external':
              $link = ( $link_external ) ? $link_external : false;
              $is_internal = false;
              break;
            case 'category':
              break;
            case 'page-post':
              break;
          }

          $html .= $this->render_navigation_item( $title, $link, $is_active, $is_internal );

        }
      $html .= '</nav>';
    }

    return $html;

  }

  // --------------------------- Navigation Item
  public function render_navigation_item( $title = '', $url = '', $active = false, $internal = true ) {

    $html = '';
    $block_name = 'navigation';
    $target = '_self';
    $rel = false;

    if ( !$internal ) {
      $target = '_blank';
      $rel = 'noopener';
    }

    if ( $title && $url ) {
      $html .= '<div class="' . $block_name . '__item' . ( $active ? ' active' : '' ) . '">';
        $html .= '<a
          class="' . $block_name . '__link' . ( $active ? ' active' : '' ) . '"
          href="' . $url . '"
          target="' . $target . '"
          ' . ( $rel ? 'rel="' . $rel . '"' : '' ) . '
        >' . $title . '</a>';
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- Pagination
  public function render_pagination( $params = [] ) {

    $block_name = 'pagination';
    $defaults = [
      'next' => false,
      'prev' => false,
      'pages' => false,
    ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    // ---------------------------------------- Template
    $html .= '<section class="' . $block_name . '__pagination pagination">';
      $html .= $this->render_container( 'open', 'col-12', 'container-fluid' );
        $html .= '<div class="' . $block_name . '__main">';

          $html .= '<div class="' . $block_name . '__item prev ' . ( $prev ? 'active' : 'not-active' ) . '">';
            $html .= $prev ? $prev : 'Prev';
          $html .= '</div>';

          foreach( $pages as $i => $item ) {
            $html .= '<div class="' . $block_name . '__item page">' . $item . '</div>';
          }

          $html .= '<div class="' . $block_name . '__item next ' . ( $next ? 'active' : 'not-active' ) . '">';
            $html .= $next ? $next : 'Next';
          $html .= '</div>';

        $html .= '</div>';
      $html .= $this->render_container( 'closed' );
    $html .= '</section>';

    return $html;

  }

  // --------------------------- Post Meta
  public function render_post_meta( $params = [] ) {

    $block_name = 'article';
    $defaults = [ 'show_cat_icon' => false, 'cat_limit' => 1, 'date_format' => 'm.d.y', 'meta_types' => [], 'post_id' => false ];
    $html = $meta_html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id && $meta_types ) {

      if ( 'post' !== get_post_type( $post_id ) ) {
        $block_name = get_post_type( $post_id );
      }

      foreach ( $meta_types as $i => $meta ) {

        switch( $meta ) {
          case 'author':

            $author_id = get_post_field( 'post_author', $post_id );
            $author = get_the_author_meta( 'display_name', $author_id );
            $meta_html = ( $author ) ? '<div class="' . $block_name . '__author author">' . $author . '</div>' : '';
            break;

          case 'categories':

            $categories = $this->render_article_categories([ 'show_cat_icon' => $show_cat_icon, 'post_id' => $post_id, 'limit' => $cat_limit ]);
            $meta_html = ( $categories ) ? '<div class="' . $block_name . '__categories categories" data-show-cat-icont="' . $show_cat_icon . '">' . $categories . '</div>' : '';
            break;

          case 'date':

            $date = get_the_date( $date_format, $post_id );
            $meta_html = ( $date ) ? '<time class="' . $block_name . '__date date" datetime="' . $date . '">' . $date . '</time>' : '';
            break;

          case 'issue':

            $issue = get_field( 'issue', $post_id );
            $meta_html = ( $issue ) ? '<div class="' . $block_name . '__issue issue">Issue ' . ( $issue > 10 ? $issue : '0' . $issue ) . '</div>' : '';
            break;

        }

        if ( $meta_html ) {
          $html .= ( $i > 0 ) ? '<div class="' . $block_name . '__delimiter delimiter">|</div>' . $meta_html : $meta_html;
        }

      }
    }

    return $html;

  }

  // --------------------------- Preload Fonts
  public function render_preload_fonts( $fonts = [] ) {
    $html = '';
    foreach ( $fonts as $font ) {
      $font_src = $this->get_theme_directory('assets') . '/fonts/' . $font . '.woff2';
      $html .= '<link rel="preload" href="' . $font_src . '" as="font" type="font/woff2" crossorigin>';
    }
    return $html;
  }

  // --------------------------- Preview
  public function render_press_preview( $params = [] ) {

    $block_name = 'press';
    $date_format = 'm.d.y';
    $defaults = [ 'post_id' => false, 'wide' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- Post Data
      $date = get_the_date( 'm.d.y', $post_id );
      $image = $this->get_featured_image_by_post_id( $post_id );
      $title = get_the_title( $post_id );

      // ---------------------------------------- ACF Data
      $image_wide = $wide ? get_field( 'featured_image_wide', $post_id ) : false;
      $url = get_field( 'url', $post_id ) ? get_field( 'url', $post_id ) : false;
      $source = get_field( 'source', $post_id ) ? get_field( 'source', $post_id ) : false;

      // ---------------------------------------- Template
      $html .= '<article class="' . $block_name . '__item' . ( $wide ? ' wide' : '' ) . '" data-post-id="' . $post_id . '">';

        $html .= $source ? '<h3 class="' . $block_name . '__title title title--press">' . $source . '</h3>' : '';

        if ( $image ) {
          $html .= '<div class="' . $block_name . '__image">';
            $html .= '<a href="' . $url . '" target="_blank" rel="noopener">';
              if ( $image_wide ) {
                $html .= '<div class="d-md-none">' . $this->render_lazyload_image( $image, [ 'alt_text' => $title ] ) . '</div>';
                $html .= '<div class="d-none d-md-block">' . $this->render_lazyload_image( $image_wide, [ 'alt_text' => $title ] ) . '</div>';
              } else {
                $html .= $this->render_lazyload_image( $image, [ 'alt_text' => $title ] );
              }
            $html .= '</a>';
          $html .= '</div>';
        }

        $html .= '<div class="' . $block_name . '__meta meta meta--date-cta">';
          $html .= $this->render_post_meta([ 'date_format' => 'm.d.y', 'meta_types' => [ 'date' ], 'post_id' => $post_id ]);
          $html .= $url ? '<div class="' . $block_name . '__cta cta">' . $this->render_link([ 'new_tab' => true, 'title' => 'More', 'url' => $url ]) . '</div>' : '';
        $html .= '</div>';

      $html .= '</article>';

    }

    return $html;

  }

  // --------------------------- Seasons
  public function render_season( $month = 1 ) {

    switch( $month ) {
      case 12:
      case 1:
      case 2:
        return 'Winter';
        break;
      case 3:
      case 4:
      case 5:
        return 'Spring';
        break;
      case 6:
      case 7:
      case 8:
        return 'Summer';
        break;
      case 9:
      case 10:
      case 11:
        return 'Fall';
        break;
    }

  }

  // --------------------------- Section Blockquote
  public function render_section_blockquote( $params = [] ) {

    $block_name = 'blockquote';
    $html = '';

    if ( $params ) {

      // ---------------------------------------- Default Vars
      $icon = $note = $quote = false;
      $theme = 'default';
      extract( $params );

      // ---------------------------------------- Template
      $html .= '<div class="' . $block_name . '__main" data-background-colour="' . $theme . '">';
        $html .= $this->render_container( 'open', 'col-12 col-lg-10 offset-lg-1', 'container-fluid' );

          $html .= $icon ? '<div class="' . $block_name . '__icon"><img src="' . $icon['url'] . '" alt="Decorative Icon" /></div>' : '';
          $html .= $quote ? '<div class="' . $block_name . '__quote"><h2 class="blockquote__heading heading heading--blockquote">' . $quote . '</h2></div>' : '';
          $html .= $note ? '<div class="' . $block_name . '__note"><p>' . $note . '</p></div>' : '';

        $html .= $this->render_container( 'closed' );
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Section Image Feature
  public function render_section_image_feature( $params = [] ) {

    $block_name = 'image-feature';
    $html = '';

    if ( $params ) {

      // ---------------------------------------- Default Vars
      $images = [];
      $theme = 'default';
      extract( $params );

      // ---------------------------------------- Template
      $html .= '<div class="' . $block_name . '__main" data-background-colour="' . $theme . '" data-image-count="' . count( $images ) . '">';

        foreach ( $images as $index => $item ) {

          $image = false;
          extract( $item );

          $html .= $image ? '<div class="' . $block_name . '__image" data-image-index="' . $index . '">' . $this->render_lazyload_image( $image ) . '</div>' : '';

        }

      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Section Image with Text
  public function render_section_image_text( $params = [] ) {

    $block_name = 'image-text';
    $html = '';

    if ( $params ) {

      // ---------------------------------------- Default Vars
      $cta = $heading = $image = $layout = $message = false;
      $theme = 'default';
      extract( $params );

      // ---------------------------------------- Template
      $html .= '<div class="' . $block_name . '__main" data-background-colour="' . $theme . '" data-style="' . $layout . '">';

        $html .= $image ? '<div class="' . $block_name . '__image">' . $this->render_lazyload_image( $image ) . '</div>' : '';

        if ( $heading || $message ) {
          $html .= '<div class="' . $block_name . '__content">';
            $html .= $heading ? '<h2 class="' . $block_name . '__heading heading heading--title">' . $heading . '</h2>' : '';
            $html .= $message ? '<div class="' . $block_name . '__message message rte">' . $message . '</div>' : '';
            if ( $cta ) {
              $cta['classes'] = 'button button--pill button--secondary';
              $html .= '<div class="' . $block_name . '__cta">' . $this->render_cta( $cta ) . '</div>';
            }
          $html .= '</div">';
        }

      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Section Text with Links
  public function render_section_text_links( $params = [] ) {

    $block_name = 'text-links';
    $html = '';

    if ( $params ) {

      // ---------------------------------------- Default Vars
      $heading = $message = false;
      $links = [];
      $theme = 'default';
      extract( $params );

      // ---------------------------------------- Template
      $html .= '<div class="' . $block_name . '__main" data-background-colour="' . $theme . '">';
        $html .= $this->render_container( 'open', 'col-12 col-lg-10 offset-lg-1', 'container-fluid' );

          if ( $heading || $message ) {
            $html .= '<div class="' . $block_name . '__content">';
              $html .= $heading ? '<h2 class="' . $block_name . '__heading heading heading--title">' . $heading . '</h2>' : '';
              $html .= $message ? '<div class="' . $block_name . '__message message rte">' . $message . '</div>' : '';
            $html .= '</div>';
          }

          if ( $links ) {
            $html .= '<div class="' . $block_name . '__cta-list">';
              foreach ( $links as $index => $item ) {

                $cta = [];
                extract( $item );

                if ( $cta ) {
                  $cta['classes'] = 'button button--pill button--secondary';
                  $html .= '<div class="' . $block_name . '__cta">' . $this->render_cta( $cta ) . '</div>';
                }

              }
            $html .= '</div>';
          }

        $html .= $this->render_container( 'closed' );
      $html .= '</div>';

    }

    return $html;

  }

  // --------------------------- Section Intro
  public function render_section_intro( $params = [] ) {

    $block_name = 'intro';
    $defaults = [ 'post_id' => false ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      // ---------------------------------------- Vars
      $featured_image = $this->get_featured_image_by_post_id( $post_id );
      $intro = get_field( 'intro', $post_id ) ? get_field( 'intro', $post_id ) : [];
      $cta = $heading = $layout = $message = $theme = false;

      // ---------------------------------------- Extract Data
      extract( $intro );

      // ---------------------------------------- Template
      $html .= '<section
        class="section section--intro intro"
        data-background-colour="' . $theme . '"
        data-colour="black"
        data-style="' . $layout . '"
      >';

        $html .= '<div class="' . $block_name . '__main">';

          if ( $featured_image ) {
            $html .= '<div class="' . $block_name . '__image">';
              $html .= $this->render_lazyload_image( $featured_image, [ 'alt_text' => $heading ] );
            $html .= '</div>';
          }

          if ( $featured_image && ( $heading || $message ) ) {
            $html .= '<div class="' . $block_name . '__vr"></div>';
          }

          if ( $heading || $message ) {
            $html .= '<div class="' . $block_name . '__content">';
              $html .= $heading ? '<h2 class="' . $block_name . '__heading heading heading--title">' . $heading  . '</h2>' : '';
              $html .= $message ? '<div class="' . $block_name . '__message message rte">' . $message  . '</div>' : '';
              if ( $cta ) {
                $cta['classes'] = 'button button--pill button--secondary';
                $html .= '<div class="' . $block_name . '__cta">' . $this->render_cta( $cta ) . '</div>';
              }
            $html .= '</div>';
          }

        $html .= '</div>';

      $html .= '</section>';

    }

    return $html;

  }

  // --------------------------- Preload Fonts
  public function render_seo( $enable = true ) {
    $html = '<meta name="robots" content="noindex, nofollow">';
    if ( $enable && !is_attachment( $this->get_theme_info('post_ID') ) ) {
			if ( defined( 'WPSEO_VERSION' ) ) {
				$html = '<!-- Yoast Plugin IS ACTIVE! -->';
			} else {
				$html = '<!-- Yoast Plugin IS NOT ACTIVE! -->';
				$html .= '<meta name="description" content="' . get_bloginfo( 'description' ) . '">';
			}
    }
    return $html;
  }

  // --------------------------- Share URL
  public function render_share_url( $params = [] ) {

    $block_name = '';
    $defaults = [
      'account' => '',
      'post_id' => false,
      'type' => 'twitter',
    ];
    $html = '';

    extract( array_merge( $defaults, $params ) );

    if ( $post_id ) {

      $title = get_the_title( $post_id ) ? get_the_title( $post_id ) : false;
      $url = get_the_permalink( $post_id ) ? urlencode( trim( get_the_permalink( $post_id ) ) ) : false;

      /*
        // ---------------------------------------- Facebook Share URL
        $app_id = 3549970701729692;
        $display = "page";
        $share_url = 'https://www.facebook.com/dialog/share?app_id=' . $app_id . '&display=' . $display . '&href=' . $encoded_permalink . '&redirect_uri=' . $encoded_permalink;

        // ---------------------------------------- LinkedIn Share URL
        https://www.linkedin.com/shareArticle?mini=true&url=https://micromodal.now.sh/&title=JamesBond
        https://www.linkedin.com/shareArticle?mini=true&url=https%3A//verypoliteagency.teamwork.com&title=Sample%20Title&summary=&source=
        https://www.linkedin.com/sharing/share-offsite/?url=' . $encoded_permalink

        // ---------------------------------------- Twitter Share URL
        $from_twitter_account = 'PunitDhillon';
        $share_url = 'https://twitter.com/intent/tweet?url=' . $encoded_permalink . '&via=' . $from_twitter_account;
      */

      if ( $url ) {
        switch( $type ) {

          case 'facebook':
            $app_id = 3549970701729692;
            $display = "page";
            $html .= 'https://www.facebook.com/dialog/share?app_id=' . $app_id . '&display=' . $display . '&href=' . $url . '&redirect_uri=' . $url;
            break;

          case 'linkedin':
          case 'linked-in':
            $html .= 'https://www.linkedin.com/sharing/share-offsite/?url=' . $url;
            break;

          case 'twitter':
            $html .= 'https://twitter.com/intent/tweet?url=' . $url;
            $html .= $account ? '&via=' . $account : '';
            break;

        }
      }

    }

    return $html;

  }

  // --------------------------- Subnavigation Item
  public function render_subnavigation_item( $params = [], $current_id = 0 ) {

    // default data
    $html = '';
    $block_name = 'subnavigation';
    $link = $link_attachment = $link_category = $link_external = $link_id = $link_page = $link_type = $title = false;
    $target = '_self';

    extract( $params );

    switch( $link_type ) {
      case 'attachment':
        $link = ( isset($link_attachment['url']) ) ? $link_attachment['url'] : false;
        $target = '_blank';
        break;
      case 'category':
        $link_id = $link_category;
        $link = get_category_link( $link_id );
        break;
      case 'external':
        $link = $link_external;
        $target = '_blank';
        break;
      case 'page':
        $link_id = $link_page;
        $link = get_permalink( $link_id );
        break;
    }

    $is_active = ( $current_id === $link_id ) ? true : false;

    if ( $title && $link ) {
      $html .= '<div class="' . $block_name . '__item" data-is-active="' . ( $is_active ? 'true' : 'false' ) . '">';
        $html .= '<a class="' . $block_name . '__link' . ( $is_active ? ' active' : '' ) . '" href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      $html .= '</div>';
    }

    return $html;

  }

  // --------------------------- SVG Icon
  public function render_svg_icon( $params = [] ) {

    $html = '';
    $default_args = [ 'type' => 'burger' ];

    extract( array_merge( $default_args, $params ) );

    switch( $type ) {

      case 'burger':
        $html = '<svg width="23" height="16" viewBox="0 0 23 16" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect width="23" height="2"/>
          <rect y="7" width="23" height="2"/>
          <rect y="14" width="23" height="2"/>
        </svg>';
        break;

      case 'close':
        $html = '<svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
          <rect x="0.661133" y="16.4246" width="23" height="2" transform="rotate(-45 0.661133 16.4246)" />
          <rect x="2.0752" y="0.161133" width="23" height="2" transform="rotate(45 2.0752 0.161133)" />
        </svg>';
        break;

      case 'facebook':
        $html = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
          <g id="fd368b8b-cedd-4990-b9da-d9f6c4d8f3e1">
	          <path class="st0" d="M37.2,43.5c-0.1,0-0.2,0-0.3,0l-6.4,0c-0.3,0-0.5-0.2-0.5-0.5V29.3c0-0.3,0.2-0.5,0.5-0.5h5.5v-5h-5.5
		c-0.3,0-0.5-0.2-0.5-0.5v-2.4c0-0.5,0.1-1.1,0.5-1.5c0.2-0.3,0.6-0.5,1-0.6c0,0,0.1,0,0.1,0h4.3v-5.1h-4.4c-0.8,0-1.7,0.2-2.4,0.6
		c-0.8,0.4-1.5,1-2.1,1.6c-0.6,0.7-1,1.4-1.4,2.3c-0.3,0.9-0.5,1.8-0.5,2.7v2.4c0,0.3-0.2,0.5-0.5,0.5H21v5h3.9
		c0.3,0,0.5,0.2,0.5,0.5v13.6c0,0.3-0.2,0.5-0.5,0.5H13c-0.9,0-1.7-0.2-2.5-0.5c-0.8-0.4-1.5-0.8-2.1-1.5c-0.6-0.6-1.1-1.3-1.4-2
		c-0.3-0.8-0.5-1.6-0.5-2.4c0,0,0,0,0,0V12.9c0-0.9,0.2-1.7,0.5-2.5C7.3,9.7,7.8,9,8.5,8.4C9.1,7.8,9.8,7.3,10.6,7
		c0.7-0.4,1.6-0.5,2.5-0.5l24,0c0.9,0,1.7,0.2,2.5,0.5c0.8,0.3,1.5,0.8,2.1,1.5c0.6,0.6,1.1,1.3,1.4,2c0.4,0.8,0.5,1.6,0.5,2.5l0,24
		c0,0.9-0.2,1.7-0.5,2.5c-0.3,0.8-0.8,1.5-1.5,2.1c-0.6,0.6-1.3,1.1-2,1.4C38.8,43.3,38,43.5,37.2,43.5z M31.1,42.5H37
		c0.7,0,1.4-0.1,2-0.4c0.7-0.3,1.3-0.7,1.8-1.3c0.5-0.5,1-1.1,1.3-1.8c0.3-0.7,0.4-1.4,0.4-2.1V13c0-0.7-0.1-1.4-0.4-2
		c-0.3-0.7-0.7-1.3-1.3-1.8c-0.5-0.5-1.1-1-1.8-1.3c-0.7-0.3-1.4-0.4-2.1-0.4H13c-0.7,0-1.4,0.1-2,0.4c-0.7,0.3-1.3,0.7-1.8,1.2
		c-0.6,0.5-1,1.1-1.3,1.8c-0.3,0.7-0.4,1.4-0.4,2.1V37c0,0.7,0.1,1.3,0.4,2c0.3,0.6,0.7,1.2,1.3,1.8c0.5,0.5,1.1,1,1.8,1.3
		c0.7,0.3,1.4,0.4,2.1,0.4h11.4V29.8h-3.9c-0.3,0-0.5-0.2-0.5-0.5v-6c0-0.3,0.2-0.5,0.5-0.5h3.9v-1.9c0-1.1,0.2-2.1,0.6-3.1
		c0.4-0.9,0.9-1.8,1.5-2.6c0.7-0.8,1.4-1.4,2.3-1.8c0.9-0.5,1.9-0.7,2.9-0.7h4.8c0.3,0,0.5,0.2,0.5,0.5v6.1c0,0.3-0.2,0.5-0.5,0.5
		h-4.8c-0.2,0.1-0.3,0.1-0.4,0.3c-0.2,0.2-0.3,0.5-0.3,0.8l0,1.9h5.5c0.3,0,0.5,0.2,0.5,0.5v6c0,0.3-0.2,0.5-0.5,0.5h-5.5V42.5z
		 M7.5,37L7.5,37L7.5,37z"/>
          </g>
        </svg>';
        break;

      case 'glide.control.next':
        $html = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <circle cx="12" cy="12" r="12"/>
          <path d="M10 7L15 12L10 17"/>
        </svg>';
        break;

      case 'heart':
        $html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116 100">
          <g>
            <path d="M58,100a2.64,2.64,0,0,1-1.6-.61L55,98.3c-9.15-6.83-18.61-13.9-27.2-21.75C18.21,67.82,11.47,59.78,6.54,51.23A46.51,46.51,0,0,1,.32,33.77l0-.16A21.68,21.68,0,0,1,0,29.48C.05,15.55,9,4.39,22.94,1A32.46,32.46,0,0,1,44.3,2.76,31.38,31.38,0,0,1,58,14.61C63.61,6.1,71.35,1.28,81,.27,92.14-.9,101.45,2.4,108.68,10.05A26.64,26.64,0,0,1,115.81,26c.81,7.73-1,15.4-5.65,24.15C105.8,58.3,99.78,65.26,95,70.42,88,78,79.87,84.26,72,90.31,68.2,93.2,64.29,96,60.51,98.76l-.91.65A2.7,2.7,0,0,1,58,100ZM31.37,2.08a33.36,33.36,0,0,0-7.94,1C10.48,6.18,2.12,16.57,2.11,29.52v.08a19.84,19.84,0,0,0,.26,3.74l0,.16a44.53,44.53,0,0,0,6,16.69C13.17,58.55,19.78,66.44,29.17,75c8.51,7.78,17.93,14.82,27,21.62l1.46,1.09c.35.26.35.26.71,0l.91-.65c3.77-2.73,7.66-5.54,11.41-8.41,7.83-6,15.93-12.21,22.79-19.65,5-5.38,10.61-11.95,14.83-19.87,4.46-8.36,6.19-15.65,5.42-23a24.6,24.6,0,0,0-6.57-14.72C100.4,4.31,91.68,1.24,81.23,2.33,72.11,3.28,64.82,7.9,59.57,16c-.46.72-.86,1.32-1.56,1.28s-1.1-.57-1.54-1.28a29.46,29.46,0,0,0-25.1-14Z"/>
          </g>
        </svg>';
        break;

      case 'likes':
        $html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116 100">
          <g>
            <path d="M58,100a2.64,2.64,0,0,1-1.6-.61L55,98.3c-9.15-6.83-18.61-13.9-27.2-21.75C18.21,67.82,11.47,59.78,6.54,51.23A46.51,46.51,0,0,1,.32,33.77l0-.16A21.68,21.68,0,0,1,0,29.48C.05,15.55,9,4.39,22.94,1A32.46,32.46,0,0,1,44.3,2.76,31.38,31.38,0,0,1,58,14.61C63.61,6.1,71.35,1.28,81,.27,92.14-.9,101.45,2.4,108.68,10.05A26.64,26.64,0,0,1,115.81,26c.81,7.73-1,15.4-5.65,24.15C105.8,58.3,99.78,65.26,95,70.42,88,78,79.87,84.26,72,90.31,68.2,93.2,64.29,96,60.51,98.76l-.91.65A2.7,2.7,0,0,1,58,100ZM31.37,2.08a33.36,33.36,0,0,0-7.94,1C10.48,6.18,2.12,16.57,2.11,29.52v.08a19.84,19.84,0,0,0,.26,3.74l0,.16a44.53,44.53,0,0,0,6,16.69C13.17,58.55,19.78,66.44,29.17,75c8.51,7.78,17.93,14.82,27,21.62l1.46,1.09c.35.26.35.26.71,0l.91-.65c3.77-2.73,7.66-5.54,11.41-8.41,7.83-6,15.93-12.21,22.79-19.65,5-5.38,10.61-11.95,14.83-19.87,4.46-8.36,6.19-15.65,5.42-23a24.6,24.6,0,0,0-6.57-14.72C100.4,4.31,91.68,1.24,81.23,2.33,72.11,3.28,64.82,7.9,59.57,16c-.46.72-.86,1.32-1.56,1.28s-1.1-.57-1.54-1.28a29.46,29.46,0,0,0-25.1-14Z"/>
          </g>
        </svg>';
        break;

      case 'linkedin':
      case 'linked-in':
        $html = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
          <g id="abf7c095-13b0-4a9f-a9e1-53e7e7216bec">
          	<path class="st0" d="M45,43.3h-8.5c-0.3,0-0.5-0.2-0.5-0.5V30.2c0-0.7-0.1-1.4-0.2-2.1c-0.1-0.5-0.4-1-0.7-1.5
          		c-0.3-0.4-0.7-0.7-1.1-1c-0.5-0.3-1.1-0.4-1.8-0.4c-1,0-1.9,0.3-2.7,0.8c-0.6,0.5-1,1.1-1.4,1.7C28.1,28,28,28.3,28,28.5
          		c0,0.2,0,0.4-0.1,0.6c0,0.2,0,0.3,0,0.4v13.1c0,0.3-0.2,0.5-0.5,0.5h-8.6c-0.3,0-0.5-0.2-0.5-0.5V18.5c0-0.3,0.2-0.5,0.5-0.5h8.6
          		c0.3,0,0.5,0.2,0.5,0.5v2.3c0,0,0.1-0.1,0.1-0.1c0.5-0.6,1-1.1,1.6-1.5c0.7-0.5,1.5-0.9,2.4-1.1c1-0.3,2.1-0.5,3.1-0.4
          		c1.4,0,2.7,0.2,4.1,0.6c1.3,0.4,2.4,1.2,3.3,2.1c1,1,1.7,2.2,2.2,3.5c0.5,1.6,0.8,3.3,0.7,5v13.9C45.5,43.1,45.3,43.3,45,43.3z
          		 M37,42.3h7.5V28.9c0.1-1.6-0.2-3.2-0.7-4.7c-0.4-1.2-1.1-2.2-2-3.1c-0.8-0.8-1.8-1.5-2.9-1.9c-1.2-0.4-2.5-0.6-3.8-0.6
          		c-1,0-1.9,0.1-2.9,0.4c-0.8,0.2-1.5,0.5-2.1,0.9c-0.5,0.4-1,0.8-1.4,1.3c-0.2,0.3-0.4,0.6-0.6,0.8c-0.1,0.2-0.2,0.4-0.4,0.5
          		c-0.1,0.2-0.4,0.3-0.6,0.2c-0.2-0.1-0.3-0.3-0.3-0.5V19h-7.6v23.1h7.6V29.5c0-0.2,0-0.4,0.1-0.6c0-0.2,0-0.3,0-0.4
          		c0-0.4,0.1-0.8,0.2-1.1c0.4-0.8,1-1.5,1.7-2.1c0.9-0.7,2.1-1.1,3.3-1c0.7,0,1.5,0.1,2.2,0.5c0.6,0.3,1.1,0.7,1.5,1.3
          		c0.4,0.6,0.7,1.2,0.9,1.9c0.1,0.8,0.2,1.6,0.2,2.3V42.3z M14,43.3H5.5c-0.3,0-0.5-0.2-0.5-0.5v-24c0-0.3,0.2-0.5,0.5-0.5H14
          		c0.3,0,0.5,0.2,0.5,0.5v24C14.5,43.1,14.3,43.3,14,43.3z M6,42.3h7.5v-23H6V42.3z M9.1,16c-1.2,0-2.4-0.5-3.3-1.3
          		c-0.9-0.9-1.4-2.1-1.3-3.3c0-0.6,0.1-1.3,0.3-1.9C5.1,8.8,5.5,8.3,6,7.9C6.5,7.5,7,7.2,7.6,7c0.7-0.2,1.4-0.3,2.2-0.3
          		c0.6,0,1.3,0.1,1.9,0.3c0.6,0.2,1.1,0.6,1.5,1c0.4,0.4,0.7,0.9,0.9,1.5c0.2,0.6,0.3,1.2,0.3,1.8c0,1.2-0.5,2.4-1.4,3.3
          		c-1,0.9-2.3,1.4-3.7,1.3C9.3,16,9.2,16,9.1,16z M9.8,7.7C9.1,7.7,8.5,7.8,7.9,8C7.5,8.2,7,8.4,6.6,8.7C6.2,9,5.9,9.4,5.8,9.8
          		c-0.2,0.5-0.3,1-0.3,1.5c0,0,0,0,0,0c0,1,0.4,1.9,1,2.6C7.3,14.6,8.3,15,9.4,15c1.1,0.1,2.2-0.3,3-1.1c0.7-0.7,1.1-1.6,1-2.5
          		c0-0.5-0.1-1-0.3-1.4c-0.2-0.4-0.4-0.8-0.7-1.1c-0.3-0.3-0.7-0.6-1.1-0.8C10.8,7.8,10.3,7.7,9.8,7.7C9.8,7.7,9.8,7.7,9.8,7.7z
          		M5,11.3L5,11.3L5,11.3z"/>
          </g>
        </svg>';
        break;

      case 'punit':
        $html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 867 500">
          <path d="M191.51,277.17H160.78v73.35H171V319.09h20.52c15.42,0,24.12-8,24.12-21S206.93,277.17,191.51,277.17Zm-.23,33.12H171V286h20.29c9.16,0,13.8,4.09,13.8,12.16S200.44,310.29,191.28,310.29Z"/>
          <path d="M308.72,351.36c8.34,0,15.18-2.3,20.63-7s8.12-11.11,8.12-19.28V277.17h-10.2v48.52c0,10.06-6.84,16.87-18.55,16.87s-18.55-6.81-18.55-16.87V277.17H280v47.89c0,8.17,2.67,14.57,8.12,19.28S300.37,351.36,308.72,351.36Z"/>
          <polygon points="416.59 293.94 450.56 350.52 461.69 350.52 461.69 277.17 451.49 277.17 451.49 333.76 417.52 277.17 406.39 277.17 406.39 350.52 416.59 350.52 416.59 293.94"/>
          <polygon points="579.39 342.56 559.79 342.56 559.79 285.14 579.39 285.14 579.39 277.17 529.99 277.17 529.99 285.14 549.59 285.14 549.59 342.56 529.99 342.56 529.99 350.52 579.39 350.52 579.39 342.56"/>
          <polygon points="673.05 350.52 683.26 350.52 683.26 285.97 708.53 285.97 708.53 277.17 647.78 277.17 647.78 285.97 673.05 285.97 673.05 350.52"/><path d="M85.45,464.6c6.61-6.19,9.85-15.3,9.85-27.35s-3.24-21.17-9.85-27.35-15.19-9.33-26.21-9.33H38.72v73.35H59.24C70.26,473.92,79,470.78,85.45,464.6Zm-36.53.52V409.37H58.2c17,0,26.2,8.17,26.2,27.88s-9.16,27.87-26.2,27.87Z"/>
          <polygon points="169.51 439.34 205.23 439.34 205.23 473.92 215.43 473.92 215.43 400.57 205.23 400.57 205.23 430.54 169.51 430.54 169.51 400.57 159.31 400.57 159.31 473.92 169.51 473.92 169.51 439.34"/>
          <polygon points="334.77 465.96 315.18 465.96 315.18 408.53 334.77 408.53 334.77 400.57 285.38 400.57 285.38 408.53 304.97 408.53 304.97 465.96 285.38 465.96 285.38 473.92 334.77 473.92 334.77 465.96"/>
          <polygon points="459.07 465.12 417.67 465.12 417.67 400.57 407.47 400.57 407.47 473.92 459.07 473.92 459.07 465.12"/><polygon points="583.69 465.12 542.3 465.12 542.3 400.57 532.09 400.57 532.09 473.92 583.69 473.92 583.69 465.12"/>
          <path d="M680.12,474.76c9.27,0,16.81-3.25,22.72-9.64s8.82-15.82,8.82-27.87-2.9-21.38-8.82-27.77-13.45-9.75-22.72-9.75-16.82,3.25-22.73,9.75-8.81,15.71-8.81,27.77,2.9,21.37,8.81,27.87S670.84,474.76,680.12,474.76Zm-15.31-59.21c3.83-4.71,8.93-7,15.31-7s11.48,2.31,15.3,7,5.68,11.84,5.68,21.7-1.85,17.07-5.68,21.79-8.93,6.92-15.3,6.92-11.48-2.31-15.31-6.92-5.68-12-5.68-21.79S661,420.16,664.81,415.55Z"/>
          <polygon points="785.32 417.33 819.29 473.92 830.42 473.92 830.42 400.57 820.22 400.57 820.22 457.15 786.25 400.57 775.12 400.57 775.12 473.92 785.32 473.92 785.32 417.33"/>
          <path d="M743.92,370.94h-.13V247.22H620.7V123.61H497.62V0H369.11V123.61H246V247.22H123V370.94H0V500H867V370.94ZM374.54,129.06H492.19V247.22H374.54ZM615.28,370.83H497.62V252.67H615.28ZM492.19,252.67V370.83H374.54V252.67ZM369.11,370.83H251.46V252.67H369.11ZM738.36,252.67V370.83H620.7V252.67ZM615.28,129.06V247.22H497.62V129.06ZM374.54,5.45H492.19V123.61H374.54ZM251.46,129.06H369.11V247.22H251.46ZM128.38,252.67H246V370.83H128.38Zm-5.3,241.88H5.43V376.39H123.08Zm123.08,0H128.51V376.39H246.16Zm123.08,0H251.59V376.39H369.24Zm123.09,0H374.67V376.39H492.33Zm123.08,0H497.75V376.39H615.41Zm123.08,0H620.84V376.39H738.49Zm123.08,0H743.92V376.39H861.57Z"/>
          </svg>';
        break;

      case 'punit.small':
        $html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 298 200">
          <path d="M138,56.53h16.34c12.27,0,19.19-6.31,19.19-16.59s-6.92-16.6-19.19-16.6H129.86V81.43H138Zm0-26.22h16.15c7.29,0,11,3.23,11,9.63s-3.69,9.62-11,9.62H138Z"/>
          <path d="M200.11,97.84V0H97.9V97.84H0V200H298V97.84ZM97.9,195.68H4.32V102.16H97.9Zm97.89,0H102.21V102.16h93.58Zm0-97.84H102.21V4.32h93.58Zm97.89,97.84H200.11V102.16h93.57Z"/>
          <path d="M263.65,171.68c5.26-4.9,7.84-12.12,7.84-21.66s-2.58-16.76-7.84-21.66S251.56,121,242.8,121H226.47v58.1H242.8C251.56,179.07,258.48,176.58,263.65,171.68Zm-29.07.41V127.94H242c13.56,0,20.85,6.48,20.85,22.08s-7.29,22.07-20.85,22.07Z"/>
        </svg>';
        break;

      case 'punit.small.alt':
        $html = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 179 200">
          <path class="a198aed3-cc75-4e5b-9548-72313d17a451" d="M82.46,61.86a17.43,17.43,0,0,0,1.22-6.65A19.58,19.58,0,0,0,82.6,48.6a14.25,14.25,0,0,0-3.22-5.25,14.61,14.61,0,0,0-5.33-3.46,20.75,20.75,0,0,0-7.54-1.25H47V87.81h9.26V71.25H67.11a19.11,19.11,0,0,0,6.7-1.13,15.57,15.57,0,0,0,5.24-3.21A14.05,14.05,0,0,0,82.46,61.86Zm-10.58-.67q-2.14,2.3-6.48,2.3H56.24V46.39h8.82a15.28,15.28,0,0,1,3.64.42A6.52,6.52,0,0,1,73.36,51,11.72,11.72,0,0,1,74,55.21,8.47,8.47,0,0,1,71.88,61.19Z"/>
          <path class="a198aed3-cc75-4e5b-9548-72313d17a451" d="M134.14,123.52a19.89,19.89,0,0,0-7.53-5.14,29.22,29.22,0,0,0-10.92-1.85H98.45v49.16h19q11.05,0,16.74-6.25t5.68-18.33a33.31,33.31,0,0,0-1.38-9.78A20.73,20.73,0,0,0,134.14,123.52ZM129.24,149a11.5,11.5,0,0,1-2.7,4.91,9.59,9.59,0,0,1-4.42,2.51,22.75,22.75,0,0,1-6,.73h-8.38v-32h8.18a16.88,16.88,0,0,1,6.5,1.13,11.13,11.13,0,0,1,4.41,3.24,13.76,13.76,0,0,1,2.54,5.09,24.6,24.6,0,0,1,.82,6.56A28.45,28.45,0,0,1,129.24,149Z"/>
          <path class="a198aed3-cc75-4e5b-9548-72313d17a451" d="M119.77,77.65a61.08,61.08,0,1,0-60.86,44.7,61.08,61.08,0,1,0,60.86-44.7ZM3.51,61.2A57.43,57.43,0,1,1,116,77.64a61.22,61.22,0,0,0-55.91,41.07A57.54,57.54,0,0,1,3.51,61.2ZM114.72,81.35A57.56,57.56,0,0,1,64,118.65,57.55,57.55,0,0,1,114.72,81.35Zm3,115a57.5,57.5,0,0,1-55-74,61.19,61.19,0,0,0,55.9-41.07,57.52,57.52,0,0,1-.88,115Z"/>
        </svg>';
        break;

      case 'torpedo':
        $html = '<svg width="216" height="27" viewBox="0 0 216 27" fill="none" xmlns="http://www.w3.org/2000/svg">
          <style>
            .secondary {
              fill: var(--theme-color-secondary);
            }
          </style>
          <g clip-path="url(#clip0)">
          <path class="secondary" d="M21.4248 7.5V0.639605C21.4248 0.579237 21.4011 0.521341 21.3589 0.478655C21.3167 0.435968 21.2595 0.411987 21.1999 0.411987H0.233723C0.174067 0.411987 0.116854 0.435968 0.0746707 0.478655C0.0324873 0.521341 0.00878906 0.579237 0.00878906 0.639605V7.46813H0.458658L3.52226 1.52504C3.59523 1.38565 3.70209 1.26738 3.83274 1.18136C3.9634 1.09534 4.11355 1.04441 4.26905 1.03338H6.29345C6.8153 0.999241 7.25842 1.27921 7.26742 1.80728L7.45412 12.1275V14.5243L7.27642 25.063C7.26907 25.2789 7.18607 25.4852 7.04226 25.6449C6.89844 25.8046 6.70319 25.9073 6.4914 25.9347H4.63569V26.6176H16.8586V25.9347H15.0074C14.5328 25.9006 14.1864 25.6684 14.1774 25.1882L14.002 14.5243V12.1343L14.1774 1.8437C14.1774 1.31791 14.6273 1.01745 15.1424 1.04704H17.142C17.2993 1.05573 17.4517 1.10534 17.5845 1.19107C17.7173 1.2768 17.826 1.39575 17.9001 1.53642L20.9817 7.50683L21.4248 7.5Z"/>
          <path class="secondary" d="M37.5438 27C35.3349 27 33.3937 26.6517 31.7562 25.9211C30.1886 25.26 28.7744 24.2755 27.6039 23.0303C26.502 21.8032 25.66 20.3609 25.1296 18.7921C24.5635 17.1585 24.2804 15.4382 24.2929 13.7071C24.2862 11.9446 24.5956 10.1955 25.2061 8.54474C25.7879 6.90163 26.7011 5.39892 27.8875 4.13207C29.074 2.86521 30.5079 1.8619 32.0981 1.18587C33.9377 0.38897 35.9231 -0.00509045 37.9239 0.029569C40.1328 0.029569 42.0739 0.375547 43.7474 1.10848C45.3085 1.77779 46.7109 2.77561 47.8615 4.03564C48.9573 5.25261 49.7989 6.68102 50.3358 8.23518C50.8719 9.87534 51.1416 11.5926 51.1343 13.3202C51.1348 15.0795 50.8389 16.8261 50.2593 18.4848C49.6707 20.1057 48.7786 21.5965 47.6321 22.8755C46.4292 24.1671 44.9737 25.1911 43.3583 25.8824C41.5137 26.6453 39.5364 27.0253 37.5438 27ZM37.7687 26.3604C38.6961 26.3552 39.5976 26.0506 40.3419 25.4909C41.1914 24.8108 41.8793 23.9467 42.3551 22.962C42.9569 21.7043 43.3954 20.3731 43.6597 19.0015C43.9711 17.3213 44.1218 15.6146 44.1096 13.9051C44.1044 12.2986 43.9675 10.6952 43.7002 9.11151C43.4698 7.66021 43.0688 6.24197 42.5058 4.88693C42.0281 3.75903 41.3409 2.73441 40.4814 1.86872C40.1197 1.48979 39.6848 1.19019 39.2038 0.988528C38.7229 0.786869 38.206 0.687463 37.6855 0.696489C36.7618 0.698241 35.8666 1.0195 35.1482 1.60696C34.2988 2.30175 33.6114 3.17766 33.135 4.17221C32.5164 5.41116 32.0763 6.73339 31.8282 8.09861C31.4885 9.76273 31.3301 11.4595 31.3558 13.1586C31.3611 14.7772 31.498 16.3927 31.7652 17.9886C31.996 19.445 32.4102 20.8656 32.9978 22.2155C33.457 23.338 34.1278 24.3593 34.9728 25.2223C35.342 25.5893 35.7793 25.8788 36.2595 26.0741C36.7398 26.2694 37.2534 26.3667 37.7709 26.3604H37.7687Z"/>
          <path class="secondary" d="M79.9013 25.9279V26.6108H70.6183C70.4906 26.6115 70.3653 26.5755 70.257 26.507C70.1487 26.4385 70.0618 26.3403 70.0065 26.2238L64.5878 15.0569C64.5324 14.9404 64.4456 14.8422 64.3373 14.7737C64.229 14.7052 64.1037 14.6693 63.976 14.67H63.1527C63.0626 14.6699 62.9733 14.6882 62.8902 14.7236C62.8072 14.7591 62.732 14.811 62.6691 14.8764C62.6062 14.9417 62.5568 15.0191 62.524 15.1041C62.4912 15.1891 62.4755 15.2798 62.4779 15.371L62.6466 25.4044C62.6466 25.7618 62.9278 25.9029 63.2809 25.9279H65.3211V26.6108H53.0981V25.9279H55.1405C55.3016 25.906 55.4494 25.826 55.5568 25.7027C55.6643 25.5793 55.7241 25.4209 55.7254 25.2564L55.9053 14.5175V12.1275L55.7344 1.77769C55.7344 1.43854 55.4824 1.04704 55.1518 1.04704H53.0981V0.411988H66.0814C67.3867 0.411631 68.6884 0.553573 69.9637 0.835357C71.0864 1.08957 72.1586 1.53358 73.1353 2.14871C74.002 2.72249 74.7306 3.48549 75.2677 4.38164C75.7867 5.40588 76.0449 6.545 76.019 7.69575C76.0143 8.66515 75.7073 9.60832 75.1417 10.3907C74.4842 11.2779 73.6715 12.0355 72.7439 12.6259C71.6482 13.3101 70.4537 13.8169 69.2034 14.1282C67.8718 14.4742 65.0017 14.7678 65.7192 14.9476L73.0746 17.4742C73.2296 17.5291 73.3604 17.638 73.4435 17.7815L78.0164 25.5865C78.0695 25.681 78.1442 25.7612 78.2342 25.8206C78.3242 25.88 78.4269 25.9168 78.5337 25.9279H79.9013ZM64.6733 14.2967C65.2927 14.3085 65.9017 14.1342 66.4233 13.7959C66.9775 13.4593 67.4472 12.9973 67.7954 12.4461C68.2063 11.8105 68.5025 11.1061 68.6703 10.3657C68.8754 9.49098 68.9774 8.59475 68.974 7.69575C68.9771 6.89138 68.8881 6.08936 68.7086 5.30577C68.5267 4.53357 68.2318 3.79322 67.8336 3.10926C67.4545 2.47361 66.9644 1.91292 66.3873 1.45448C65.8159 1.03111 65.1321 1.04249 64.3629 1.04249H62.6534L62.4644 12.1275V13.5933C62.4644 13.7744 62.5355 13.9481 62.6621 14.0762C62.7886 14.2042 62.9602 14.2762 63.1392 14.2762L64.6733 14.2967Z"/>
          <path class="secondary" d="M95.1065 25.9279V26.6108H82.1323V25.9279H84.186C84.347 25.906 84.4948 25.826 84.6023 25.7027C84.7097 25.5793 84.7696 25.4209 84.7708 25.2564L84.9508 14.5175V12.1275L84.7618 1.77769C84.7602 1.60696 84.7016 1.4418 84.5954 1.30912C84.4892 1.17644 84.3417 1.08406 84.177 1.04704H82.1323V0.411988H95.4957C96.8018 0.411626 98.1042 0.553567 99.3803 0.835357C100.506 1.08601 101.578 1.5435 102.541 2.18513C103.424 2.83575 104.153 3.67798 104.673 4.65023C105.204 5.78023 105.465 7.02071 105.436 8.27162C105.436 9.85129 105.13 11.1237 104.52 12.0478C103.934 12.9777 103.103 13.7247 102.122 14.2056C101.046 14.7382 99.8857 15.0771 98.6943 15.2071C97.3675 15.3623 96.0328 15.4383 94.6972 15.4347H92.2229C92.1327 15.4347 92.0435 15.453 91.9604 15.4884C91.8774 15.5239 91.8022 15.5758 91.7393 15.6411C91.6764 15.7065 91.627 15.7839 91.5942 15.8689C91.5614 15.9539 91.5457 16.0446 91.5481 16.1358L91.6808 25.3885C91.6808 25.7504 91.9665 25.9029 92.3241 25.9234L95.1065 25.9279ZM97.1625 13.0357C98.0015 11.9181 98.4198 10.5091 98.3816 8.2739C98.383 7.40646 98.294 6.54129 98.1162 5.69272C97.9802 4.94358 97.7321 4.2198 97.3806 3.54628C96.9647 2.84691 96.4266 2.2299 95.7926 1.72534C95.2169 1.26625 94.501 1.02476 93.7682 1.04249H92.3444C92.1654 1.04249 91.9937 1.11443 91.8672 1.24249C91.7406 1.37055 91.6695 1.54424 91.6695 1.72534L91.4896 11.8452V14.085C91.4896 14.2661 91.5607 14.4398 91.6872 14.5678C91.8138 14.6959 91.9854 14.7678 92.1644 14.7678H93.9931C95.2595 14.7701 96.3257 14.1897 97.1625 13.0357Z"/>
          <path class="secondary" d="M128.213 19.4863L124.258 25.7276C124.137 25.9211 123.928 25.9211 123.703 25.9279H117.274C117.187 25.9374 117.099 25.9286 117.016 25.9022C116.933 25.8758 116.855 25.8323 116.789 25.7744C116.723 25.7166 116.67 25.6456 116.632 25.5659C116.594 25.4861 116.573 25.3994 116.57 25.3111L116.421 14.2557C116.421 13.8551 116.748 13.6639 117.144 13.6866H119.348C119.453 13.693 119.555 13.7236 119.646 13.7761C119.737 13.8286 119.815 13.9015 119.874 13.9894L122.731 18.2527H123.044V8.42868H122.731L119.874 12.6578C119.816 12.7451 119.738 12.8175 119.647 12.8692C119.556 12.921 119.454 12.9507 119.35 12.956H117.144C116.748 12.9788 116.417 12.7876 116.421 12.3869L116.59 1.5C116.59 1.11533 116.912 1.03111 117.292 1.04476H124.173C124.288 1.03637 124.404 1.06039 124.507 1.1142C124.611 1.168 124.697 1.2495 124.758 1.34977L127.997 7.54325H128.469V0.411987H107.044V1.04021H109.098C109.262 1.07723 109.41 1.16961 109.516 1.30229C109.622 1.43497 109.681 1.60013 109.682 1.77086L109.862 12.1252V14.5243L109.682 25.2633C109.681 25.4277 109.621 25.5861 109.514 25.7095C109.406 25.8328 109.259 25.9128 109.098 25.9347H107.044V26.6176H128.669V19.4863H128.213Z"/>
          <path class="secondary" d="M144.473 26.613H131.375V25.9302H133.418C133.579 25.9083 133.727 25.8283 133.834 25.7049C133.941 25.5816 134.001 25.4231 134.002 25.2587L134.185 14.5197V12.1297L134.014 1.77769C134.012 1.60696 133.953 1.4418 133.847 1.30912C133.741 1.17644 133.594 1.08406 133.429 1.04704H131.375V0.411987H144.853C147.062 0.411987 149.004 0.758725 150.679 1.4522C152.257 2.0818 153.694 3.0253 154.906 4.22686C156.012 5.41297 156.867 6.81582 157.418 8.34901C157.985 9.94232 158.268 11.6246 158.255 13.3179C158.262 15.056 157.952 16.7805 157.342 18.4052C156.735 19.9881 155.816 21.43 154.642 22.6434C153.395 23.8822 151.921 24.864 150.303 25.5341C148.585 26.2671 146.644 26.613 144.473 26.613ZM140.742 12.1275V14.5243L140.921 25.5546C140.921 25.9325 141.23 25.9347 141.596 25.9347H144.313C145.341 25.9347 146.255 25.9666 147.132 25.3862C148.035 24.7367 148.788 23.8964 149.339 22.9234C150.04 21.7301 150.542 20.4282 150.825 19.0698C151.194 17.3747 151.373 15.643 151.358 13.9074C151.351 12.3186 151.211 10.7332 150.938 9.16844C150.7 7.71431 150.267 6.29986 149.651 4.96434C149.105 3.80436 148.341 2.763 147.402 1.89833C146.489 1.12898 145.422 1.04476 144.203 1.04476H141.603C141.23 1.04476 140.928 1.04476 140.928 1.42033L140.742 12.1275Z"/>
          <path class="secondary" d="M174.758 26.9704C172.551 26.9704 170.608 26.6222 168.973 25.8915C167.406 25.2296 165.992 24.2452 164.821 23.0008C163.719 21.7736 162.877 20.3313 162.346 18.7625C161.779 17.1291 161.496 15.4087 161.509 13.6775C161.506 11.9143 161.82 10.1652 162.434 8.51517C163.016 6.87206 163.929 5.36935 165.115 4.1025C166.302 2.83564 167.736 1.83233 169.326 1.15629C171.165 0.359647 173.149 -0.0344078 175.149 -1.32923e-06C177.358 -1.32923e-06 179.302 0.345977 180.975 1.07891C182.506 1.73218 183.884 2.70441 185.019 3.9323C186.155 5.1602 187.021 6.61622 187.564 8.20561C188.1 9.84577 188.369 11.5631 188.362 13.2906C188.363 15.0499 188.067 16.7965 187.487 18.4552C186.898 20.0761 186.006 21.5669 184.86 22.846C183.657 24.1376 182.202 25.1616 180.586 25.8528C178.737 26.6174 176.755 26.9975 174.758 26.9704ZM174.983 26.3308C175.91 26.3256 176.812 26.021 177.556 25.4613C178.41 24.7825 179.102 23.9182 179.581 22.9325C180.181 21.6743 180.62 20.3432 180.885 18.9719C181.197 17.2917 181.347 15.585 181.335 13.8756C181.33 12.269 181.193 10.6656 180.926 9.08194C180.694 7.63014 180.293 6.21127 179.731 4.85508C179.252 3.72794 178.565 2.70357 177.707 1.83687C177.345 1.4584 176.909 1.15927 176.428 0.958022C175.947 0.756773 175.43 0.657693 174.909 0.666918C173.986 0.669189 173.092 0.990417 172.374 1.57739C171.52 2.27097 170.829 3.14697 170.349 4.14264C169.733 5.38213 169.293 6.70421 169.045 8.06904C168.701 9.73199 168.539 11.428 168.561 13.1267C168.566 14.7454 168.703 16.3608 168.971 17.9568C169.202 19.4128 169.615 20.8331 170.201 22.1836C170.66 23.3103 171.331 24.3356 172.178 25.2018C172.549 25.5686 172.989 25.8572 173.471 26.051C173.953 26.2447 174.469 26.3399 174.988 26.3308H174.983Z"/>
          <path d="M208.557 13.484C208.557 13.9939 208.415 13.9893 207.911 14.2966C207.886 14.3126 204.564 16.245 201.838 21.4142C201.746 21.5886 201.616 21.7393 201.458 21.8547C201.299 21.9701 201.117 22.0472 200.924 22.08C200.731 22.1127 200.534 22.1003 200.347 22.0437C200.16 21.9871 199.988 21.8878 199.845 21.7534C199.602 21.5135 199.444 21.1996 199.395 20.8599C199.346 20.5202 199.408 20.1735 199.573 19.8733C201.348 16.4749 204.737 14.7519 206.31 13.6138C206.33 13.5988 206.347 13.5791 206.358 13.5564C206.37 13.5336 206.376 13.5084 206.376 13.4829C206.376 13.4574 206.37 13.4322 206.358 13.4094C206.347 13.3867 206.33 13.367 206.31 13.352C204.735 12.2139 201.361 10.4931 199.582 7.09481C199.417 6.79457 199.355 6.44792 199.404 6.1082C199.453 5.76849 199.611 5.45455 199.854 5.21469C199.997 5.08032 200.169 4.98099 200.356 4.92437C200.543 4.86775 200.74 4.85534 200.933 4.88811C201.126 4.92088 201.308 4.99794 201.467 5.11336C201.625 5.22878 201.755 5.37948 201.847 5.55384C204.564 10.723 207.889 12.6532 207.92 12.6737C208.388 12.9947 208.557 12.981 208.557 13.484Z" fill="#FFCE00"/>
          <path d="M216 13.4841C216 13.9939 215.856 13.9894 215.352 14.2967C215.33 14.3126 212.005 16.2451 209.279 21.4143C209.187 21.5886 209.057 21.7393 208.898 21.8548C208.74 21.9702 208.557 22.0472 208.365 22.08C208.172 22.1128 207.975 22.1004 207.788 22.0438C207.601 21.9871 207.429 21.8878 207.286 21.7534C207.043 21.5136 206.885 21.1996 206.836 20.8599C206.787 20.5202 206.849 20.1735 207.014 19.8733C208.782 16.475 212.172 14.7587 213.744 13.6161C213.764 13.6011 213.781 13.5814 213.792 13.5587C213.804 13.5359 213.81 13.5108 213.81 13.4852C213.81 13.4597 213.804 13.4345 213.792 13.4117C213.781 13.389 213.764 13.3693 213.744 13.3543C212.169 12.2163 208.782 10.4955 207.014 7.09713C206.849 6.79689 206.787 6.45024 206.836 6.11052C206.885 5.77081 207.043 5.45687 207.286 5.21701C207.429 5.08264 207.601 4.98331 207.788 4.92669C207.975 4.87007 208.172 4.85766 208.365 4.89043C208.557 4.9232 208.74 5.00026 208.898 5.11568C209.057 5.2311 209.187 5.3818 209.279 5.55616C211.996 10.7254 215.321 12.6556 215.352 12.676C215.829 12.9947 216 12.981 216 13.4841Z" fill="#FFCE00"/>
          </g>
          <defs>
          <clipPath id="clip0">
          <rect width="216" height="27" fill="white"/>
          </clipPath>
          </defs>
          </svg>';
          break;

      case 'torpedo.chevrons':
        $html = '<svg width="25" height="27" viewBox="0 0 25 27" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M13.8048 13.5C13.8048 14.2991 13.5916 14.292 12.8338 14.7735C12.7966 14.7985 7.79955 17.8272 3.69908 25.9286C3.56097 26.2019 3.36519 26.4381 3.12684 26.619C2.8885 26.7999 2.61394 26.9206 2.32434 26.972C2.03474 27.0233 1.73781 27.0039 1.45646 26.9152C1.17511 26.8264 0.916848 26.6708 0.701557 26.4602C0.336415 26.0842 0.098551 25.5922 0.0245833 25.0598C-0.0493843 24.5274 0.0446365 23.9841 0.292181 23.5135C2.96154 18.1875 8.06005 15.487 10.4249 13.7033C10.4557 13.6799 10.4807 13.649 10.498 13.6134C10.5152 13.5777 10.5242 13.5383 10.5242 13.4982C10.5242 13.4582 10.5152 13.4187 10.498 13.3831C10.4807 13.3474 10.4557 13.3166 10.4249 13.2931C8.05667 11.5094 2.98185 8.81251 0.305727 3.48647C0.0581826 3.01591 -0.0358589 2.47262 0.0381088 1.9402C0.112076 1.40779 0.34994 0.915758 0.715083 0.539841C0.930373 0.329243 1.18866 0.173577 1.47001 0.0848381C1.75136 -0.00390107 2.04827 -0.0233438 2.33787 0.0280096C2.62747 0.079363 2.90202 0.20014 3.14037 0.381034C3.37872 0.561929 3.57449 0.798108 3.71261 1.07137C7.79954 9.17281 12.7999 12.1979 12.8473 12.23C13.551 12.733 13.8048 12.7116 13.8048 13.5Z"/>
          <path d="M25.0001 13.5C25.0001 14.2991 24.7836 14.2919 24.0257 14.7735C23.9919 14.7985 18.9915 17.8272 14.891 25.9286C14.7529 26.2019 14.5571 26.438 14.3188 26.6189C14.0804 26.7998 13.8058 26.9206 13.5162 26.972C13.2266 27.0233 12.9297 27.0039 12.6484 26.9151C12.367 26.8264 12.1087 26.6707 11.8935 26.4601C11.5283 26.0842 11.2904 25.5922 11.2165 25.0598C11.1425 24.5274 11.2365 23.9841 11.4841 23.5135C14.1433 18.1875 19.2418 15.4977 21.6067 13.7069C21.6374 13.6834 21.6625 13.6526 21.6797 13.6169C21.697 13.5813 21.706 13.5418 21.706 13.5018C21.706 13.4617 21.697 13.4223 21.6797 13.3866C21.6625 13.351 21.6374 13.3201 21.6067 13.2966C19.2384 11.513 14.1433 8.81605 11.4841 3.49001C11.2365 3.01945 11.1425 2.47616 11.2165 1.94374C11.2904 1.41133 11.5283 0.919299 11.8935 0.543382C12.1087 0.332784 12.367 0.177115 12.6484 0.0883762C12.9297 -0.000362938 13.2266 -0.0198029 13.5162 0.0315505C13.8058 0.0829039 14.0804 0.203681 14.3188 0.384575C14.5571 0.565469 14.7529 0.801649 14.891 1.07492C18.9779 9.17635 23.9784 12.2015 24.0257 12.2336C24.743 12.733 25.0001 12.7116 25.0001 13.5Z"/>
        </svg>';
        break;

      case 'twitter':
          $html = '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
            <g id="b5ef6d99-fd0f-477d-9a49-c37ddb37f12a">
            	<path class="st0" d="M18.1,41.7c-0.2,0-0.3,0-0.5,0c-2.3,0-4.6-0.4-6.8-1c-2.2-0.7-4.2-1.6-6.1-2.8c-0.2-0.1-0.3-0.4-0.2-0.6
            		C4.6,37,4.8,36.8,5,36.9C5.3,37,5.6,37,5.9,37L7,37c3.1,0.1,6.2-0.8,8.8-2.6c-1.4-0.2-2.7-0.8-3.8-1.6c-1.5-1.1-2.5-2.6-3-4.3
            		c-0.1-0.2,0-0.4,0.1-0.5c0.1-0.1,0.3-0.2,0.5-0.1c0.4,0.1,0.8,0.2,1.2,0.2c-1.1-0.6-2.1-1.3-2.9-2.3c-1.3-1.5-2-3.5-2-5.5
            		c0-0.2,0.1-0.4,0.2-0.5c0.1-0.1,0.3-0.1,0.5-0.1c0.5,0.3,1.1,0.5,1.6,0.7c0.1,0,0.2,0.1,0.4,0.1c-0.5-0.6-1-1.2-1.4-1.9
            		c-0.7-1.3-1-2.8-1-4.2c0-1.5,0.4-3.1,1.2-4.4c0.1-0.1,0.2-0.2,0.4-0.2c0.2,0,0.3,0.1,0.4,0.2c2,2.5,4.5,4.5,7.3,6
            		c2.7,1.4,5.7,2.2,8.7,2.4c-0.1-0.4-0.1-0.8-0.1-1.2c0-1.1,0.2-2.3,0.6-3.4c0.5-1,1.1-2,1.8-2.8c0.8-0.8,1.7-1.4,2.8-1.9
            		c1.1-0.5,2.2-0.7,3.4-0.6c1.2,0,2.4,0.3,3.6,0.7c1,0.5,1.9,1.1,2.7,1.8c0.8-0.1,1.6-0.4,2.4-0.7c0.7-0.2,1.2-0.5,1.7-0.7
            		c0.3-0.1,0.6-0.3,0.8-0.4c0.2-0.1,0.4,0,0.5,0.1c0.1,0.1,0.2,0.3,0.1,0.5c-0.5,1.3-1.2,2.5-2.2,3.4c0.1,0,0.2-0.1,0.3-0.1
            		c0.7-0.4,1.4-0.7,2.1-1c0.2-0.1,0.4,0,0.6,0.1c0.1,0.2,0.1,0.4,0,0.6c-1.1,1.6-2.5,3-4,4.3v0.7c0,2.9-0.6,5.8-1.6,8.5
            		c-1,2.8-2.6,5.4-4.6,7.7c-2.1,2.3-4.6,4.2-7.4,5.6C24.8,40.9,21.4,41.7,18.1,41.7z M7,38c1.3,0.7,2.7,1.3,4.1,1.7
            		c2.1,0.6,4.3,1,6.5,1c3.4,0.1,6.7-0.6,9.8-2c2.7-1.3,5.1-3.1,7.1-5.3c1.9-2.2,3.4-4.7,4.4-7.4c1-2.6,1.5-5.4,1.5-8.1v-0.9
            		c0-0.2,0.1-0.3,0.2-0.4c0.9-0.8,1.8-1.6,2.6-2.5c0,0,0,0,0,0c0,0-0.1,0-0.1,0c-0.8,0.3-1.7,0.5-2.5,0.5c-0.2,0-0.4-0.1-0.5-0.3
            		s0-0.5,0.2-0.6c1.2-0.8,2.2-1.8,2.9-3c-0.4,0.2-0.8,0.4-1.3,0.6c-0.9,0.4-1.9,0.7-2.9,0.8c-0.2,0-0.3,0-0.4-0.1
            		c-0.7-0.7-1.6-1.3-2.5-1.8c-1-0.4-2.1-0.6-3.2-0.7c-1.1,0-2.1,0.2-3,0.6c-0.9,0.4-1.7,0.9-2.4,1.6c-0.7,0.7-1.2,1.6-1.6,2.5
            		c-0.4,1-0.6,2-0.6,3c0,0.6,0,1.1,0.2,1.6c0,0.2,0,0.3-0.1,0.4c-0.1,0.1-0.2,0.2-0.4,0.2c-3.4-0.1-6.7-1-9.7-2.6
            		c-2.7-1.5-5.1-3.4-7.2-5.7c-0.5,1-0.7,2.1-0.7,3.3c0,1.3,0.3,2.6,0.9,3.8c0.6,1,1.4,1.9,2.3,2.6c0.2,0.1,0.3,0.3,0.2,0.6
            		s-0.3,0.4-0.5,0.4c-0.7,0-1.5-0.1-2.2-0.3c-0.4-0.1-0.7-0.2-1.1-0.4c0.2,1.6,0.8,3,1.7,4.2c1.2,1.3,2.8,2.3,4.5,2.7
            		c0.2,0,0.4,0.2,0.4,0.5s-0.1,0.4-0.4,0.5c-0.2,0-0.3,0.1-0.5,0.1c-0.2,0-0.3,0-0.5,0.1C11.7,29,11.3,29,10.9,29c-0.2,0-0.4,0-0.7,0
            		c0.5,1.2,1.3,2.2,2.4,3c1.3,1,2.9,1.5,4.6,1.5c0.2,0,0.4,0.1,0.5,0.3s0,0.4-0.2,0.5C14.5,36.8,10.8,38.1,7,38L7,38z"/>
            </g>
          </svg>';
          break;

    }

    return $html;

  }

  // ---------------------------------------- SVG Chevrons
  public function render_svg_icon_chevrons() {

    return '<svg width="25" height="27" viewBox="0 0 25 27" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M13.8048 13.5C13.8048 14.2991 13.5916 14.292 12.8338 14.7735C12.7966 14.7985 7.79955 17.8272 3.69908 25.9286C3.56097 26.2019 3.36519 26.4381 3.12684 26.619C2.8885 26.7999 2.61394 26.9206 2.32434 26.972C2.03474 27.0233 1.73781 27.0039 1.45646 26.9152C1.17511 26.8264 0.916848 26.6708 0.701557 26.4602C0.336415 26.0842 0.098551 25.5922 0.0245833 25.0598C-0.0493843 24.5274 0.0446365 23.9841 0.292181 23.5135C2.96154 18.1875 8.06005 15.487 10.4249 13.7033C10.4557 13.6799 10.4807 13.649 10.498 13.6134C10.5152 13.5777 10.5242 13.5383 10.5242 13.4982C10.5242 13.4582 10.5152 13.4187 10.498 13.3831C10.4807 13.3474 10.4557 13.3166 10.4249 13.2931C8.05667 11.5094 2.98185 8.81251 0.305727 3.48647C0.0581826 3.01591 -0.0358589 2.47262 0.0381088 1.9402C0.112076 1.40779 0.34994 0.915758 0.715083 0.539841C0.930373 0.329243 1.18866 0.173577 1.47001 0.0848381C1.75136 -0.00390107 2.04827 -0.0233438 2.33787 0.0280096C2.62747 0.079363 2.90202 0.20014 3.14037 0.381034C3.37872 0.561929 3.57449 0.798108 3.71261 1.07137C7.79954 9.17281 12.7999 12.1979 12.8473 12.23C13.551 12.733 13.8048 12.7116 13.8048 13.5Z"/>
      <path d="M25.0001 13.5C25.0001 14.2991 24.7836 14.2919 24.0257 14.7735C23.9919 14.7985 18.9915 17.8272 14.891 25.9286C14.7529 26.2019 14.5571 26.438 14.3188 26.6189C14.0804 26.7998 13.8058 26.9206 13.5162 26.972C13.2266 27.0233 12.9297 27.0039 12.6484 26.9151C12.367 26.8264 12.1087 26.6707 11.8935 26.4601C11.5283 26.0842 11.2904 25.5922 11.2165 25.0598C11.1425 24.5274 11.2365 23.9841 11.4841 23.5135C14.1433 18.1875 19.2418 15.4977 21.6067 13.7069C21.6374 13.6834 21.6625 13.6526 21.6797 13.6169C21.697 13.5813 21.706 13.5418 21.706 13.5018C21.706 13.4617 21.697 13.4223 21.6797 13.3866C21.6625 13.351 21.6374 13.3201 21.6067 13.2966C19.2384 11.513 14.1433 8.81605 11.4841 3.49001C11.2365 3.01945 11.1425 2.47616 11.2165 1.94374C11.2904 1.41133 11.5283 0.919299 11.8935 0.543382C12.1087 0.332784 12.367 0.177115 12.6484 0.0883762C12.9297 -0.000362938 13.2266 -0.0198029 13.5162 0.0315505C13.8058 0.0829039 14.0804 0.203681 14.3188 0.384575C14.5571 0.565469 14.7529 0.801649 14.891 1.07492C18.9779 9.17635 23.9784 12.2015 24.0257 12.2336C24.743 12.733 25.0001 12.7116 25.0001 13.5Z"/>
    </svg>';

  }

  // ---------------------------------------- SVG Close
  public function render_svg_icon_close() {
    return '<svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="0.661133" y="16.4246" width="23" height="2" transform="rotate(-45 0.661133 16.4246)" />
      <rect x="2.0752" y="0.161133" width="23" height="2" transform="rotate(45 2.0752 0.161133)" />
    </svg>';
  }

  // ---------------------------------------- SVG Heart
  public function render_svg_icon_heart() {

  }

  // ---------------------------------------- SVG Likes
  public function render_svg_icon_likes() {
    return '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 116 100">
      <g>
        <path d="M58,100a2.64,2.64,0,0,1-1.6-.61L55,98.3c-9.15-6.83-18.61-13.9-27.2-21.75C18.21,67.82,11.47,59.78,6.54,51.23A46.51,46.51,0,0,1,.32,33.77l0-.16A21.68,21.68,0,0,1,0,29.48C.05,15.55,9,4.39,22.94,1A32.46,32.46,0,0,1,44.3,2.76,31.38,31.38,0,0,1,58,14.61C63.61,6.1,71.35,1.28,81,.27,92.14-.9,101.45,2.4,108.68,10.05A26.64,26.64,0,0,1,115.81,26c.81,7.73-1,15.4-5.65,24.15C105.8,58.3,99.78,65.26,95,70.42,88,78,79.87,84.26,72,90.31,68.2,93.2,64.29,96,60.51,98.76l-.91.65A2.7,2.7,0,0,1,58,100ZM31.37,2.08a33.36,33.36,0,0,0-7.94,1C10.48,6.18,2.12,16.57,2.11,29.52v.08a19.84,19.84,0,0,0,.26,3.74l0,.16a44.53,44.53,0,0,0,6,16.69C13.17,58.55,19.78,66.44,29.17,75c8.51,7.78,17.93,14.82,27,21.62l1.46,1.09c.35.26.35.26.71,0l.91-.65c3.77-2.73,7.66-5.54,11.41-8.41,7.83-6,15.93-12.21,22.79-19.65,5-5.38,10.61-11.95,14.83-19.87,4.46-8.36,6.19-15.65,5.42-23a24.6,24.6,0,0,0-6.57-14.72C100.4,4.31,91.68,1.24,81.23,2.33,72.11,3.28,64.82,7.9,59.57,16c-.46.72-.86,1.32-1.56,1.28s-1.1-.57-1.54-1.28a29.46,29.46,0,0,0-25.1-14Z"/>
      </g>
    </svg>';
  }

  // --------------------------- Theme Vitals
  public function render_theme_vitals() {

    $html = '';
    $html .= "<!-- PHP Version: " . $this->get_theme_info('php_version') . " -->";
  	$html .= "<!-- WP Version: " . $this->get_theme_info('wp_version') . " -->";
  	$html .= "<!-- Current Template: " . $this->get_theme_info('template') . " -->";
  	$html .= "<!-- Post ID: " . $this->get_theme_info('post_ID') . " -->";
    $html .= "<!-- Object ID: " . $this->get_theme_info('object_ID') . " -->";

    return $html;

  }

  // --------------------------- Placeholder Content
  public function render_placeholder_content( $type = 'grid', $container = 'container' ) {

    $html = '<div class="placeholder">';
      $html .= $this->render_container( 'open', 'col-12', $container );

        switch ( $type ) {
          case 'content':

            $html .= '<div class="placeholder__content rte">';

              $html .= '<h1>H1 - Placeholder Content</h1>';
              $html .= '<h2>H2 - Mauris turpis enim venenatis quis mi egestas mattis purus</h2>';
              $html .= '<p>Pellentesque at interdum enim. Suspendisse vulputate convallis mi quis auctor. Cras at urna mi. Quisque pretium tempus lacus in viverra. Sed auctor erat enim, sed accumsan orci tristique sit amet. Mauris turpis enim, venenatis quis mi in, egestas mattis purus. Duis eleifend varius tempus. Aliquam rutrum commodo ex, vitae imperdiet tortor sodales sagittis. Mauris tellus neque, imperdiet a lectus sed, placerat mollis turpis.</p>';
              $html .= '<p>Sed tincidunt nibh vel sapien consequat placerat. In molestie, lacus sit amet imperdiet convallis, enim ex vestibulum nibh, in accumsan est elit non ligula. Etiam tellus dolor, pharetra ac tempor vel, facilisis nec elit. Duis consectetur ligula eu metus cursus bibendum. Praesent tellus est, vehicula varius volutpat at, hendrerit sed velit. Morbi in tempus nibh. Ut ultrices viverra elit, id lacinia eros tincidunt a. In eget purus massa. Nulla facilisi. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris laoreet sapien vel odio accumsan, sed posuere libero vestibulum. Praesent est felis, tincidunt eu tellus id, bibendum placerat enim. Praesent pulvinar tortor tortor, at tincidunt erat molestie vel. Pellentesque accumsan sem massa, ac tincidunt velit rutrum non. Etiam vel turpis id dolor bibendum gravida ac pharetra ex. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>';
              $html .= '<p>Vestibulum id nunc tempor, faucibus leo eu, tincidunt dui. In cursus, metus vel commodo tincidunt, odio felis facilisis arcu, in egestas erat enim id quam. In laoreet metus id luctus pellentesque. Nullam ac nunc non arcu porta maximus ac non odio. Suspendisse luctus mauris sit amet dignissim lacinia. Duis volutpat facilisis nisl quis vulputate. Sed risus purus, mollis in pulvinar in, rhoncus tristique nisi. Nunc sollicitudin sapien nibh, laoreet congue velit porttitor at. Vestibulum elementum maximus condimentum. Nam aliquam, velit ut consectetur scelerisque, sapien magna bibendum sem, ut vulputate libero massa ut justo.</p>';

              $html .= '<h3>H3 - Lorem ipsum dolor sit amet consectetur adipiscing elit</h3>';
              $html .= '<p>Donec mattis eget lorem id fermentum. Duis rhoncus nulla porta, commodo turpis eu, bibendum erat. Donec non scelerisque arcu, id imperdiet odio. Nulla sit amet mi non elit ornare laoreet et nec ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat urna sed suscipit rutrum. Phasellus feugiat turpis nibh, a accumsan elit suscipit vitae. Nulla turpis risus, fermentum id mattis eget, ullamcorper ac neque. Vestibulum congue tortor eu pellentesque venenatis. Ut sagittis ante in vestibulum pharetra. Nam cursus auctor nibh. Praesent eu libero urna.</p>';
              $html .= '<p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>';

              $html .= '<ol>';
                $html .= '<li>Pellentesque fringilla massa non metus cursus, vitae pretium dolor feugiat.</li>';
                $html .= '<li>Nunc bibendum sapien ac cursus sollicitudin.</li>';
                $html .= '<li>Pellentesque ut elit ac arcu luctus tincidunt.</li>';
                $html .= '<li>Morbi a arcu a lacus iaculis efficitur.</li>';
                $html .= '<li>Suspendisse efficitur nibh in lectus porttitor, vel faucibus lacus sagittis.</li>';
              $html .= '</ol>';

              $html .= '<p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>';
              $html .= '<p>Quisque sodales tristique tincidunt. Phasellus suscipit velit vel massa feugiat placerat. Cras quis dolor iaculis nunc commodo rhoncus ac eu ex. Morbi pharetra egestas nunc, at fermentum lorem condimentum quis. Praesent sed erat id diam sollicitudin tincidunt eget vitae odio. Praesent quam odio, ultricies consectetur est eu, varius rhoncus felis. Nam vitae lectus cursus, porttitor orci in, fringilla ipsum. Vestibulum facilisis lacus turpis.</p>';

              $html .= '<h4>H4 - Lorem ipsum dolor sit amet consectetur adipiscing elit</h4>';

              $html .= '<p>Ted aliquam augue a auctor vulputate. Nunc ut congue tellus, non mattis purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi quam ex, euismod at elit nec, placerat aliquam mi. Mauris nec blandit lorem, lacinia tristique enim. Donec suscipit lobortis arcu, non hendrerit urna rutrum in. Sed vitae metus id ex accumsan tempor. Phasellus sed commodo ex. Maecenas quis dui tortor. Vivamus quis commodo orci. Vivamus dignissim, diam malesuada imperdiet vehicula, ligula ligula ultricies libero, ut tempor turpis lacus non ipsum. Quisque non magna quis mauris ullamcorper vestibulum. Aliquam ipsum urna, faucibus congue erat vitae, maximus tincidunt nunc. Maecenas iaculis dui at velit egestas, vel commodo magna scelerisque. Nulla quis risus eu velit ornare aliquam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>';
              $html .= '<p>Nam ornare laoreet vulputate. Aliquam cursus risus libero, ac tincidunt ante interdum ac. Proin scelerisque dolor non justo vestibulum, nec blandit ante fermentum. Aliquam at odio lobortis, vestibulum felis vitae, vehicula nulla. Sed dapibus sit amet nisl quis tempus. Nulla rutrum non velit id fermentum. Praesent orci nulla, finibus ac sapien nec, tristique imperdiet enim. Nulla elementum lacus vel iaculis condimentum. Vivamus eu semper ligula. Quisque ut ultricies nibh. Maecenas risus justo, gravida eu urna vestibulum, sollicitudin dictum sapien.</p>';

              $html .= '<ul>';
                $html .= '<li>Pellentesque in ligula quis nisl egestas pharetra.</li>';
                $html .= '<li>Cras elementum arcu vel leo tempus convallis.</li>';
                $html .= '<li>Pellentesque non orci id ipsum condimentum laoreet.</li>';
                $html .= '<li>Nulla maximus enim nec neque volutpat, ultrices viverra tellus facilisis.</li>';
                $html .= '<li>Morbi pharetra nunc sed tristique posuere.</li>';
                $html .= '<li>Quisque efficitur odio vitae ipsum fringilla, id aliquam ipsum pretium.</li>';
              $html .= '</ul>';

              $html .= '<p>Fusce fringilla eget nisl vitae eleifend. Proin aliquam odio ut felis ornare feugiat. Integer ac enim et nisi laoreet commodo sed sit amet metus. Aliquam porta semper dolor ac cursus. Nunc bibendum ipsum non lobortis vehicula. Phasellus iaculis sagittis ipsum id porta. Nulla eu ante ut sapien fringilla egestas iaculis at ex. Sed in varius nibh. Etiam eros neque, tincidunt ut diam et, congue imperdiet metus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec tincidunt enim non urna laoreet eleifend. Suspendisse purus risus, suscipit vel urna viverra, venenatis lobortis ante.</p>';
              $html .= '<p>Quisque eget suscipit dui. Etiam lacinia pulvinar felis sed fringilla. Ut vitae diam et lorem eleifend porttitor a quis felis. Nulla malesuada volutpat felis, at consectetur elit consequat non. Fusce sed erat sagittis, venenatis urna a, ultricies tellus. Nunc tempor semper ligula, eget consequat elit blandit non. Donec consectetur, est vitae imperdiet vestibulum, ligula urna ultrices est, in auctor neque lectus nec sapien. Pellentesque ac rutrum purus, eget iaculis quam. Vestibulum non lacinia erat.</p>';
              $html .= '<p>Mauris fermentum dui sed leo commodo efficitur. Sed quis fringilla mi. Etiam lectus odio, ultricies vestibulum leo sodales, placerat auctor arcu. Donec lorem orci, scelerisque at ante sed, vehicula condimentum purus. In ultrices facilisis nibh, sed iaculis justo vestibulum vestibulum. Duis ut felis id nunc gravida consectetur sit amet ut metus. Integer at neque imperdiet, maximus arcu nec, tincidunt turpis. Vestibulum sit amet felis quis nibh aliquam vehicula eget efficitur eros. Pellentesque semper vulputate nisl, iaculis rutrum magna molestie vitae. Nunc ac dolor ut leo suscipit gravida id non dui. Fusce vestibulum tellus elit, euismod semper libero rutrum eu. Sed tempus, lorem et dapibus interdum, arcu urna rutrum odio, sed dictum dui leo nec nunc. Vestibulum congue tortor tellus. Vivamus euismod risus a tellus laoreet, eu consequat enim ullamcorper. Etiam a iaculis odio, in faucibus purus. Pellentesque venenatis, mauris eu iaculis tempus, dui est vulputate felis, nec interdum magna velit at enim.</p>';
              $html .= '<p>Donec mattis eget lorem id fermentum. Duis rhoncus nulla porta, commodo turpis eu, bibendum erat. Donec non scelerisque arcu, id imperdiet odio. Nulla sit amet mi non elit ornare laoreet et nec ante. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed volutpat urna sed suscipit rutrum. Phasellus feugiat turpis nibh, a accumsan elit suscipit vitae. Nulla turpis risus, fermentum id mattis eget, ullamcorper ac neque. Vestibulum congue tortor eu pellentesque venenatis. Ut sagittis ante in vestibulum pharetra. Nam cursus auctor nibh. Praesent eu libero urna.</p>';
            $html .= '</div>';

            break;
          case 'grid':

            $html .= '<div class="placeholder__grid rte">';

              $html .= '<h1>H1 - Placeholder Grid</h1>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 12; $i++ ) {
                  $html .= '<div class="col-1">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 6; $i++ ) {
                  $html .= '<div class="col-2">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 4; $i++ ) {
                  $html .= '<div class="col-3">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 3; $i++ ) {
                  $html .= '<div class="col-4">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

              $html .= '<div class="row row--inner">';
                for ( $i = 1; $i <= 2; $i++ ) {
                  $html .= '<div class="col-6">';
                    $html .= '<span>' . $i . '</span>';
                  $html .= '</div>';
                }
              $html .= '</div>';

            $html .= '</div>';

            break;

        } // switch $type

      $html .= $this->render_container( 'close' );
    $html .= '</div>';

    return $html;

  }

}

?>
