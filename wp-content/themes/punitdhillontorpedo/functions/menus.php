<?php

//////////////////////////////////////////////////////////
////  Register Custom Menus
//////////////////////////////////////////////////////////

function VeryPolite_custom_menus () {

	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
		)
	);

}

add_action( 'init', 'VeryPolite_custom_menus' );

//////////////////////////////////////////////////////////
////  Print Menu
//////////////////////////////////////////////////////////

function print_menu( $menu_title = 'Main Menu' ) {

  // reset post data
  wp_reset_postdata();

  // post data
  global $post;

  // default data
  $current_id = $current_type = $menu_items = false;

  if ( wp_get_nav_menu_items ( $menu_title ) ) {

    $menu_items = wp_get_nav_menu_items ( $menu_title );

    if ( is_category() ) {
      $category = get_queried_object();
      $current_id = $category->term_id;
      $current_type = "category";
    } else {
      $current_id = $post->ID;
      $current_type = $post->post_type;
    }

    // loop through menu object here, only output '<li>' elements
    foreach ( $menu_items as $element ) {

      // default data
      $id = $link = $title = $type = false;
      $link_classes = '';

      $id = $element->object_id;
      $link = $element->url;
      $title = $element->title;
      $type = $element->object;
      $link_type = $element->type;
      $target = '_self';

      if ( $id == $current_id && $type == $current_type ) {
        $link_classes = 'active';
      }
      if ( 'custom' == $link_type ) {
        $target = '_blank';
      }

      echo '<li
        class="menu__item' . ( $link_classes ? ' ' . $link_classes : '' ) . '"
        data-this-type="' . $current_type . '"
        data-link-type="' . $type . '"
        data-this-id="' . $current_id . '"
        data-link-id="' . $id . '"
      >';
        echo '<a href="' . $link . '" target="' . $target . '">' . $title . '</a>';
      echo '</li>';

      }

  }

}

//////////////////////////////////////////////////////////
////  Print Menu LEGACY
//////////////////////////////////////////////////////////

function print_menu__LEGACY( $menu_title = 'Main Menu', $class_modifiers = array() ) {

	$menu_object = wp_get_nav_menu_items ( $menu_title ) ?? false;
	$current_page_id = get_the_ID();
	$menu_items = array();
	$menu_classes = '';

	if ( $menu_object && !empty( $menu_object ) ) {

		//////////////////////////////////////////////////////////
		////  create formatted array of menu item data
		//////////////////////////////////////////////////////////

		foreach ( $menu_object as $item ) {

			// defaults
			$id = $title = $page_id = $parent_id = $url = $type = $active = false;

			if ( isset( $item->ID ) && !empty( $item->ID ) ) {
				$id = $item->ID;
			}

			if ( isset( $item->url ) && !empty( $item->url ) ) {
				$url = $item->url;
			}

			if ( isset( $item->title ) && !empty( $item->title ) ) {
				$title = $item->title;
			}

			if ( isset( $item->type ) && !empty( $item->type ) ) {
				$type = $item->type;
			}

			if ( isset( $item->object_id ) && !empty( $item->object_id ) ) {
				$page_id = $item->object_id;
				if ( $page_id == $current_page_id ) {
					$active = true;
				}
			}

			if ( isset( $item->menu_item_parent ) && !empty( $item->menu_item_parent ) ) {
				$parent_id = $item->menu_item_parent;
			}

			$temp = array(
  			'id' => $id,
				'url' => $url,
				'title' => $title,
				'type' => $type,
				'page_id' => $page_id,
				'parent_id' => $parent_id,
				'active' => $active,
				'child_links' => array(),
			);

			if ( !$parent_id ) {
  			// not a child item
  			$menu_items[$id] = $temp;
			} else {
  			// child item
				foreach ( $menu_items as $this_id => $this_item ) {
  				if ( $this_id == $parent_id ) {
    				$menu_items[$this_id]['child_links'][$id] = $temp;
  				} else {
    				foreach ( $menu_items[$this_id]['child_links'] as $this_sub_id => $this_sub_item ) {
      				if ( $this_sub_id == $parent_id ) {
        				$menu_items[$this_id]['child_links'][$this_sub_id]['child_links'][$id] = $temp;
      				}
    				}
  				}
				}
			}

		}

		//////////////////////////////////////////////////////////
		////  loop through data and print to screen
		//////////////////////////////////////////////////////////

		if ( $class_modifiers && !empty( $class_modifiers ) ) {
  		foreach ( $class_modifiers as $i => $class ) {
    		if ( 0 == $i ) {
      		$menu_classes = "menu menu--" . $class;
    		} else {
      		$menu_classes .= " menu--" . $class;
    		}
  		}
		}

		echo '<nav class="' . $menu_classes . '">';
		  loopThroughMenus( $menu_items );
		echo '</nav>';

	} // if $menu_object

}

//////////////////////////////////////////////////////////
////  Loop Through Menu Object
//////////////////////////////////////////////////////////

function loopThroughMenus( $array = false ) {
  if ( $array && !empty( $array ) ) {
    echo '<ul class="menu__list">';
    foreach ( $array as $item ) {
      echo '<li class="menu__item' . ( $item['active'] ? ' active' : '' ) . '">';
        echo '<a href="' . $item['url'] . '" ' . ( $item['type'] == "custom" ? 'target="_blank" rel="nofollow noopener"' : '' ) . '>' . $item['title'] . '</a>';
        loopThroughMenus( $item['child_links'] );
      echo '</li>';
    }
    echo '</ul>';
  }
}

?>
