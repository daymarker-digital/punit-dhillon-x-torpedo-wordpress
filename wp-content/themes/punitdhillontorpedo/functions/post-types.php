<?php

/*
//////////////////////////////////////////////////////////
////  Custom Post Types | Actions
//////////////////////////////////////////////////////////
*/

add_action( 'init', 'VP__register_post_types' );
add_action( 'init', 'VP__resolve_post_slug_conflict' );
add_action( 'pre_get_posts', 'university_adjust_queries' );

/*
//////////////////////////////////////////////////////////
////  Custom Post Types | Functions
//////////////////////////////////////////////////////////
*/

// ---------------------------------------- Create Custom Post Type
function VP__create_post_type( $name = '', $singular_name = '', $slug = '', $icon = '', $has_archive = true ) {
  return [
    'name' => $name,
    'singular_name' => $singular_name ,
    'slug' => $slug,
    'menu_icon' => $icon,
    'has_archive' => $has_archive,
  ];
  // 'has_archive' => 'custom-slug',
}

// ---------------------------------------- Register Custom Post Type
function VP__register_post_types() {

  $VP = new PDTheme();

  $post_types = [];
  $issues = VP__create_post_type( 'Issues', 'Issue', 'issue', 'dashicons-text-page', true );
  $press = VP__create_post_type( 'Press', 'Press', 'press', 'dashicons-awards', 'press' );

  if ( $VP->is_torpedo ) {
    array_push( $post_types, $issues );
  } else {
    array_push( $post_types, $press );
  }

  foreach ( $post_types as $index => $value ) {

    $icon = $value['menu_icon'];
    $name = $value['name'];
    $singular = $value['singular_name'];
    $slug = $value['slug'];
    $has_archive = $value['has_archive'];

    $labels = array(
      'name'                => _x( $name, 'Post Type General Name', 'text_domain' ),
      'singular_name'       => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
      'menu_name'           => __( $name, 'text_domain' ),
      'name_admin_bar'      => __( $singular, 'text_domain' ),
      'parent_item_colon'   => __( 'Parent ' . $singular . ':', 'text_domain' ),
      'all_items'           => __( 'All ' . $name, 'text_domain' ),
      'add_new_item'        => __( 'Add New ' . $singular, 'text_domain' ),
      'add_new'             => __( 'Add New', 'text_domain' ),
      'new_item'            => __( 'New ' . $singular, 'text_domain' ),
      'edit_item'           => __( 'Edit ' . $singular, 'text_domain' ),
      'update_item'         => __( 'Update ' . $singular, 'text_domain' ),
      'view_item'           => __( 'View ' . $name, 'text_domain' ),
      'search_items'        => __( 'Search ' . $name, 'text_domain' ),
      'not_found'           => __( $name . ' Not found', 'text_domain' ),
      'not_found_in_trash'  => __( $name . ' Not found in Trash', 'text_domain' ),
    );

    $args = array(
      'label'               => __( $singular, 'text_domain' ),
      'description'         => __( 'Posts for ' . $singular, 'text_domain' ),
      'labels'              => $labels,
      'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
      'taxonomies'          => array( 'category', 'post_tag' ),
      'hierarchical'        => true,
      'public'              => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'menu_position'       => 5,
      'show_in_admin_bar'   => true,
      'show_in_nav_menus'   => true,
      'can_export'          => true,
      'has_archive'         => $has_archive,
      'show_in_rest' 		    => true,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'capability_type'     => 'page',
      'delete_with_user'    => false,
      'menu_icon'           => $icon,
      'rewrite'             => array( 'slug' => $slug )
    );

    register_post_type( $slug, $args );

  }

}

// ---------------------------------------- Resolve Post Slug Conflict
function VP__resolve_post_slug_conflict() {
  add_rewrite_rule('^press/page/([0-9]+)','index.php?pagename=press&paged=$matches[1]', 'top');
}

// ---------------------------------------- Comment
function university_adjust_queries( $query ) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ( is_post_type_archive( 'press' ) ) {
      // $query->set( 'post_status', [ 'publish' ] );
      // $query->set( 'order', 'DESC' );
      // $query->set( 'orderby', 'date' );
      // $query->set( 'nopaging', false );
    }
  }
}

?>
