<?php

/*
//////////////////////////////////////////////////////////
////  Custom Image Size Function
//////////////////////////////////////////////////////////
*/

function VP_image_sizes() {

	// required
	add_theme_support( 'post-thumbnails' );

	// initialize instance of PoliteDepartment
	$VP = new PDTheme();

	// get custom sizes
	$custom_sizes = $VP->custom_image_sizes;

	foreach ( $custom_sizes as $index => $size ) {
  	$size_title = $VP->custom_image_title;
  	$size_title .= '-' . $size;
  	add_image_size( $size_title, $size, 9999 );
	}

} // VP_image_sizes()

add_action( 'after_setup_theme', 'VP_image_sizes' );

?>
