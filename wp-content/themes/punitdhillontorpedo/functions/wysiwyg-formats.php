<?php
  
/*
*
*	Custom WYSIWYG Styles
*
*/

//////////////////////////////////////////////////////////
////  Add Style Select Buttons
//////////////////////////////////////////////////////////

function add_style_select_buttons ( $buttons ) {
  
  array_unshift( $buttons, 'styleselect' );

  return $buttons;
    
}

// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'add_style_select_buttons' );

//////////////////////////////////////////////////////////
////  Add Custom Styles
//////////////////////////////////////////////////////////

function my_custom_styles ( $init_array ) {  
 
    $style_formats = array(  
      array(  
        'title' => 'Citation',  
        'block' => 'span',  
        'classes' => 'citation',
        'wrapper' => true,
      ),
      array(  
        'title' => 'Intro Paragraph',  
        'block' => 'span',  
        'classes' => 'intro-para',
        'wrapper' => true,
      ),
      array(  
        'title' => 'Two Column Paragraph',  
        'block' => 'p',  
        'classes' => 'two-column-para',
        'wrapper' => true,
      ),
      array(  
        'title' => 'Uppercase',  
        'inline' => 'span',  
        'classes' => 'uppercase',
        'styles' => array(
          'textTransform' => 'uppercase',
        ),
      ),
    );  
    
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  
    
    return $init_array;  
  
} 

// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_custom_styles' );

?>
