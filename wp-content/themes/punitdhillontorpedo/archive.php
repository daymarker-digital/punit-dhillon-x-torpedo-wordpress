<?php

/*
*
*	Template Name: Archive
*	Filename: archive.php
*
*/

get_header();

// ---------------------------------------- Polite Department
$VP = new PDTheme();
$post_id = get_the_ID();

if ( have_posts() ) {
  while ( have_posts() ) {
    the_post();
    echo get_the_title();
  }
}

get_footer();

?>
