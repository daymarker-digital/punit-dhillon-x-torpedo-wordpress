<?php

/*
*
*	Filename: single.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

// ---------------------------------------- Vars
$block_name = 'article';

// ---------------------------------------- Theme Vars
$VP = new PDTheme();
$home = $VP->get_theme_directory('home');

if ( have_posts() ) {
	while ( have_posts() ) {

		// init post data
		the_post();

    // ---------------------------------------- Post Vars
    $post_id = get_the_ID();
    $featured_image = $VP->get_featured_image_by_post_id( $post_id );

    // ---------------------------------------- ACF Vars
    $issue = get_field( 'issue' ) ? get_field( 'issue' ) : false;

  	//////////////////////////////////////////////////////////
    ////  Mobile
    //////////////////////////////////////////////////////////

    echo '<article class="article d-lg-none" data-style="mobile">';

      //////////////////////////////////////////////////////////
      ////  Hero
      //////////////////////////////////////////////////////////

      echo $VP->render_article_hero([ 'post_id' => $post_id, 'mobile' => true ]);

      //////////////////////////////////////////////////////////
      ////  Main
      //////////////////////////////////////////////////////////

      echo '<div class="article__main">';
        include( locate_template( './snippets/article--content.php' ) );
      echo '</div>';

      //////////////////////////////////////////////////////////
      ////  Actions
      //////////////////////////////////////////////////////////

      echo $VP->render_article_actions([ 'post_id' => $post_id ]);

      //////////////////////////////////////////////////////////
      ////  Footer
      //////////////////////////////////////////////////////////

      echo '<div class="article__footer">';
        echo $VP->render_article_releated_articles([ 'post_id' => $post_id ]);
      echo '</div>';

    echo '</article>';

    //////////////////////////////////////////////////////////
    ////  Desktop
    //////////////////////////////////////////////////////////

    echo '<article class="article d-none d-lg-block sticky-column" data-style="desktop">';
      echo '<div class="sticky-column__container">';

        //////////////////////////////////////////////////////////
        ////  Left Column
        //////////////////////////////////////////////////////////

        echo '<div class="sticky-column__column sticky-column__column--stuck">';
          echo '<div class="sticky-column__main">';
            echo '<div class="sticky-column__content">';

              //////////////////////////////////////////////////////////
              ////  Top
              //////////////////////////////////////////////////////////

              echo '<div class="sticky-column__content-top">';
                if ( $featured_image ) {
                  echo '<div class="sticky-column__featured-image article__feature-image">';
                    echo $VP->render_lazyload_image( $featured_image, [ 'background' => true ] );
                  echo '</div>';
                }
              echo '</div>';

              //////////////////////////////////////////////////////////
              ////  Bottom
              //////////////////////////////////////////////////////////

              echo '<div class="sticky-column__content-bottom">';
                echo $VP->render_article_releated_articles([ 'post_id' => $post_id ]);
              echo '</div>';

            echo '</div>';
          echo '</div>';
        echo '</div>';

        /////////////////////////////////////////////////////////
        ////  Right Column
        //////////////////////////////////////////////////////////

        echo '<div class="sticky-column__column sticky-column__column--scroll">';
          echo '<div class="sticky-column__main">';

            echo $VP->render_article_hero([ 'post_id' => $post_id, 'mobile' => false ]);
            include( locate_template( './snippets/article--content.php' ) );
            echo $VP->render_article_actions([ 'post_id' => $post_id ]);

          echo '</div>';
        echo '</div>';

      echo '</div>';
    echo '</article>';

  }
}

get_footer();

?>
