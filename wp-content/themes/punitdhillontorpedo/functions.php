<?php

/*
*
*	Filename: functions.php
*
*/

add_post_type_support( 'page', 'excerpt' );

$includes = [
  'advanced-custom-fields',
  'image-sizes',
  'likes',
  'menus',
  'optimization',
  'polite-department',
  'polite-department-theme',
  'post-types',
  'security',
  'utilities',
  'wysiwyg-formats',
  'enqueue-scripts-styles',
];

foreach( $includes as $include ) {
  include_once( 'functions/' . $include . '.php' );
}
