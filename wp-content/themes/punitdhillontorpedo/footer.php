<?php

/*
*
*	Filename: footer.php
*
*/

// ---------------------------------------- Polite Department
$VP = new PDTheme();
$post_id = get_the_ID();
$newsletter_heading = ( $VP->is_torpedo ) ? 'Subscribe to emails <span>from Torpedo</span>' : 'Subscribe to emails <span>from Punit</span>';
$google_analytics = ( $VP->is_torpedo ) ? $VP->render_google_analytics( "G-KSQNHT61VM" ) : $VP->render_google_analytics( "UA-180234576-1" );

echo '</main>';

// ---------------------------------------- Footer
echo $VP->render_footer([ 'current_id' => $post_id ]);

// ---------------------------------------- Modal | Newsletter
$modal_args = [
  'form' => 'newsletter',
  'heading' => $newsletter_heading,
  'id' => 'newsletter',
  'post_id' => $post_id,
  'theme' => 'red',
];
echo $VP->render_modal( $modal_args );

// ---------------------------------------- Modal | Share
if ( is_single() || true ) {
  $modal_args = [
    'heading' => 'Share to',
    'id' => 'share',
    'post_id' => $post_id,
    'theme' => 'purple',
  ];
  echo $VP->render_modal( $modal_args );
}

// ---------------------------------------- Modal | Notify Me
if ( is_singular( [ 'wps_products' ] ) || true ) {
  $modal_args = [
    'form' => 'notify-me',
    'heading' => 'We’ll let you know when this<span>product is available</span>',
    'id' => 'notify-me',
    'post_id' => $post_id,
    'theme' => 'red',
  ];
  echo $VP->render_modal( $modal_args );
}

wp_footer();

echo $google_analytics;

echo '</body>';
echo '</html>';

?>
