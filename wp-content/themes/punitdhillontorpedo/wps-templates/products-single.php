<?php

get_header('wpshopify');

defined('ABSPATH') ? : die();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();

//////////////////////////////////////////////////////////
////  Template Vars
//////////////////////////////////////////////////////////

$title = get_the_title();
$post_id = get_the_ID();

$slug = $post->post_name;

$Products = WP_Shopify\Factories\DB\Products_Factory::build();
$product_id = $Products->get_product_ids_from_handles([ $slug ]);
$product = $Products->get_product_from_product_id( $product_id[0] );

$nofity_me = get_field( 'enable_notify_me' );

//////////////////////////////////////////////////////////
////  Template Layout
//////////////////////////////////////////////////////////

echo '<section class="section sticky-column product wps-product" data-background-colour="black">';
  echo '<div class="sticky-column__container wp-shopify">';

    //////////////////////////////////////////////////////////
    ////  Left Column
    //////////////////////////////////////////////////////////

    echo '<div class="sticky-column__column sticky-column__column--stuck">';
      echo '<div class="sticky-column__main">';
        echo '<div class="sticky-column__content">';

          //////////////////////////////////////////////////////////
          ////  Top
          //////////////////////////////////////////////////////////

          echo '<div class="sticky-column__content-top">';

            if ( have_rows( 'featured_image' ) ) {
              while ( have_rows( 'featured_image' ) ) {

                // init data
                the_row();

                // default data
                $mobile = $desktop = false;

                // get data
                if ( get_sub_field( 'mobile' ) ) {
                  $mobile = get_sub_field( 'mobile' );
                }
                if ( get_sub_field( 'desktop' ) ) {
                  $desktop = get_sub_field( 'desktop' );
                }

                // print data
                if ( $mobile ) {
                  echo '<div class="sticky-column__featured-image product__featured-image product__featured-image--mobile">';
                    echo $VP->render_lazyload_image( $mobile, [ 'alt_text' => $title ] );
                  echo '</div>';
                }
                if ( $desktop ) {
                  echo '<div class="sticky-column__featured-image product__featured-image product__featured-image--desktop">';
                    echo $VP->render_lazyload_image( $desktop, [ 'alt_text' => $title ] );
                  echo '</div>';
                }

              }
            } else {
              if ( $VP->get_featured_image_by_post_id( $post_id ) ) {
                echo '<div class="sticky-column__featured-image product__featured-image product__featured-image--mobile">';
                  echo $VP->render_lazyload_image( $VP->get_featured_image_by_post_id( $post_id ), [ 'alt_text' => $title ] );
                echo '</div>';
                echo '<div class="sticky-column__featured-image product__featured-image product__featured-image--desktop">';
                  echo $VP->render_lazyload_image( $VP->get_featured_image_by_post_id( $post_id ), [ 'background' => true ] );
                echo '</div>';
              }
            }

            if ( $title ) {

              echo '<h1 class="product__title heading heading--title">' . $title . '</h1>';

              $price_shortcode = '[wps_products_pricing';
              $price_shortcode .= ' show_compare_at="false"';
              $price_shortcode .= ' product_id="' . $product_id[0] .'"';
              $price_shortcode .= ']';

              if ( ! $nofity_me ) {
                echo '<div class="product__price heading heading--title">';
                  echo do_shortcode( $price_shortcode );
                echo '</div>';
              }

            }

            if ( get_field( 'featured_content' ) ) {
              echo '<div class="product__desc message rte">';
                echo get_field( 'featured_content' );
              echo '</div>';
            } else {
              if ( get_the_content() ) {
                echo '<div class="product__desc message rte">';
                  echo apply_filters( 'the_content', get_the_content() );
                echo '</div>';
              }
            }

            if ( ! $nofity_me ) {
              echo '<div class="product__buy-button">';
                $button_shortcode = '[wps_products_buy_button';
                $button_shortcode .= ' add_to_cart_button_color="#000"';
                $button_shortcode .= ' add_to_cart_button_text="Add to Bag"';
                $button_shortcode .= ' hide_quantity="true"';
                $button_shortcode .= ' product_id="' . $product_id[0] . '"';
                $button_shortcode .= ' variant_button_color="#000"';
                $button_shortcode .= ' variant_style="buttons"';
                $button_shortcode .= ']';
                echo do_shortcode( $button_shortcode );
              echo '</div>';
            } else {
              if ( 'catapult' === $slug ) {
                echo '<a class="product__cta button button--pill button--secondary" href="https://geni.us/Catapult" target="_blank">Shop This</a>';
              } else {
                echo '<button class="product__cta button button--pill button--secondary js--notify-me" type="button">Notify Me</button>';
              }
            }

          echo '</div>';

          //////////////////////////////////////////////////////////
          ////  Bottom
          //////////////////////////////////////////////////////////

          echo '<div class="sticky-column__content-bottom">';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';

    //////////////////////////////////////////////////////////
    ////  Right Column
    //////////////////////////////////////////////////////////

    echo '<div class="sticky-column__column sticky-column__column--scroll">';
      echo '<div class="sticky-column__main">';

        if ( have_rows( 'flexible_content' ) ) {

          echo '<div class="article__content article__content--product">';

            // Loop through rows.
            while ( have_rows( 'flexible_content' ) ) {

              // init data
              the_row();

              // get row layout
              $type = get_sub_field('type');

              // conditionally show layout
              switch ( $type ) {
                case "images":
                  include( locate_template( './snippets/layout--images.php' ) );
                  break;
                case "videos":
                  include( locate_template( './snippets/layout--videos.php' ) );
                  break;
                case "wysiwyg":
                  include( locate_template( './snippets/layout--wysiwyg.php' ) );
                  break;
              }

            }

          echo '</div>';

        }

      echo '</div>';
    echo '</div>';

  echo '</div>';
echo '</article>';

get_footer('wpshopify');

?>
