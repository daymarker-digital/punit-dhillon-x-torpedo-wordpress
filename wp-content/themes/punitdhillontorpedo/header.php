<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->

<head>

  <?php

    $VP = new PDTheme();
    $assets_dir = $VP->get_theme_directory('assets');
    $theme_dir = $VP->get_theme_directory();
    $theme_classes = $VP->get_theme_classes();

  ?>

  <title><?php wp_title(''); ?></title>

  <link rel="apple-touch-icon" href="<?php echo $theme_dir; ?>/apple-touch-icon.png?v=<?php echo filemtime( get_template_directory() . '/apple-touch-icon.png' ); ?>">
  <link rel="shortcut icon" href="<?php echo $theme_dir; ?>/favicon.ico?v=<?php echo filemtime( get_template_directory() . '/favicon.ico' ); ?>">

  <link rel="dns-prefetch" href="//www.google-analytics.com">

  <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin>
  <link rel="preconnect" href="https://ajax.googleapis.com" crossorigin>

  <meta charset="<?php echo get_bloginfo('charset'); ?>">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="author" content="Very Polite">
  <meta http-equiv="Expires" content="7" />

  <link rel="preload" href="<?php echo $theme_dir; ?>/style.css" as="style" onload="this.rel='stylesheet'">
  <link rel="preload" href="<?php echo $assets_dir; ?>/PoliteDepartment.min.js?ver=<?php echo filemtime( get_template_directory() . '/assets/PoliteDepartment.min.js' ); ?>" as="script">

  <?php

    // Preload Fonts
    $preloadable_fonts = [
      'ACaslonPro-Bold',
      'ACaslonPro-Regular',
      'ACaslonPro-Semibold',
      'Apercu-Bold',
      'Apercu-Light',
      'Apercu-Medium',
      'Apercu-Mono',
      'Apercu',
      'MafraDisplay-Bold'
    ];
    echo $VP->render_preload_fonts( $preloadable_fonts );

    // Search Engine Optimization (SEO)
    echo $VP->render_seo();

		// Inline Main CSS
		echo '<!-- Main CSS -->';
		echo '<style>';
		  include( locate_template( './assets/main.css' ) );
		echo '</style>';

		// Header Scripts
		echo '<!-- Header Scripts -->';
		include( locate_template( './snippets/theme--scripts.php' ) );

		// Header Hook
		wp_head();

  ?>

</head>

<body
  class='<?php echo $theme_classes; ?> sticky-footer'
  data-object-id='<?php echo $VP->get_theme_info('object_ID'); ?>'
  data-post-id='<?php echo $VP->get_theme_info('post_ID'); ?>'
>

  <?php

	  // Site Vitals
		if ( is_user_logged_in() || is_admin() ) {
      echo $VP->get_theme_info('vitals');
		}

    // Mobile Menu
		echo $VP->render_mobile_menu( [ 'current_id' => get_the_ID() ]);

    // Header

    if ( $VP->is_torpedo ) {
      echo $VP->render_header_torpedo( [ 'current_id' => get_the_ID() ] );
    } else {
      echo $VP->render_header( [ 'current_id' => get_the_ID() ] );
    }

  ?>

	<main class="<?php echo $theme_classes; ?> torpedo" role="main">
