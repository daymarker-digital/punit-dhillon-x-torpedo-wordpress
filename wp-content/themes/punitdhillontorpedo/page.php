<?php

/*
*
*	Template Name: Page
*	Filename: page.php
*
*/

get_header();

// ---------------------------------------- Polite Department
$VP = new PDTheme();

// ---------------------------------------- WP Loop
if ( have_posts() ) {
  while ( have_posts() ) {

    // init post data
    the_post();

    // ---------------------------------------- Vars
    $block_name = 'default-page';

    // ---------------------------------------- ACF Vars
    $theme = get_field( 'theme' ) ? get_field( 'theme' ) : 'default';
    $newsletter_heading = 'Stay up to date with all Punit’s News (including job postings!)';

    if ( have_rows( 'newsletter' ) ) {
      while ( have_rows( 'newsletter' ) ) {
        the_row();
        if ( get_sub_field( 'heading' ) ) {
          $newsletter_heading = get_sub_field( 'heading' );
        }
      }
    }

    // ---------------------------------------- Post Vars
    $content = get_the_content();
    $post_id = get_the_ID();
    $title = get_the_title();

    // ---------------------------------------- Template
    echo '<section
      class="section section--' . $block_name . ' ' . $block_name . ' sticky-column"
      data-background-colour="' . $theme . '"
    >';

      echo '<div class="sticky-column__container">';

        //////////////////////////////////////////////////////////
        ////  Left Column
        //////////////////////////////////////////////////////////

        echo '<div class="sticky-column__column">';
          echo '<div class="sticky-column__main">';

            echo '<div class="sticky-column__vr vr"></div>';

            echo '<div class="' . $block_name . '__content">';
              echo $title ? '<h1 class="' . $block_name . '__heading heading heading--title">' . $title . '</h1>' : '';
              echo $content ? '<div class="' . $block_name . '__message message rte">' . apply_filters( 'the_content', $content ) . '</div>' : '';
            echo '</div>';

          echo '</div>';
        echo '</div>';

        //////////////////////////////////////////////////////////
        ////  Right Column
        //////////////////////////////////////////////////////////

        echo '<div class="sticky-column__column sticky-column__column--stuck">';
          echo '<div class="sticky-column__main">';
            echo '<div class="sticky-column__content">';

              //////////////////////////////////////////////////////////
              ////  Top
              //////////////////////////////////////////////////////////

              echo '<div class="sticky-column__content-top">';
                echo '<div class="' . $block_name . '__newsletter">';
                  echo $newsletter_heading ? '<h2 class="' . $block_name . '__newsletter-heading heading heading--title">' . $newsletter_heading . '</h2>' : '';
                  $form_args = [
                    'action' => 'https://formspree.io/f/moqpjnbn',
                    'button_title' => 'Sign Up',
                    'id' => 'newsletter',
                  ];
                  echo $VP->render_form( $form_args );
                echo '</div>';
              echo '</div>';

              //////////////////////////////////////////////////////////
              ////  Bottom
              //////////////////////////////////////////////////////////

              echo '<div class="sticky-column__content-bottom">';
              echo '</div>';

            echo '</div>';
          echo '</div>';
        echo '</div>';

      echo '</div>';
    echo '</section>';

  }

}

get_footer();

?>
