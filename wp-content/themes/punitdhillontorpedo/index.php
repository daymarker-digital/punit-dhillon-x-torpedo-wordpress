<?php
	
/*
*	
*	Filename: index.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();
$home = $VP->get_theme_directory('home');
$assets_dir = $VP->get_theme_directory('assets');
$theme_dir = $VP->get_theme_directory();
 
?>

<div id="index" class="page" role="main">
		
	<?php if ( have_posts() ) : ?>	
		<?php while ( have_posts() ) : the_post(); ?>
		
			<div class="section section--wp-post">
				<div class="wrapper"><div class="row"><div class="col-xs-12">
					
					<?php if ( get_the_content() ) : ?>
				
						<div class="rte">
							<?php the_content(); ?>
						</div>
						
					<?php endif; ?>
					
				</div></div></div>
				<!-- /.wrapper .row .col -->
				
			</div>
			<!-- /.section--wp-post -->
		
		<?php endwhile; ?>
	<?php else : ?>
		<!-- No Post Content -->
	<?php endif; wp_reset_postdata(); ?>		

</div>
<!-- /#index -->	
	
<?php get_footer(); ?>
