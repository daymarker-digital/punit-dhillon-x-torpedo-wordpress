<?php

/*
*
*	Template Name: 404
*	Filename: 404.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();
$home = $VP->get_theme_directory('home');

$block_name = 'error-404';
$error_404 = get_field( '404', 'options' ) ? get_field( '404', 'options' ) : [];

echo '<section class="section section--' . $block_name . ' ' . $block_name . '" data-background-colour="black">';
  echo '<div class="' . $block_name . '__main">';

    if ( $error_404 ) {

      $heading = ( isset($error_404['heading']) && !empty($error_404['heading']) ) ? $error_404['heading'] : false;
      $image = ( isset($error_404['image']) && !empty($error_404['image']) ) ? $error_404['image'] : false;
      $message = ( isset($error_404['message']) && !empty($error_404['message']) ) ? $error_404['message'] : false;

      echo $image ? '<div class="' . $block_name . '__image">' . $VP->render_lazyload_image( $image ) . '</div>' : '';

      if ( $heading || $message ) {
        echo '<div class="' . $block_name . '__content">';
          echo $heading ? '<h1 class="' . $block_name . '__heading heading">' . $heading . '</h1>' : '';
          echo $message ? '<div class="' . $block_name . '__message message rte">' . $message . '</div>' : '';
        echo '</div>';
      }

      echo '<div class="' . $block_name . '__cta">' . $VP->render_link([ 'classes' => 'button button--pill button--secondary', 'title' => 'Take me Home', 'url' => $home ]) . '</div>';

    } else {

      echo '<h1>Error 404</h1>';
      echo '<h2>Page or Content Not Found.</h2>';
      echo '<p>Let us take you <a href="' . $home . '">home</a>.</p>';

    }

  echo '</div>';
echo '</section>';

get_footer();

?>
