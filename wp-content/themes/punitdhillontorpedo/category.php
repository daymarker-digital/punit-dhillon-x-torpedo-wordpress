<?php

/*
*
*	Filename: footer.php
*
*/

get_header();

// ---------------------------------------- Polite Department
$VP = new PDTheme();
$post_id = get_the_ID();

// ---------------------------------------- Template
echo $VP->render_articles([ 'post_id' => $post_id ]);

get_footer();

?>
