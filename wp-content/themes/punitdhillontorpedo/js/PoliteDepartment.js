///////////////////////////////////////////////////////////////////////////////////////////////
////  Vendor Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend quiet "../node_modules/jquery/dist/jquery.min.js";
// @codekit-prepend quiet "../node_modules/axios/dist/axios.min.js";
// @codekit-prepend quiet "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";
// @codekit-prepend quiet "../node_modules/@glidejs/glide/dist/glide.min.js";
// @codekit-prepend quiet "../node_modules/micromodal/dist/micromodal.min.js";
// @codekit-prepend quiet "../node_modules/validator/validator.min.js";

///////////////////////////////////////////////////////////////////////////////////////////////
////  Polite Department Imports
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend "./modules/_article.js";
// @codekit-prepend "./modules/_breakpoints.js";
// @codekit-prepend "./modules/_browser.js";
// @codekit-prepend "./modules/_credits.js";
// @codekit-prepend "./modules/_forms.js";
// @codekit-prepend "./modules/_gliders.js";
// @codekit-prepend "./modules/_likes.js";
// @codekit-prepend "./modules/_mobileMenu.js";
// @codekit-prepend "./modules/_modals.js";
// @codekit-prepend "./modules/_press.js";
// @codekit-prepend "./modules/_scrolling.js";
// @codekit-prepend "./modules/_theme.js";
// @codekit-prepend "./modules/_tools.js";

//////////////////////////////////////////////////////////////////////////////////////////
////  Execute Theme
//////////////////////////////////////////////////////////////////////////////////////////

let article = new Article();
let credits = new Credits();
let forms = new Forms();
let gliders = new Gliders();
let likes = new Likes();
let mobileMenu = new MobileMenu();
let modals = new Modals();
let press = new Press();
let scrolling = new Scrolling();

Theme.init([
  credits,
  article,
  forms,
  gliders,
  likes,
  mobileMenu,
  modals,
  press,
  scrolling,
]);

// axios.get( 'https://optimilife.com/products/defense-chaga-mushroom-supplement.json' )
// .then(function (response) {
//   console.log( response.data );
// })
// .catch(function (error) {
//   console.log( error );
// });
