//////////////////////////////////////////////////////////
////  Gliders
//////////////////////////////////////////////////////////

const Gliders = (() => {

  let debug = false;
  let info = { name : 'Gliders', version : '1.1' };

  let tools = new Tools();
  let breakpoints = new Breakpoints();

  let gliders = {};
  let queryTargetElement = '.js--glide';

  //////////////////////////////////////////////////////////
  ////  Get Gliders
  //////////////////////////////////////////////////////////

  const getGliders = () => {
    return document.querySelectorAll( queryTargetElement ) || [];
  };

  //////////////////////////////////////////////////////////
  ////  Glider Options
  //////////////////////////////////////////////////////////

  const gliderOptions = ( $customOptions = false ) => {

    let options = {
      breakpoints: {
        991: {
          autoplay: 3250,
          animationDuration: 850,
        },
        9999: {
          autoplay: 3950,
          animationDuration: 1050,
        },
      },
      gap: 0,
      hoverpause: true,
      perView: 1,
      rewind: true,
      type: 'carousel',
    };

    if ( $customOptions && (typeof $customOptions == "object") ) {
      options = { ...options, ...$customOptions };
    }

    return options;

  };

  //////////////////////////////////////////////////////////
  ////  Update Glider Height
  //////////////////////////////////////////////////////////

  const updateGliderHeight = ( $glideContainerSelector = '' ) => {

    if ( debug ) console.log( `updateGliderHeight() Started!` );

    let glideContainer = document.querySelector( $glideContainerSelector );
    let glideActiveElement = glideContainer.querySelector( '.glide__slide--active' );
    let glidetrack = glideContainer.querySelector( '.glide__track' );

    if ( glideActiveElement && glidetrack ) {

      let glideActiveElementHeight = glideActiveElement.offsetHeight;
      let glidetrackHeight = glidetrack.offsetHeight;

      // glideActiveElement.querySelector( '.lazyload-item.lazyload-item--image' ).style.opacity = 1;

      if ( glidetrackHeight != glideActiveElementHeight ) {
        glidetrack.style.height = glideActiveElementHeight + 'px';
      }

    }

    if ( debug ) console.log( `updateGliderHeight() Finished!` );

  };

  //////////////////////////////////////////////////////////
  ////  Update Subnavigation
  //////////////////////////////////////////////////////////

  const updateGliderSubnavigation = ( $gliderObject = false, $index = false ) => {
//     if ( $gliderObject ) {
//       if ( debug ) console.log( 'updateGliderSubnavigation', $gliderObject );
//       let elementSubnavigationItems = document.querySelectorAll( '[data-gallery-id="glide--product-campaign-gallery"]' ) || [];
//       if ( elementSubnavigationItems.length ) {
//         for (let i = 0; i < elementSubnavigationItems.length; i++) {
//           let element = elementSubnavigationItems[i];
//           let elementIndex = parseInt( element.getAttribute('data-gallery-item-index') );
//           tools.removeClass( 'active', [ element ] );
//           if ( $index === elementIndex ) {
//             tools.addClass( 'active', [ element ] );
//           }
//         }
//       }
//
//     }
  }

  //////////////////////////////////////////////////////////
  ////  Update Glider Status
  //////////////////////////////////////////////////////////

  const updateGliderStatus = ( $gliderObject = false, $index = false ) => {

    if ( debug ) console.log( `updateGliderStatus.init() Started` );

    if ( debug ) console.log({
      $gliderObject,
      $index
    });

    if ( $gliderObject ) {

      let classes = { end: 'at-end', start: 'at-start' };
      let element = $gliderObject.element;
      let slides = element.querySelectorAll( '.glide__slide' ) || [];
      let slidesCount = slides.length;

      if ( $index === 0 ) {
        tools.addClass( classes.start, [ element ] );
        tools.removeClass( classes.end, [ element ] );
      }

      if ( $index == (slidesCount - 1) ) {
        tools.addClass( classes.end, [ element ] );
        tools.removeClass( classes.start, [ element ] );
      }

      if ( ( $index > 0 ) && ( $index < ( slidesCount - 1 ) ) ) {
        tools.removeClass( classes.start, [ element ] );
        tools.removeClass( classes.end, [ element ] );
      }

    }

    if ( debug ) console.log( `updateGliderStatus.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Set Gliders
  //////////////////////////////////////////////////////////

  const setGliders = () => {

    if ( getGliders() ) {
      let gliderElements = getGliders();
      for ( let i = 0; i < gliderElements.length; i++ ) {
        let element = gliderElements[i];
        let id = element.id;
        let style = element.getAttribute('data-glide-style') || 'not-set';
        let mobileOnly = element.getAttribute('data-glide-mobile-only') === 'true' || false;
        let desktopOnly = element.getAttribute('data-glide-desktop-only') === 'true' || false;
        gliders[id] = {
          glider: false,
          element: element,
          id: id,
          active: false,
          style: style,
          mobileOnly: mobileOnly,
          desktopOnly: desktopOnly,
        }
      }
    }

  };

  //////////////////////////////////////////////////////////
  ////  Initialize Gliders
  //////////////////////////////////////////////////////////

  const initGliders = () => {

    if ( debug ) console.log( 'initGliders started' );

    let windowWidth = window.innerWidth;
    let options = {};

    for ( let key in gliders ) {

      let glider = gliders[key];
      let gliderStyle = glider.style;

      switch ( gliderStyle ) {
        case 'featured-customers':
          options = gliderOptions({
            breakpoints: {
              566: {
                gap: 20,
                perView: 1
              },
              768: {
                gap: 20,
                perView: 2
              },
              991: {
                gap: 20,
                perView: 3
              },
              1199: {
                gap: 20,
                perView: 4
              },
              9999: {
                gap: 20,
                perView: 5
              }
            },
            type: 'carousel'
          });
          break;
        default:
          options = gliderOptions();
          break;
      }

      if ( glider.mobileOnly ) {
        if ( windowWidth > breakpoints.sizes.lg ) {
          toggleGliderByID( glider.id, false, options );
        } else {
          toggleGliderByID( glider.id, true, options );
        }
        return;
      }

      if ( glider.desktopOnly ) {
        if ( windowWidth >= breakpoints.sizes.lg ) {
          toggleGliderByID( glider.id, true, options );
        } else {
          toggleGliderByID( glider.id, false, options );
        }
        return;
      }

      toggleGliderByID( glider.id, true, options );

      return;

    }

    if ( debug ) console.log( 'initGliders finished' );

  };

  //////////////////////////////////////////////////////////
  ////  Subnavigation Controls
  //////////////////////////////////////////////////////////

  const subnavigationControls = () => {

    let targets = '.product__campaign-gallery-subnavigation-item';
    let targetElements = document.querySelectorAll('.product__campaign-gallery-subnavigation-item') || [];

    function updateGalleryByIndex( $target = false ){
      let galleryID = $target.getAttribute('data-gallery-id') || false;
      let selectedIndex = $target.getAttribute('data-gallery-item-index') || false;
      let glider = gliders[galleryID];
      if ( glider.active ) {
        glider.glider.go( '=' + selectedIndex );
      }
    }

    targetElements.forEach((target) => {
      target.addEventListener( 'click', function(){
        updateGalleryByIndex( target );
      });
    });

  };

  //////////////////////////////////////////////////////////
  ////  Toggle Glider by ID
  //////////////////////////////////////////////////////////

  const toggleGliderByID = ( $id = false, $enable = true, $options = {} ) => {

    if ( $id ) {

      let glider = gliders[$id];

      if ( $enable ) {
        if ( glider.active === false ) {

          let glide = new Glide( '#' + glider.id, $options );

          glide.on( 'mount.after', function( e ) {
            glider.glider = glide;
            glider.active = true;
            gliders[$id] = glider;
            setTimeout( () => updateGliderHeight( glide.selector ), 50 );
          });

          glide.on([ 'build.after', 'move', 'run', 'run.after' ], function( e ) {
            setTimeout( () => updateGliderHeight( glide.selector ), 50 );
            updateGliderStatus( glider, glider.glider.index );
          });

          glide.mount();

          tools.removeClass( 'disabled', [ glider.element ] );

        }
      } else {
        if ( glider.active === true ) {
          glider.glider.destroy();
          glider.element.querySelector( '.glide__track' ).style.height = 'auto';
          glider.active = false;
          gliders[$id] = glider;
          tools.addClass( 'disabled', [ glider.element ] );
        }
      }

    }

  };

  //////////////////////////////////////////////////////////
  ////  Public Method | Initialize
  //////////////////////////////////////////////////////////

  const init = () => {

    if ( debug ) console.log( `${info.name}.init() v.${info.version} Started` );

    let ticking = false;

    setGliders();
    initGliders();
    subnavigationControls();

    window.addEventListener('resize', function(e){
      if ( !ticking ) {
        window.requestAnimationFrame(function() {
          initGliders();
          ticking = false;
        });
        ticking = true;
      }
    });

    if ( debug ) console.log( `${info.name}.init() v.${info.version} Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    debug: debug,
    info: info,
    init: init,
    gliderOptions: gliderOptions,
    gliders: gliders
  };

});
