//////////////////////////////////////////////////////////
////  WP Shopify Plugin
//////////////////////////////////////////////////////////

const WPShopifyPlugin = (() => {

	let debug = false;
	let info = { name : 'WPShopifyPlugin', version : '1.0' };

  let breakpoints = new Breakpoints();
  let tools = new Tools();

  //////////////////////////////////////////////////////////
  ////  Actions
  //////////////////////////////////////////////////////////

  const actions = () => {

    if ( debug ) console.log( `${info.name}.init() actions() Started` );

    // ---------------------------------------- Cart | After Ready
    wp.hooks.addAction( 'after.cart.ready', 'wpshopify', function( cartState ) {
      if ( debug ) console.log( 'after.cart.ready :: ', cartState  );
      toggleCart();
    });

    // ---------------------------------------- Product | After Added to Cart
    wp.hooks.addAction( 'after.product.addToCart', 'wpshopify', function( lineItems, variant, cartState ) {
      if ( debug ) console.log( 'after.product.addToCart :: ', { lineItems, variant, cartState } );
    });

  };

  //////////////////////////////////////////////////////////
  ////  Filters
  //////////////////////////////////////////////////////////

  const filters = () => {

    if ( debug ) console.log( `${info.name}.init() filters() Started` );

    // ---------------------------------------- Product | Unavailable
    wp.hooks.addFilter('products.buyButton.unavailable.html', 'wpshopify', function( defaultVal, productState ) {
      return '<button type="button" class="button button--rounded button--fill button--black" disabled>Out of Stock</button>';
    });

    // ---------------------------------------- Product | Pricing
    wp.hooks.addFilter('product.pricing.from.text', 'wpshopify', function( defaultText ) {
      //return 'Custom from text';
      console.log( defaultText );
    });

  };

  //////////////////////////////////////////////////////////
  ////  Toggle Cart (on 'click')
  //////////////////////////////////////////////////////////

  const toggleCart = () => {
    document.querySelectorAll('.js--cart-summary').forEach(( button ) => {
      button.addEventListener( 'click', function(){
        wp.hooks.doAction('cart.toggle', 'open');
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = () => {

    if ( debug ) console.log( `${info.name}.init() Started` );

    if ( wp !== 'undefined' ) {
      actions();
      filters();
    }

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init
	};

})();
