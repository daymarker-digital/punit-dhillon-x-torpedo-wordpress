//////////////////////////////////////////////////////////
////  Press
//////////////////////////////////////////////////////////

const Press = (() => {

	let debug = false;
	let info = { name : 'Press', version : "1.0" };

  let breakpoints = new Breakpoints();
  let tools = new Tools();
  let ticking = false;

	//////////////////////////////////////////////////////////
  ////  Toggle Likes
  //////////////////////////////////////////////////////////

  const setPressImageHeight = () => {

    let pressItem = document.querySelector('.press__item:not(.wide) .press__image') || false;
    let pressItemHeight = pressItem.offsetHeight || false;
    let viewportWidth = window.innerWidth;

    if ( pressItemHeight ) {
      if ( viewportWidth >= breakpoints.sizes.lg ) {
        tools.setCSSVariable( 'theme-press-item-height', pressItemHeight + 'px' );
        console.log( 'desktop', pressItemHeight );
      } else {
        tools.setCSSVariable( 'theme-press-item-height', 'auto' );
      }
    }

  };

  //////////////////////////////////////////////////////////
  ////  Update Likes
  //////////////////////////////////////////////////////////

  const updateLikes = ( $params = {} ) => {};

	//////////////////////////////////////////////////////////
	//// Init
	//////////////////////////////////////////////////////////

	const init = ( $options = false ) => {
    if ( debug ) console.log( `${info.name}.init() Started` );

    setPressImageHeight();

    window.addEventListener('resize', function(e){
      if ( !ticking ) {
        window.requestAnimationFrame(function() {
          setPressImageHeight();
          ticking = false;
        });
        ticking = true;
      }
    });

    if ( debug ) console.log( `${info.name}.init() Finished` );
  };


	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
    debug,
    info,
    init
	};

});
