//////////////////////////////////////////////////////////
////  Modals
//////////////////////////////////////////////////////////

const Modals = (() => {

	let debug = false;
	let info = { name : 'Modals', version : '2.5' };

	//////////////////////////////////////////////////////////
  ////  Newsletter
  //////////////////////////////////////////////////////////

	const newsletter = {

  	cookie: {
    	name: 'newsletter',
      value: 'seen',
      expires: 180
  	},

  	target: 'modal--newsletter',

  	setCookie: function() {
    	Browser.setCookie( this.cookie.name, this.cookie.value, this.cookie.expires );
    },

    show: function( $delay ) {

      let target = this.target;
      let onClose = this.setCookie();

      setTimeout(function(){
        MicroModal.show( target, {
          awaitCloseAnimation: true,
          onShow: function(modal) {
            console.log('micromodal opened');
          },
          onClose: function(modal) {
            console.log('micromodal closed');
            onClose;
          }
        });
      }, $delay );

    },

    init: function() {

      let cookieNotSeen = Browser.getCookie( this.cookie.name ) !== 'seen';
      let modalExists = $('#' + this.target).length;
      let showModal = ( modalExists && cookieNotSeen ) ? true : false;

      let daysSaved = parseInt( $('#' + this.target).attr('data-days-saved') ) || 180;
      let timeout = parseInt( $('#' + this.target).attr('data-timeout') ) || 1500;

      this.cookie.expires = daysSaved;

      if ( showModal ) {
  		  this.show( timeout );
  		}

  		$('#' + this.target + ' [data-micromodal-close]').on('click', function(){
        MicroModal.close( this.target );
  		});

    },

	};

  //////////////////////////////////////////////////////////
  ////  Share
  //////////////////////////////////////////////////////////

  const notifyMe = {

    target: 'modal--notify-me',

    init: function() {

      let modalExists = $('#' + this.target).length;
      let target = this.target;

      if ( modalExists ) {

        $('.js--notify-me').on('click', function(){

          MicroModal.show( target, {
            disableFocus: true,
            awaitCloseAnimation: true,
            onShow: function(modal) {
              console.log('micromodal opened');
            },
            onClose: function(modal) {
              console.log('micromodal closed');
            }
          });

        });

        $('#' + this.target + ' [data-micromodal-close]').on('click', function(){
          MicroModal.close( target );
        });

      }

    },

  };

	//////////////////////////////////////////////////////////
  ////  Share
  //////////////////////////////////////////////////////////

	const share = {

  	target: 'modal--share',

    init: function() {

      let modalExists = $('#' + this.target).length;
      let target = this.target;

      if ( modalExists ) {

        $('.js--share').on('click', function(){

          MicroModal.show( target, {
            disableFocus: true,
            awaitCloseAnimation: true,
            onShow: function(modal) {
              console.log('micromodal opened');
            },
            onClose: function(modal) {
              console.log('micromodal closed');
            }
          });

        });

  		  $('#' + this.target + ' [data-micromodal-close]').on('click', function(){
          MicroModal.close( target );
    		});

  		}

    },

	};

	//////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {

    if ( debug ) console.log( `${info.name}.init() Started` );

    newsletter.init();
    notifyMe.init();
    share.init();

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

	//////////////////////////////////////////////////////////
	//// Init
	//////////////////////////////////////////////////////////

	const init = () => {
		main();
	};

	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
    init : init
	};

});
