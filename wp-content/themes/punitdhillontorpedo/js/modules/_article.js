//////////////////////////////////////////////////////////
////  Article
//////////////////////////////////////////////////////////

const Article = (() => {

	let DEBUG = false;
	let INFO = { name : 'Article', version : 1.0 };
	var BREAKPOINTS = {
		sm: 567,
		md: 768,
		lg: 992,
		xl: 1200,
		xxl: 1400
	};

	//////////////////////////////////////////////////////////
  ////  Update Status
  //////////////////////////////////////////////////////////

  const updateStatus = () => {

    let viewportTop = window.pageYOffset || document.documentElement.scrollTop;
    let viewportHeight = window.innerHeight;
    let target = '.footer';
    let extraOffset = 0;

		$(target).each( function( index, item ) {

  		let thisEl = $(item);

  		let element = {
    		height: thisEl.outerHeight(),
        topEdge: thisEl.offset().top,
        bottomEdge: thisEl.outerHeight() + thisEl.offset().top,
  		};

  		let viewport = {
    		topEdge: viewportTop,
        height: viewportHeight,
        bottomEdge: viewportTop + viewportHeight,
  		};

  		if ( DEBUG ) {
    		console.log( '----------' );
    		console.log( 'item: ' + index );
    		console.log({
      		element: element,
      		viewport: viewport
        });
  		}

  		//////////////////////////////////////////////////////////
      ////
      //////////////////////////////////////////////////////////

      if ( element.topEdge > viewport.bottomEdge ) {
        // element not in view
        //console.log( 'element NOT IN view' );
      } else {
        // element in view
        //console.log( 'element IN view' );
        //extraOffset = viewport.bottomEdge - element.topEdge;
      }

      setHeight( extraOffset );

    });

    function setHeight( $extraOffset = 0 ) {

        let target = '[data-article-sizing]';
        let headerHeight = $('#header').outerHeight();
        let viewportHeight = window.innerHeight;
        let newHeight = viewportHeight - headerHeight;

        if ( $extraOffset ) {
          newHeight = newHeight - $extraOffset;
        }

        if ( isDesktop() ) {
          $(target).css({
            height: newHeight + 'px'
          });
        }

      };

    function isDesktop() {

      let viewportWidth = window.innerWidth;

      if ( viewportWidth >= BREAKPOINTS.lg ) {
        return true;
      }

      return false;

    };

  };

	//////////////////////////////////////////////////////////
  ////  Scroll Watcher
  //////////////////////////////////////////////////////////

	const scrollWatcher = () => {

    updateStatus();
    Browser.onResize( updateStatus, 150 );
    Browser.onScroll( updateStatus, 0 );

  };

	//////////////////////////////////////////////////////////
	////  Initialize
	//////////////////////////////////////////////////////////

	const init = () => {
		scrollWatcher();
	};

	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
		init : init,
		updateStatus: updateStatus,
	};

});
