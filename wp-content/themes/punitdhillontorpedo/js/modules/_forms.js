//////////////////////////////////////////////////////////
////  Forms
//////////////////////////////////////////////////////////

var Forms = (function () {

  var DEBUG = false;
  var INFO = { name : 'Forms', version : '2.0' };

  //////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {

    $('.js--validate-me').on('click', 'button[type="submit"]', function(event){

      event.preventDefault();

      let thisform = $(this).closest('form');

      if ( isFormValid( thisform ) ) {
        submitForm( thisform );
      }

    });

  };

  //////////////////////////////////////////////////////////
  ////  Is Field Valid
  //////////////////////////////////////////////////////////

  const isFieldValid = ( $field, $value, $type ) => {

    let isValid = false;

    switch ( $type ) {
      case 'INPUT':
        let type = $field.attr('type');
        switch ( type ) {
          case 'email':
            if ( validator.isEmail( $value ) ) {
              isValid = true;
            }
            break;
          case 'radio':
            console.log( 'radio' );
            console.log( $field );
            break;
          case 'tel':
            if ( validator.isMobilePhone( $value ) ) {
              isValid = true;
            }
            break;
          case 'text':
            if ( ! validator.isEmpty( $value ) ) {
              isValid = true;
            }
            break;
        }
        break;
      case 'TEXTAREA':
        if ( ! validator.isEmpty( $value ) ) {
          isValid = true;
        }
        break;
    }

    return isValid;

  };

  //////////////////////////////////////////////////////////
  ////  Is Form Valid
  //////////////////////////////////////////////////////////

  const isFormValid = ( $form ) => {

    let isValid = true;
    let requiredFields = $form.find('.required');
    let rudeField = $form.find('.rude');
    let requiredValues = {};

    $( requiredFields ).each(function(){

      let thisField = $(this);
      let fieldValue = thisField.val();
      let fieldName = thisField.attr('name');
      let fieldType = thisField.prop('nodeName');
      let fieldValid = isFieldValid( thisField, fieldValue, fieldType );

      if ( fieldValid ) {
        if ( $form.hasClass('contact-us') ) {
          thisField.closest('.form__row').removeClass('error');
        } else {
          thisField.closest('.form__field').removeClass('error');
        }
      } else {
        if ( $form.hasClass('contact-us') ) {
          thisField.closest('.form__row').addClass('error');
        } else {
          thisField.closest('.form__field').addClass('error');
        }
      }

      requiredValues[fieldName] = { valid: fieldValid, value: fieldValue, type: fieldType };

    });

    if ( DEBUG ) {
      console.log( 'isFormValid', requiredValues );
    }

    $.each( requiredValues, function( key, value ) {
      if ( value.valid == false ) {
        isValid = false;
        return false;
      }
    });

    if ( rudeField.val() ) {
      if ( DEBUG ) {
        console.log( 'rudeField.val():', 'SPAM' );
      }
      isValid = false;
    }

    return isValid;

  };

  //////////////////////////////////////////////////////////
  ////  Submit Form
  //////////////////////////////////////////////////////////

  const submitForm = ( $form ) => {

    let action = $form.attr('action') || false;
    let data = new FormData( $form[0] ) || false;
    let successAction = $form.attr('data-success-action') || false;

    if ( $form && action && data ) {
      $.ajax({
        url: action,
        type: 'POST',
        async: true,  // maybe disable with formspree?
        data: data,
        dataType: "json",
        processData: false,
        contentType: false,
        successAction: successAction,
        success: function ( $data, $textStatus, $jqXHR ) {
          console.log( '[ Forms.init() ] Ajax Success', this.data );
          confirmation( $form, successAction );
        },
        error: function( $jqXHR, $textStatus, $errorThrown ) {
          console.log( '[ Forms.init() ] Ajax Error' );
          console.log( [ $jqXHR, $textStatus, $errorThrown ] );
        }
      });
    }

    // ---------------------------------------- Confirmation

    function confirmation( $form, $successAction ) {

      // display quick thank you message
      $form.find('.form__main, .form__action').empty();
      $form.find('.form__main').html('<p class="form__success-message">Thanks for your interest!</p>');

      // conditionally execute success action
      switch ( $successAction ) {
        case "none":
          break;
        case "close-modal":
          let modal = $form.closest('.modal');
          let modalID = modal.attr('id') || false;
          if ( modalID ) {
            setTimeout(function(){
              MicroModal.close( modalID );
            }, 1250);
          }
          break;
      }

      /*
      setTimeout(function() {
        $form.find('.form__notification--success').fadeIn( 250, function(){
          reset( $form );
        });
      }, 50);
      */

    }

    // ---------------------------------------- Reset

    function reset( $form ) {
      $form[0].reset();
    }

  };

  //////////////////////////////////////////////////////////
  ////  Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    main();
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    info : INFO,
    init : init
  };

});
