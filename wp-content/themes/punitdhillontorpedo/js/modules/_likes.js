//////////////////////////////////////////////////////////
////  Likes
//////////////////////////////////////////////////////////

const Likes = (() => {

	let debug = false;
	let info = { name : 'Likes', version : "1.0" };

  let tools = new Tools();

	//////////////////////////////////////////////////////////
  ////  Toggle Likes
  //////////////////////////////////////////////////////////

  const incrementLikes = () => {

    document.querySelectorAll( '.js--likes' ).forEach( button => {
      button.addEventListener( 'click', () => {

        let formAction = button.getAttribute('data-form-action') || '';
        let likes = parseInt( button.getAttribute('data-likes-count') ) || 0;
        let postID = parseInt( button.getAttribute('data-post-id') ) || 0;

        likes++;

        updateLikes({
          button,
          formAction,
          likes,
          postID
        });

      });
    });

  };

  //////////////////////////////////////////////////////////
  ////  Update Likes
  //////////////////////////////////////////////////////////

  const updateLikes = ( $params = {} ) => {

    let settings = {
      action: 'VP__likes',
      button: false,
      formAction: false,
      likes: 0,
      postID: false,
    };

    // merge $params into default settings
    if ( $params && (typeof $params == "object") ) {
      settings = { ...settings, ...$params };
    }

    document.querySelectorAll( '.js--likes' ).forEach( button => {
      button.disabled = true;
    });

    // WordPress expects 'form data' from Axios post request
    var data = new FormData();
    data.append( 'likes', settings.likes );
    data.append( 'postID', settings.postid );
    data.append( 'action', settings.action );
    let url = settings.formAction;

    // post to WordPress admin-ajax.php
    axios.post( url, data )
    .then(function (response) {

      document.querySelectorAll( '.js--likes' ).forEach( button => {
        button.setAttribute( 'data-likes-count', settings.likes );
        button.disabled = false;
        let buttonCount = button.querySelector('.button__count') || false;
        if ( buttonCount ) {
          buttonCount.innerHTML = settings.likes;
        }
      });

    })
    .catch(function (error) {});

  };

	//////////////////////////////////////////////////////////
	//// Init
	//////////////////////////////////////////////////////////

	const init = ( $options = false ) => {
    if ( debug ) console.log( `${info.name}.init() Started` );
    incrementLikes();
    if ( debug ) console.log( `${info.name}.init() Finished` );
  };


	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
    debug,
    info,
    init
	};

});
