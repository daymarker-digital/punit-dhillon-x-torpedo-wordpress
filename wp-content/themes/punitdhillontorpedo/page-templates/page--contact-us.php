<?php

/*
*
*	Template Name: Page [ Contact Us ]
*	Filename: page--contact-us.php
*
*/

get_header();

// ---------------------------------------- Polite Department
$VP = new PDTheme();

// ---------------------------------------- WP Loop
if ( have_posts() ) {
  while ( have_posts() ) {

    // init post data
    the_post();

    // ---------------------------------------- Vars
    $block_name = 'contact-us';

    // ---------------------------------------- ACF Vars
    $contact = get_field( 'contact' ) ? get_field( 'contact' ) : false;
    $intro = get_field( 'intro' ) ? get_field( 'intro' ) : false;

    // ---------------------------------------- Post Vars
    $content = get_the_content();
    $post_id = get_the_ID();
    $title = get_the_title();

    // ---------------------------------------- Template
    echo '<section class="section section--' . $block_name . ' ' . $block_name . '" data-background-colour="black">';
      echo $VP->render_container( 'open', 'col-12 col-lg-10 offset-lg-1', 'container-fluid' );

        // ---------------------------------------- Main
        echo '<div class="' . $block_name . '__main">';
          echo '<div class="row row--inner">';

            echo '<div class="col-12">';
              echo '<h1 class="' . $block_name . '__main-heading heading heading--title">' . $title . '</h1>';
            echo '</div>';

            echo '<div class="col-12 col-lg-5">';
              echo $intro ? '<div class="' . $block_name . '__main-message message rte">' . $intro . '</div>' : '';
            echo '</div>';

            echo '<div class="d-none d-lg-block col-lg-5 offset-lg-1">';
              echo $contact ? '<div class="' . $block_name . '__main-enquiries message rte">' . $contact . '</div>' : '';
            echo '</div>';

          echo '</div>';
        echo '</div>';

        // ---------------------------------------- Connect
        echo '<div class="' . $block_name . '__connect">';
          echo '<div class="row row--inner">';

            if ( have_rows( 'connect' ) ) {
              while ( have_rows( 'connect' ) ) {

                // init data
                the_row();

                // default data
                $cta = get_sub_field( 'cta' ) ? get_sub_field( 'cta' ) : false;
                $heading = get_sub_field( 'heading' ) ? get_sub_field( 'heading' ) : false;
                $message = get_sub_field( 'message' ) ? get_sub_field( 'message' ) : false;

                // print data
                if ( $heading || $message ) {
                  echo '<div class="col-12 col-sm-6 col-lg-3">';
                    echo '<div class="' . $block_name . '__connect-item">';

                      echo $heading ? '<h2 class="' . $block_name . '__connect-heading heading">' . $heading . '</h2>' : '';
                      echo $message ? '<div class="' . $block_name . '__connect-message message rte">' . $message . '</div>' : '';

                      if ( $cta ) {
                        echo '<div class="' . $block_name . '__connect-cta">';
                          $cta['classes'] = 'button button--pill button--secondary';
                          $cta['email_subject'] = $heading ? $heading : '';
                          echo $VP->render_cta( $cta );
                        echo '</div>';
                      }

                    echo '</div>';
                  echo '</div>';
                }

              }

              echo '<div class="col-12 d-lg-none">';
                echo '<div class="' . $block_name . '__connect-message message rte">';
                  echo '<p>All general enquiries can be sent to:<br><a href="mailto:hello@punitdhillon.com" target="_blank">hello@punitdhillon.com</a><br>Thanks!</p>';
                echo '</div>';
              echo '</div>';

            }

          echo '</div>';
        echo '</div>';

      echo $VP->render_container( 'closed' );
    echo '</section>';

  }
}

get_footer();

?>
