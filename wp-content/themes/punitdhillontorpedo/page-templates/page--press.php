<?php

/*
*
*	Template Name: Page [ Press ]
*	Filename: page--press.php
*
*/

get_header();

$VP = new PDTheme();
$post_id = get_the_ID();
$block_name = 'press';
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$featured = get_field( 'featured' ) ? get_field( 'featured' ) : [];
$navigation = get_field( 'navigation' ) ? get_field( 'navigation' ) : [];

echo '<div class="' . $block_name . '">';

  // ---------------------------------------- Grid | Featured
  if ( $featured && ( $paged < 2 ) ) {
    echo '<section class="' . $block_name . '__grid featured">';
      echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
        echo '<div class="row row--inner">';

          foreach( $featured as $i => $item ) {

            $press_id = ( isset($item['press']) && !empty($item['press']) ) ? $item['press'] : false;
            $press_wide = ( 0 === $i ) ? true : false;
            $column_classes = ( 0 === $i ) ? 'col-12 col-lg-6' : 'col-12 col-sm-6 col-lg-3';

            if ( $press_id ) {
              echo '<div class="' . $column_classes . '">';
                echo $VP->render_press_preview( [ 'post_id' => $press_id, 'wide' => $press_wide ] );
              echo '</div>';
            }

          }

        echo '</div>';
      echo $VP->render_container( 'closed' );
    echo '</section>';
  }

  // ---------------------------------------- Grid | All
  $query_args = [
    'post_type'             => [ 'press' ],
    'post_status'           => [ 'publish' ],
    'posts_per_page'        => 4,
    'order'                 => 'DESC',
    'orderby'               => 'date',
    'nopaging'              => false,
    'paged'                 => $paged,
    'post__not_in'          => [],
  ];

  foreach ( $featured as $i => $item ) {
    $press_id = ( isset($item['press']) && !empty($item['press']) ) ? $item['press'] : false;
    if ( $press_id ) {
      array_push( $query_args['post__not_in'], $press_id );
    }
  }

  $press_query = new WP_Query( $query_args );

  if ( $press_query->have_posts() ) {

    echo '<section class="' . $block_name . '__grid all">';
      echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
        echo '<div class="row row--inner">';

          while ( $press_query->have_posts() ) {

            // init post data
            $press_query->the_post();
            $press_id = get_the_ID();

            echo '<div class="col-12 col-sm-6 col-md-4 col-lg-3">';
              echo $VP->render_press_preview( [ 'post_id' => $press_id ] );
            echo '</div>';

          }

        echo '</div>';
      echo $VP->render_container( 'closed' );
    echo '</section>';

    // ---------------------------------------- Grid | All Pagination
    if ( $press_query->max_num_pages > 1 )  {

      $pagination_links = [];
      $max_pages = $press_query->max_num_pages;
      $pagination_format = empty( get_option('permalink_structure') ) ? '&page=%#%' : 'page/%#%/';
      $pagination_args = [
        'base' => get_pagenum_link(1) . '%_%',
        'format' => $pagination_format,
        'total' => $max_pages,
        'current' => $paged,
        'aria_current' => false,
        'show_all' => true,
        'end_size' => 1,
        'mid_size' =>2,
        'prev_next' => false,
        'prev_text' => '',
        'next_text' => '',
        'type' => 'array',
        'add_args' => false,
        'add_fragment' => '',
        'before_page_number' => '',
        'after_page_number' => ''
      ];

      $pagination_links = paginate_links( $pagination_args ) ? paginate_links( $pagination_args ) : [];
      $pagination_labels = [
        'next' => 'Next',
        'prev' => 'Prev',
      ];
      $pagination_buttons = [
        'next' => get_next_posts_link( $pagination_labels['next'], $max_pages ),
        'prev' => get_previous_posts_link( $pagination_labels['prev'] ),
      ];

      echo '<section class="' . $block_name . '__pagination pagination">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
          echo '<div class="pagination__main">';

            echo '<div class="pagination__button prev ' . ( $pagination_buttons['prev'] ? 'active' : 'not-active' ) . '">';
              echo $pagination_buttons['prev'] ? $pagination_buttons['prev'] : 'Prev';
            echo '</div>';

            foreach( $pagination_links as $i => $item ) {
              echo '<div class="pagination__link">' . $item . '</div>';
            }

            echo '<div class="pagination__button next ' . ( $pagination_buttons['next'] ? 'active' : 'not-active' ) . '">';
              echo $pagination_buttons['next'] ? $pagination_buttons['next'] : 'Next';
            echo '</div>';

          echo '</div>';
        echo $VP->render_container( 'closed' );
      echo '</section>';

    }

    // ---------------------------------------- Reset Post Data
    wp_reset_postdata();

  }

echo '</div';

get_footer();

?>
