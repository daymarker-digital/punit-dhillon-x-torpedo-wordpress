<?php

/*
*
*	Template Name: Page [ Issues ]
*	Filename: page--issues.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();

$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$meta_query = [
  'issue_number' => [
    'key'     => 'issue',
    'compare' => 'EXISTS',
    'type'    => 'NUMERIC'
  ],
];
$order_by = [
  'issue_number' => 'ASC',
];
$args = [
	'post_type'              	=> [ 'issue' ],
	'post_status'            	=> [ 'publish' ],
	'posts_per_page' 			    => 12,
  'paged'                   => $paged,
  'meta_query'              => $meta_query,
  'orderby'                 => $order_by,
];

$query = new WP_Query( $args );

echo '<section class="section section--issues-listing issues-listing">';
  echo $VP->render_container( 'open', 'col-12', 'container-fluid' );
    echo '<div class="row row--inner">';

      if ( $query->have_posts() ) {

        $loop_count = 1;

      	while ( $query->have_posts() ) {

          $query->the_post();

          echo '<div class="col-12 col-sm-6 col-lg-4 col-xl-3" data-count="' . $loop_count . '">';
      		  echo $VP->render_issue_preview( [ 'post_id' => get_the_ID() ] );
          echo '</div>';

          $loop_count++;

      	}
      }

      wp_reset_postdata();

    echo '</div>';
  echo $VP->render_container( 'closed' );
echo '</section>';


get_footer();

?>
