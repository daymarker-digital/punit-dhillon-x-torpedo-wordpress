<?php

/*
*
*	Template Name: Page [ Flexible Content ]
*	Filename: page--flexible-content.php
*
*/

get_header();

// ---------------------------------------- Vars
$VP = new PDTheme();
$post_id = get_the_ID();

// ---------------------------------------- Templates
echo $VP->render_section_intro( [ 'post_id' => $post_id ] );
echo $VP->render_flexible_content( [ 'post_id' => $post_id ] );

get_footer();

?>
