<?php
	
/*
*	
*	Template Name: Page [ Product ]
*	Filename: page--product.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();
$home = $VP->get_theme_directory('home');
$assets_dir = $VP->get_theme_directory('assets');
$theme_dir = $VP->get_theme_directory();

//////////////////////////////////////////////////////////
////  Section | Intro
//////////////////////////////////////////////////////////

if ( have_posts() ) {
  echo '<section>';
  while ( have_posts() ) {
    
    the_post(); 
    
  } // end while
  echo '</section>';
} // end if

get_footer(); 

?>
