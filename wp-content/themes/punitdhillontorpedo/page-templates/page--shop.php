<?php

/*
*
*	Template Name: Page [ Shop ]
*	Filename: page--shop.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();
$home = $VP->get_theme_directory('home');
$assets_dir = $VP->get_theme_directory('assets');
$theme_dir = $VP->get_theme_directory();

//////////////////////////////////////////////////////////
////  Template Vars
//////////////////////////////////////////////////////////

$page_id = $post->ID;
$Products = WP_Shopify\Factories\DB\Products_Factory::build();

//////////////////////////////////////////////////////////
////  Template Layout
//////////////////////////////////////////////////////////

echo '<section class="section sticky-column shop wps-shop product">';
  echo '<div class="sticky-column__container">';

    //////////////////////////////////////////////////////////
    ////  Left Column
    //////////////////////////////////////////////////////////

    echo '<div class="sticky-column__column sticky-column__column--stuck">';
      echo '<div class="sticky-column__main">';
        echo '<div class="sticky-column__content">';

          if ( have_rows( 'featured_product', $page_id ) ) {
            while ( have_rows( 'featured_product', $page_id ) ) {

              // init data
              the_row();

              // default data
              $button_title = $product = false;

              // get data {
              if ( get_sub_field( 'button_title' ) ) {
                $button_title = get_sub_field( 'button_title' );
              }
              if ( get_sub_field( 'product' ) ) {

                $post_id = get_sub_field( 'product' );
                $product_post = get_post( $post_id );

                $product_title = $product_post->post_title;
                $product_desc = $product_post->post_content;
                $product_permalink = $product_post->guid;
                $product_slug = $product_post->post_name;
                $product_id = $Products->get_product_ids_from_handles([ $product_slug ]);
                $product_featured_image = $VP->get_featured_image_by_post_id( $post_id );

                //////////////////////////////////////////////////////////
                ////  Top
                //////////////////////////////////////////////////////////

                echo '<div class="sticky-column__content-top">';

                  if ( have_rows( 'featured_image', $post_id ) ) {
                    while ( have_rows( 'featured_image', $post_id ) ) {

                      // init data
                      the_row();

                      // default data
                      $mobile = $desktop = false;

                      // get data
                      if ( get_sub_field( 'mobile' ) ) {
                        $mobile = get_sub_field( 'mobile' );
                      }
                      if ( get_sub_field( 'desktop' ) ) {
                        $desktop = get_sub_field( 'desktop' );
                      }

                      // print data
                      if ( $mobile ) {
                        echo '<div class="sticky-column__featured-image product__featured-image product__featured-image--mobile">';
                          echo $VP->render_lazyload_image( $mobile, [ 'alt_text' => $title ] );
                        echo '</div>';
                      }
                      if ( $desktop ) {
                        echo '<div class="sticky-column__featured-image product__featured-image product__featured-image--desktop">';
                          echo $VP->render_lazyload_image( $desktop, [ 'alt_text' => $title ] );
                        echo '</div>';
                      }

                    }
                  } else {
                    if ( $product_featured_image ) {
                      echo '<div class="sticky-column__featured-image product__image">';
                        echo $VP->render_lazyload_image( $product_featured_image, [ 'alt_text' => $product_title ] );
                        echo $VP->render_lazyload_image( $product_featured_image, [ 'background' => true ] );
                      echo '</div>';
                    }
                  }

                  echo '<h1 class="product__title heading heading--title">' . $product_title . '</h1>';

                  $price_shortcode = '[wps_products_pricing';
                  $price_shortcode .= ' show_compare_at="false"';
                  $price_shortcode .= ' product_id="' . $product_id[0] .'"';
                  $price_shortcode .= ']';

                  if ( get_field( 'featured_content', $post_id ) ) {
                    echo '<div class="product__desc message rte">';
                      echo get_field( 'featured_content', $post_id );
                    echo '</div>';
                  } else {
                    if ( get_the_content( $post_id ) ) {
                      echo '<div class="product__desc message rte">';
                        echo apply_filters( 'the_content', get_the_content( $post_id ) );
                      echo '</div>';
                    }
                  }

                  if ( $product_permalink ) {
                    echo '<a class="button button--pill button--secondary" href="' . $product_permalink . '">' . $button_title . '</a>';
                  }

                echo '</div>';

                //////////////////////////////////////////////////////////
                ////  Bottom
                //////////////////////////////////////////////////////////

                echo '<div class="sticky-column__content-bottom">';
                echo '</div>';

              }

            }
          }

        echo '</div>';
      echo '</div>';
    echo '</div>';

    //////////////////////////////////////////////////////////
    ////  Right Column
    //////////////////////////////////////////////////////////

    echo '<div class="sticky-column__column sticky-column__column--scroll">';
      echo '<div class="sticky-column__main">';

        echo '<div class="product-listing">';
          echo '<div class="row">';

            if ( have_rows( 'curated_products', $page_id ) ) {
              while ( have_rows( 'curated_products', $page_id ) ) {

                // init data
                the_row();

                // default data
                $post_id = false;

                // get data
                if ( get_sub_field( 'product' ) ) {

                  $post_id = get_sub_field( 'product' );
                  $product_post = get_post( $post_id );

                  $product_title = $product_post->post_title;
                  $product_desc = $product_post->post_content;
                  $product_permalink = $product_post->guid;
                  $product_slug = $product_post->post_name;
                  $product_id = $Products->get_product_ids_from_handles([ $product_slug ]);
                  $product_featured_image = $VP->get_featured_image_by_post_id( $post_id );

                  echo '<div class="col-12 col-md-6">';
                    echo '<div class="product product--curated wps-product">';
                      echo '<a href="' . $product_permalink . '">';

                        if ( have_rows( 'featured_image', $post_id ) ) {
                          while ( have_rows( 'featured_image', $post_id ) ) {

                            // init data
                            the_row();

                            // default data
                            $mobile = $desktop = false;

                            // get data
                            if ( get_sub_field( 'mobile' ) ) {
                              $mobile = get_sub_field( 'mobile' );
                            }
                            if ( get_sub_field( 'desktop' ) ) {
                              $desktop = get_sub_field( 'desktop' );
                            }

                            // print data
                            if ( $mobile ) {
                              echo '<div class="product__image product__image--listing">';
                                echo $VP->render_lazyload_image( $mobile, [ 'alt_text' => $product_title ] );
                              echo '</div>';
                            }

                          }
                        } else {
                          if ( $product_featured_image ) {
                            echo '<div class="product__image product__image--listing">';
                              echo $VP->render_lazyload_image( $product_featured_image, [ 'alt_text' => $product_title ] );
                            echo '</div>';
                          }
                        }

                        echo '<h2 class="product__title product__title--listing heading heading--title">' . $product_title . '</h2>';

                        $price_shortcode = '[wps_products_pricing';
                        $price_shortcode .= ' show_compare_at="false"';
                        $price_shortcode .= ' product_id="' . $product_id[0] .'"';
                        $price_shortcode .= ']';

                        echo '<div class="product__price product__price--listing" data-product-title="' . $product_title .'">';
                          echo do_shortcode( $price_shortcode );
                        echo '</div>';

                      echo '</a>';
                    echo '</div>';
                  echo '</div>';

                }

              }
            }

          echo '</div>';
        echo '</div>';

      echo '</div>';
    echo '</div>';

  echo '</div>';
echo '</article>';

get_footer();

?>
