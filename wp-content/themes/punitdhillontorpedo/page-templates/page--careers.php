<?php
	
/*
*	
*	Template Name: Page [ Careers ]
*	Filename: page--careers.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();
$home = $VP->get_theme_directory('home');
$assets_dir = $VP->get_theme_directory('assets');
$theme_dir = $VP->get_theme_directory();

echo '<section class="section section--intro section--intro-with-padding sticky-column">';
  echo '<div class="sticky-column__container">';
  
    //////////////////////////////////////////////////////////
    ////  Left Column
    //////////////////////////////////////////////////////////
    
    echo '<div class="sticky-column__column sticky-column__column--scroll">';
      echo '<div class="sticky-column__main">';
      
        echo '<div class="sticky-column__vr vr"></div>';
      
        echo '<div class="jobs">';
          echo '<h2 class="heading heading--title">' . get_the_title() . '</h2>';
          if ( get_field( 'intro' ) ) {
            echo '<div class="message message--intro rte">';
              echo get_field( 'intro' );
            echo '</div>';
          }
          if ( have_rows( 'jobs' ) ) {
            echo '<ul class="jobs__list">';
            while ( have_rows( 'jobs' ) ) {
              
              // init data
              the_row();
              
              // default data
              $title = $desc = false;
              
              // get data
              if ( get_sub_field( 'title' ) ) {
                $title = get_sub_field( 'title' );
              }
              if ( get_sub_field( 'desc' ) ) {
                $desc = get_sub_field( 'desc' );
              }
              
              if ( $title && $desc ) {
                echo '<li class="jobs__item">';
                
                  echo '<h2 class="jobs__title">' . $title . '</h2>';
                  echo '<div class="jobs__desc message rte">' . $desc . '</div>';
                  
                  $cta_classes = "button button--rounded button--outline";
                  $cta_email_subject = $title;
                  include( locate_template( './snippets/layout--cta.php' ) );
                  
                echo '</li>';
              }
              
            }
            echo '</ul>';
          }
        echo '</div>';
     
      echo '</div>';
    echo '</div>';
      
    //////////////////////////////////////////////////////////
    ////  Right Column
    //////////////////////////////////////////////////////////
    
    echo '<div class="sticky-column__column sticky-column__column--stuck">';
      echo '<div class="sticky-column__main">';
        echo '<div class="sticky-column__content">';
        
          //////////////////////////////////////////////////////////
          ////  Top
          //////////////////////////////////////////////////////////

          echo '<div class="sticky-column__content-top">';
            echo '<div class="newsletter newsletter--careers">';
              echo '<h2 class="newsletter__heading heading heading--title">Stay up to date with all Punit’s News (including job postings!)</h2>';
              include( locate_template( './snippets/form--newsletter.php' ) );              
            echo '</div>';
          echo '</div>';
          
          //////////////////////////////////////////////////////////
          ////  Bottom
          //////////////////////////////////////////////////////////
          
          echo '<div class="sticky-column__content-bottom">';
          echo '</div>';

        echo '</div>';
      echo '</div>';
    echo '</div>';
  
  echo '</div>';
echo '</section>';

get_footer(); 

?>
