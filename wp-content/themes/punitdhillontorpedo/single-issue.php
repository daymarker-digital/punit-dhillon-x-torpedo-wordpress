<?php

/*
*
*	Filename: single-issue.php
*
*/

get_header();

//////////////////////////////////////////////////////////
////  Polite Department Vars
//////////////////////////////////////////////////////////

$VP = new PDTheme();

if ( have_posts() ) {
  echo '<div class="issue">';
	while ( have_posts() ) {

		// init post data
		the_post();

    $issue_id = get_the_ID();
    $issue_number = get_field( 'issue' ) ? get_field( 'issue' ) : false;

    echo $VP->render_issue_hero( [ 'post_id' => $issue_id ] );

    if ( have_rows( 'issues', 'options' ) ) {
      echo '<section class="issue__articles-grid">';
        echo $VP->render_container( 'open', 'col-12', 'container-fluid' );

          while ( have_rows( 'issues', 'options' ) ) {

            // init row
            the_row();

            if ( have_rows( 'article_categories' ) ) {
              echo '<div class="row row--inner">';
                while ( have_rows( 'article_categories' ) ) {

                  // init row
                  the_row();

                  // DESC is newest articles first, ASC is oldest articles first
                  $issue_args = [
                		'post_type'              	=> [ 'post' ],
                		'post_status'            	=> [ 'publish' ],
                		'posts_per_page' 			    => 1,
                    'orderby'                 => 'post_date',
                    'order'                   => 'DESC',
                	];
                  $issue_article_id = false;
                  $issue_category = get_sub_field( 'category_id' ) ? get_sub_field( 'category_id' ) : false;
                  $issue_category_id = ( $issue_category ) ? $issue_category->term_id : false;
                  $issue_category_name = ( $issue_category ) ? $issue_category->name : false;

                  if ( $issue_category_id ) {
                    $issue_args['category__in'] = $issue_category_id;
                  }

                  if ( $issue_number ) {
                    $issue_args['meta_query'] = [
                      [
                        'key'     => 'issue',
                        'value'   =>  $issue_number,
                        'compare' => '=',
                        'type'    => 'NUMERIC'
                      ],
                    ];
                  }

                	$issue_query = new WP_Query( $issue_args );

                	if ( $issue_query->have_posts() ) {
                		while ( $issue_query->have_posts() ) {

                      // init post data
                			$issue_query->the_post();

                      // assign issue article id
                			$issue_article_id = get_the_ID();

                		}
                	}

                  echo '<div class="col-12 col-md-6 col-lg-4 col-xxl-3">';
                    echo $VP->render_issue_article_preview( [ 'issue_cat_id' => $issue_category_id, 'issue_cat_name' => $issue_category_name, 'post_id' => $issue_article_id ] );
                  echo '</div>';

                	wp_reset_postdata();

                }
              echo '</div>';
            }

          }

        echo $VP->render_container( 'closed' );
      echo '</section>';
    }

    echo $VP->render_issue_donations( [ 'post_id' => $issue_id ] );

  }
  echo '</div>';
}

get_footer();

?>
